<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EmpresaController;
use App\Http\Controllers\UsuarioController;
use App\Http\Controllers\CovesController;
use App\Http\Controllers\PedimentoController;
use App\Http\Controllers\ExpedienteController;
use App\Http\Controllers\JobUploadPedimentoController;
use App\Http\Controllers\FacturasController;
use App\Http\Controllers\DocumentoController;
use App\Http\Controllers\MovimientoController;
use App\Http\Controllers\AgenteController;
use App\Http\Controllers\OktaController;


use Illuminate\Routing\Route as RoutingRoute;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return redirect("https://cpavision.mx/");
// });

Route::get('/', function () {// customsandtrade.cpavision.mx/ctrade
    return view('welcome');
    // debe de activar la función y verificar
})->middleware('okta_Auth');

Route::get('index', function () {// customsandtrade.cpavision.mx/ctrade
    return view('welcome');
    // debe de activar la función y verificar
})->middleware('okta_Auth');


// Route::post('/ctrade', [OktaController::class, 'OktaLogin']); //config de token 

//configuracion de session y datos de okta
Route::post('/', [OktaController::class, 'OktaLogin']); //config de token 

//cerrar sesion y redirec a okta login
Route::get('salir', [OktaController::class, 'cerrar_sesion']); //config de token 


//empresas
Route::middleware(['okta_Auth'])->group(function () {
    //
    Route::get('empresas', [EmpresaController::class, 'index']); //view empresas
    //Route::get('empresas', [EmpresaController::class, 'index'])->middleware('okta_Auth'); //view empresas
    Route::get('Addempresa', [EmpresaController::class , 'viewcreate']); //view create empresa"
    Route::post('empresa/saveEmpresa', [EmpresaController::class , 'store']); //view create empresa"
    Route::get('empresa/registrar/{id}', [EmpresaController::class, 'registrarEmpresa']);
    //modal seleccion de empresa para realizar operaciones
    Route::get('empresas/edit/{id}', [EmpresaController::class, 'edit']); // view edit empresas
    Route::get('empresas/actualizar/{id}', [EmpresaController::class, 'update']); //actualiza los datos
    Route::get('empresa/delete/{id}', [EmpresaController::class, 'destroy']); //delete empresa
});

//usuarios

    // Route::middleware(['okta_Auth'])->group(function () {
    //     Route::get('usuarios', [UsuarioController::class, 'index']); //view empresas
    //     Route::get('usuarios/create', [UsuarioController::class, 'create']); //registrar usuario
    //    
    // });

//coves
Route::middleware(['okta_Auth'])->group(function () {
    Route::get('coves/{id}', [CovesController::class, 'index']); //view coves pendiente hay que mandar un id
    Route::get('cargar_cove', [CovesController::class, 'cargar_cove']); //view cargar cove
    Route::post('upload_cove/{id}', [CovesController::class,'upload_cove']); //logica para cargar un cove
    Route::get('cove/unsigned_cove/{id}', [CovesController::class, 'show_unsigned_cove']); //view asignar covea expediente.
    Route::get('asigna_cove/{id_cove}/{id_expediente}', [CovesController::class, 'asigna_coveExpediente']); //add cove
    Route::get('cove_factura/{id_cove}/{id_expediente}', [CovesController::class,  'coveFacturaShow']); //ver detalles de cove
    Route::get('/pdf_cove/{id}/{expediente_id}/{empresa}', [CovesController::class, 'descargarPDFCove']); //logica descarga pdf
    Route::get('/xml_cove/{id}/{expediente_id}/{empresa}', [CovesController::class, 'descargarXMLCove']); //logica descarga xml
});

//Pedimentos
Route::middleware(['okta_Auth'])->group(function () {
    Route::get('pedimentos/{id}', [PedimentoController::class, 'index']); //view list pedimento
    Route::get('cargar_pedimento/{id}', [PedimentoController::class, 'cargar_pedimento']); //view upload pedimento
    Route::post('upload_pedimento/{id}', [PedimentoController::class, 'upload_pedimento']); //logical upload pedimetno
    
    Route::get('facreviewMatch', [PedimentoController::class, 'matchFacreview']); //view reporte
    Route::get('asigna_pedimentos/{idpedimetno}/{id_expediente}', [PedimentoController::class, 'asigna_pedimentos']);//asigna pedietno a un expediente
    Route::get('xml_pedimento/{pedimento_id}',[PedimentoController::class, 'pedimentosXML']); //descarga de xml
    Route::get('pedimento_vista/{id}/{expediente_id}', [PedimentoController::class, 'pedimento_vista']); //vista del pedimento
    Route::get('pdf_pedimento/{id}', [PedimentoController::class, 'pedimentosPDF']); //descarga pdf

    Route::get('reporte', [PedimentoController::class, 'reporte']); //view reporte
    Route::get('reporte/pedimento/detalles/{id}', [PedimentoController::class, 'pedimentoDetails']); //vista del pedimento
});

////sftp////programacion_pedimento/create
Route::middleware(['okta_Auth'])->group(function () {
    Route::get('programacion_pedimento', [JobUploadPedimentoController::class, 'index']);  // Se carga la vista para iniciar la descarga de SFTP
    Route::get('programacion_pedimento/create', [JobUploadPedimentoController::class, 'create']);  // vista crear nuevo SFTP
    Route::get('programacion_pedimento/edit/{id}', [JobUploadPedimentoController::class, 'edit']);  // vista editar SFTP
    Route::post('programacion_pedimento/update/{id}', [JobUploadPedimentoController::class, 'update']);  // logica actualizar SFTP
    Route::post('programacion_pedimento/store', [JobUploadPedimentoController::class, 'store']);  // logica nuevo SFTP
});


//expedientes
Route::middleware(['okta_Auth'])->group(function () { 
    Route::get('expedientes', [ExpedienteController::class, 'index']); //view list expediente
    Route::get('expedientes/create', [ExpedienteController::class, 'create']); //view create expediente
    Route::get('expedientes/edit/{id}', [ExpedienteController::class, 'edit']); //view edit expediente
    Route::post('expedientes/update/{id}', [ExpedienteController::class, 'update']); //logical update expediente
    Route::get('expedientes/show/{id}', [ExpedienteController::class, 'show']); //view edit expediente
    Route::get('expediente/pedimento/unsigned/{id}', [ExpedienteController::class, 'pedimentosUnsigned']); //view edit expedienteasigna_pedimentos
    Route::post('expedientes/store', [ExpedienteController::class, 'store']); //logical update expediente
    Route::get('expediente/carga/{id}', [ExpedienteController::class, 'cargaDocumentos']);//cargar cove o pedimento
    Route::get('estado_cuenta/{id}/{tipo_estado}',[ExpedienteController::class, 'estado_cuenta']); //obtine datos del estado de cuenta
    Route::get('aplicar_pago/{id}',[ExpedienteController::class, 'aplicarPagoCreate']); //se aplicara pago
    Route::get('expedientes/descarga',[ExpedienteController::class, 'filtro_expedientes']); //descarga masiva
    Route::get('expediente/filtro_expedientes',[ExpedienteController::class, 'filtro_expedientes']); //descarga masiva

});

//Facturas
Route::middleware(['okta_Auth'])->group(function () {
    Route::get('facturas/{id}', [FacturasController::class, 'show']); //view list facturas expediente
    Route::get('/subir_facturas/{id}/{tipo}', [FacturasController::class, 'subirFacturaPagos']); //view subir facturas
    Route::post('uploadFactura/{id_empresa}', [FacturasController::class, 'uploadFiles']); //logivca carga de archvivos
    Route::get('/vista_factura/{id}/{id_expediente}', [FacturasController::class, 'show_facturaCargada']); //vista de la facturas cargadas
    Route::get('/factura/download/{id}', [FacturasController::class, 'download']); //descarga el xm de la factura
    Route::get('/pdf_factura/{id}', [FacturasController::class, 'facturaPDF']); //decarga  de pdf de la factura
});

//documentos
Route::middleware(['okta_Auth'])->group(function () {
    Route::get('create_document/{id}',[DocumentoController::class, 'createDocument']); //view carga de documentos
    Route::post('documentos',[DocumentoController::class, 'store']); // logica de carga de documentos
    Route::get('descargar_documentos/{expediente_id}', [DocumentoController::class, 'zipFile']); //descarga de docmentos en general
    Route::delete('eliminar/{id}',[DocumentoController::class, 'destroy']); // logica de carga de documentos
    Route::get('descargar_documento/{documento_id}', [DocumentoController::class, 'descargar_documento']); //descargar documento individual
    Route::put('update/{id}',[DocumentoController::class, 'update']); // logica de carga de documentos
});

//Registrar Pago/Anticipo
Route::middleware(['okta_Auth'])->group(function () {
    Route::get('movimientos/pago/register/{id}', [MovimientoController::class, 'getFormPago']); //abrir modal para registrar pago
    Route::get('movimientos/anticipo/register/{id}', [MovimientoController::class, 'getFormAnticipo']); //abrir modal para registrar pago
    Route::post('movimientos/store/{id}', [MovimientoController::class, 'store']); //logica para registrar pago/anticipo
});

Route::get('usuarios/empresas', [UsuarioController::class, 'empresas']); //view empresas modal
Route::get('agentes', [AgenteController::class, 'index'])->middleware('okta_Auth');//agentes


//Route::get('/aplicar_pago/{id}','ExpedienteController@aplicarPagoCreate');







