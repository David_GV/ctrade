<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApiController; //jwt
use App\Http\Controllers\ProductController; //jwt

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

//Route::get('api_login', 'App\Http\Controllers\OktaController@ApiVerify'); //insert

Route::middleware(['jwt'])->group(function () {
//==================================Empresas===========================================
    Route::get('getEmpresas', 'App\Http\Controllers\EmpresaController@getEmpresa'); //select
    Route::post('insertEmpresa', 'App\Http\Controllers\EmpresaController@insertEmpresa'); //insert
    Route::put('updateEmpresa/{id}', 'App\Http\Controllers\EmpresaController@updateEmpresa'); //edit
    Route::delete('deleteEmpresa/{id}', 'App\Http\Controllers\EmpresaController@deleteEmpresa');//delete
    Route::get('getEmpresaId/{id}', 'App\Http\Controllers\EmpresaController@getEmpresaId');//get empresa por id

//=====================================agencias=================================
    Route::get('AgenciaId/{id}', 'App\Http\Controllers\MovimientoController@AgenciaId'); //obtiene los datos de la agencia por id
    Route::get('getAgentes', 'App\Http\Controllers\AgenteController@getAgentes'); //obtenemos la lista de agentes

//=====================================pedimentos===============================
    Route::get('getpedimento/{id}', 'App\Http\Controllers\PedimentoController@getPedimento'); //obtener lista de pedimento por empresa
    Route::post('insertPedimento', 'App\Http\Controllers\PedimentoController@insertPedimento'); //guardar pedimento
    Route::post('insertPedimAsig', 'App\Http\Controllers\PedimentoController@insertPedimAsignado'); //guardar pedimentoAsignado
    Route::get('getpedimentoId/{id}', 'App\Http\Controllers\PedimentoController@getPedimentoId');//obtiene el pedimento por el id
    Route::put('updatePedimentoExpediente/{id}', 'App\Http\Controllers\PedimentoController@updatePedimentoExpediente');//actualiza el id expediente
    Route::get('getPedimentoEmpresa/{id}', 'App\Http\Controllers\PedimentoController@getPedimentoEmpresa');// consulta de empresa pedimento por id pedimento
    Route::get('getPedimentoIdExpediente/{id}', 'App\Http\Controllers\PedimentoController@getPedimentoIdExpediente');// obtener pedimetnos por id del expediente
    Route::get('getPedimentosAsignados/{id}', 'App\Http\Controllers\PedimentoController@getPedimentosAsignados');// pedimetnos asigandos a la empresa
    Route::get('PedimentoAduanaExpedienteId/{id}/{id_empresa}', 'App\Http\Controllers\PedimentoController@getPedimentosAsignados'); //pedimetnod por id expediente y empresa
//===============================datos de conexion sftp==========================
    Route::get('getsftp/{id}', 'App\Http\Controllers\JobUploadPedimentoController@getSFTP'); //cargar conexion sftp por empresa
    Route::post('insertSFTP', 'App\Http\Controllers\JobUploadPedimentoController@insertSFTP'); //crear conexion sftp
    Route::get('getsftpid/{id}', 'App\Http\Controllers\JobUploadPedimentoController@getSFTPid'); //cargar datos para la modificacion
    Route::put('update/{id}', 'App\Http\Controllers\JobUploadPedimentoController@updateSFTP'); //cargar datos para la modificacion

//===================================Coves========================================
    Route::get('getCove/{id}', 'App\Http\Controllers\CovesController@getCove'); //obtener lista de coves por empresa
    Route::post('insertCove', 'App\Http\Controllers\CovesController@GuardarCove'); //guardar cove
    Route::get('CoveExpediente/{id}', 'App\Http\Controllers\CovesController@CoveExpediente');//consulta de cove por empresa sin expediente
    Route::get('getCoveId/{id}', 'App\Http\Controllers\CovesController@getCoveId');//obtiene el cove por su id
    Route::get('getCoveIdExpediente/{id}', 'App\Http\Controllers\CovesController@getCoveIdExpediente');//obtiene el cove por id del expediente
    Route::get('getCovePedimentoByidExp/{id}', 'App\Http\Controllers\CovesController@getCovePedimentoByidExp'); //union con pedimentos y expedientes
    Route::put('updateCoveIdExpediente/{id}', 'App\Http\Controllers\CovesController@updateCoveIdExpediente'); //actualiza el id del expediente

});



//>>>>>>>>>>>>>>>>usuarios>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//Route::get('getUsuarios', 'App\Http\Controllers\UsuarioController@getUsuario'); //obtener usuario
//Route::post('insertUsuarios', 'App\Http\Controllers\UsuarioController@insertUsuario'); //insertar nuevo usuario
//Route::get('getTypeUser', 'App\Http\Controllers\UsuarioController@getTypeUser'); //obtener tipo de usuario






Route::middleware(['jwt'])->group(function () {
    //============================expedientess=========================
    Route::get('detalles-expediente/{id}/{id_empresa}', 'App\Http\Controllers\ExpedienteController@detallesExpediente'); //select

    Route::get('getExpedientes/{id}', 'App\Http\Controllers\ExpedienteController@getExpediente'); //select
    Route::post('insertExpedientes', 'App\Http\Controllers\ExpedienteController@insertExpediente'); //insert
    Route::post('updateExpedientes/{id}', 'App\Http\Controllers\ExpedienteController@updateExpediente'); //edit Agentes
    Route::get('agentes', 'App\Http\Controllers\ExpedienteController@Agentes'); //select agentes aduanales
    Route::get('idExpediente', 'App\Http\Controllers\ExpedienteController@IdExpediente'); //trae el ultimo id del expediente
    Route::get('ConfigEmpresa/{id}', 'App\Http\Controllers\ExpedienteController@ConfigEmpresa'); //obtengo la config de la empresa en sesion
    Route::get('expedienteId/{id}', 'App\Http\Controllers\ExpedienteController@expedienteId'); //obtengo la informacion del expediente
    Route::get('getExpedienteById/{id}', 'App\Http\Controllers\ExpedienteController@getExpedienteById'); //obtiene datso de expediente union agencia
    Route::get('getPedimUsigned/{id}', 'App\Http\Controllers\ExpedienteController@pedimenUnsigned'); //obtiene pedimentos no asignados
    Route::put('updateExpedienteAduana/{id}', 'App\Http\Controllers\ExpedienteController@updateExpedienteAduana'); //agregar aduana para el expediente
    Route::get('pagosFacturasEstCta/{id}', 'App\Http\Controllers\ExpedienteController@pagosFacturasEstCta'); //agregar aduana para el expediente
    Route::get('expedienteXFecha/{id__empresa}', 'App\Http\Controllers\ExpedienteController@expedienteXFecha'); //trae empresa por fecha y empresa

    //================================Facturas==================================================
    Route::get('getFacturaExpediente/{id}', 'App\Http\Controllers\FacturasController@getFacturaExpediente');//lista de facturas por id de expediente
    Route::get('folderEmpresa/{id}', 'App\Http\Controllers\FacturasController@folderEmpresa'); //datos de la empresa para uso de factura
    Route::post('SaveFacturas', 'App\Http\Controllers\FacturasController@SaveFacturas'); //guarda datos de la factura.
    Route::get('FacturasCargadasId/{id}', 'App\Http\Controllers\FacturasController@FacturasCargadasId'); //obtiene facturas cargadas por su id
    Route::put('UpdateFacturaCargada', 'App\Http\Controllers\FacturasController@UpdateFacturaCargada'); //modifica el estado de la fatudara cargada
    Route::get('sumaFacturaExpediente/{id}', 'App\Http\Controllers\FacturasController@sumaFacturaExpediente'); //suma de factura por expediente
    Route::get('pagosFacturasEstCta/{id}', 'App\Http\Controllers\FacturasController@pagosFacturasEstCta'); //pagos estado de cta. por id expediente.
    Route::get('FacturasCargadasId_idExpediente/{id}/{ids}', 'App\Http\Controllers\FacturasController@FacturasCargadasId_idExpediente'); //factauras cargadas por id y id_expediente

    //===================================Documentos=========================================
    Route::post('SaveDocument', 'App\Http\Controllers\DocumentoController@SaveDocument');//guarda nombre, idexpediente
    Route::get('getDocument/{id}', 'App\Http\Controllers\DocumentoController@getDocument');//documentos por id_expediente
    Route::get('getDocumentoid/{id}', 'App\Http\Controllers\DocumentoController@getDocumentoid');//documentos por su id
    Route::delete('deleteDocumento/{id}', 'App\Http\Controllers\DocumentoController@deleteDocumento');//eliminar documentos por su id
    Route::put('updateDocumet/{id}', 'App\Http\Controllers\DocumentoController@updateDocumet'); //actalizar nota

    //===================================registrar pago======================================
    Route::get('getFacturas/{id}', 'App\Http\Controllers\MovimientoController@getFacturas'); //guarda nombre, idexpediente

    //===================================tipo de pagos==================================
    Route::get('TipoPago', 'App\Http\Controllers\MovimientoController@TipoPago');

    //=====================================Movimientos==================================
    Route::post('insertMovimiento', 'App\Http\Controllers\MovimientoController@insertMovimiento');//registrar movimiento
    Route::get('getMovimientoIdexpediente/{id}', 'App\Http\Controllers\MovimientoController@getMovimientoIdexpediente');//obtenemos movimiendo por id expediente y tipo 1
    Route::get('getMovimientoExp2/{id}', 'App\Http\Controllers\MovimientoController@getMovimientoExp_2');//obtenemos los anticipos
    Route::get('movimientoCtaGastaos/{id}', 'App\Http\Controllers\MovimientoController@movimientoCtaGastaos'); //obtenemos cuenta de gastos
    Route::get('movimientosComprobantes/{id}', 'App\Http\Controllers\MovimientoController@movimientosComprobantes'); //obtenemos m. comprobantes
    Route::put('UpdateMontoP/{id}', 'App\Http\Controllers\FacturasController@UpdateMontoP'); //actualiza monto pagado del movimiento
    Route::get('pagosFacturasExp/{id}', 'App\Http\Controllers\MovimientoController@pagosFacturasExp'); //pagos por expediente
});


Route::middleware(['jwt'])->group(function () {
    Route::post('get-empresas', 'App\Http\Controllers\UsuarioController@getEmpresas'); //view empresas modal

    Route::get('reporte-pedimentos','App\Http\Controllers\PedimentoController@getReportePedimentos');
});
