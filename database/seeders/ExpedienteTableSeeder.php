<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Expediente;
class ExpedienteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csvFile = fopen(base_path().'/database/seeders/csvs/expedientes-demo.csv', "r");
        $firstline = true;
        while (($data = fgetcsv($csvFile, 2000, "|")) !== FALSE) {
            if (!$firstline) {
                Expediente::create([
                    "id" => $data['0'],
                    "expediente" => $data['1'],
                    "nombre" => $data['2'],
                    "descripcion" => $data['3'],
                    "agente_aduanal" => $data['4'],
                    "aduana_id" => $data['5'],
                    "empresa_id" => $data['6'],
                    "status" => $data['7'],
                ]);
            }
            $firstline = false;
        }
        fclose($csvFile);
    }
}
