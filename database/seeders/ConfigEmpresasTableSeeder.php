<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ConfigEmpresa;

class ConfigEmpresasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csvFile = fopen(base_path().'/database/seeders/csvs/config_empresas.csv', "r");
        $firstline = true;
        while (($data = fgetcsv($csvFile, 2000, "|")) !== FALSE) {
            if (!$firstline) {
                ConfigEmpresa::create([
                    "id" => $data['0'],
                    "empresa_id" => $data['1'],
                    "configuracion" => $data['2'],
                    "value" => $data['3']
                ]);
            }
            $firstline = false;
        }
        fclose($csvFile);
    }
}
