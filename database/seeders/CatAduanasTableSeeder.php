<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Aduana;

class CatAduanasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csvFile = fopen(base_path().'/database/seeders/csvs/catalogoaduanas.csv', "r");

        $firstline = true;
        while (($data = fgetcsv($csvFile, 2000, "|")) !== FALSE) {
            if (!$firstline) {
                Aduana::create([
                    "id" => $data['0'],
                    "aduana" => $data['1'],
                    "seccion" => $data['2'],
                    "denominacion" => $data['3'],
                    "compuesto" => $data['4']
                ]);
            }
            $firstline = false;
        }

        fclose($csvFile);
    }
}
