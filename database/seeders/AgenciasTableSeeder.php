<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Agencia;

class AgenciasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Agencia::truncate();

        $csvFile = fopen(base_path().'/database/seeders/csvs/agencias.csv', "r");
        $firstline = true;
        while (($data = fgetcsv($csvFile, 2000, "|")) !== FALSE) {
            if (!$firstline) {
                Agencia::create([
                    "id" => $data['0'],
                    "nombre" => $data['1'],
                    "rfc" => $data['2']
                ]);
            }
            $firstline = false;
        }
        fclose($csvFile);
    }
}
