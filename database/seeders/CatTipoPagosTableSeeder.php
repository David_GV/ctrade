<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\CatalogoTipoPago;

class CatTipoPagosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csvFile = fopen(base_path().'/database/seeders/csvs/CatalogoTipoPagos.csv', "r");
        $firstline = true;
        while (($data = fgetcsv($csvFile, 2000, "|")) !== FALSE) {
            if (!$firstline) {
                CatalogoTipoPago::create([
                    "id" => $data['0'],
                    "nombreTipo" => $data['1'],
                ]);
            }
            $firstline = false;
        }
        fclose($csvFile);
    }
}
