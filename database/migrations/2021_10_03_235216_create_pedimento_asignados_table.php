<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePedimentoAsignadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedimento_asignados', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_pedimento'); // codigo modificado laravel 8
            $table->unsignedBigInteger('id_expediente'); //codigo modificado 
            $table->foreign('id_pedimento')->references('id')->on('pedimentos');
            $table->foreign('id_expediente')->references('id')->on('expedientes');
            $table->index('id','idx_pedimAsignados');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pedimento_asignados');
    }
}
