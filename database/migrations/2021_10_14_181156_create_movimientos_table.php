<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMovimientosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movimientos', function (Blueprint $table) {
            $table->id();
            $table->string('tipo',20)->nullable();
            $table->integer('idTipo');
            
            $table->float('monto_factura')->nullable();  //monto real de la factura
            $table->float('monto_pagado')->nullable();   //monto pagado
            $table->float('monto_anterior')->nullable(); //monto inicial
            $table->string('rfc')->nullable();
            $table->integer('idExpediente');
            $table->date('fechaPago');
            $table->string('uidPago');

            $table->unsignedBigInteger('id_agencia');
            $table->integer('id_empresa')->unsigned();

            $table->foreign('id_agencia')->references('id')->on('agencias');
            $table->foreign('id_empresa')->references('id')->on('empresas');

            $table->string('transaccionCP')->nullable();
            $table->string('polizaContable')->nullable();
            $table->integer('id_facturacargada');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movimientos');
    }
}
