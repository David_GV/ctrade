<?php

namespace App\Repositories\PedimentoExternal;

/**
 * Interface IPedimentoExternalRepository.
 */
interface IPedimentoExternalRepository
{
    /**
     * @param array $conditions
     * @param $paginate
     *
     * @return mixed
     */
    public function facReviewInCt(array $conditions, $paginate);

    /**
     * @param array $conditions
     * @param $paginate
     *
     * @return mixed
     */
    public function facReviewNotInCt(array $conditions, $paginate);

    /**
     * @param array $conditions
     *
     * @return mixed
     */
    public function totalPedimentosFr(array $conditions);

    /**
     * @param array $conditions
     *
     * @return mixed
     */
    public function totalPedimentosEncontrados(array $conditions);

    /**
     * @param array $conditions
     *
     * @return mixed
     */
    public function totalPedimentosNoEncontrados(array $conditions);
}
