<?php

namespace App\Repositories\Pedimento;
/**
 * Interface IPedimentoRepository.
 */
interface IPedimentoRepository
{
    /**
     * @param $empresaId
     * @param $anio
     * @param $mes
     *
     * @return mixed
     */
    public function pedimentosPeriodo($empresaId, $anio, $mes);

    /*
     * @param int id_pedimento, id_expdiente
    */
    public function asignarPedimento($id_pedimento,$id_expediente,$id_aduana);

    /*
     * @param int id_pedimento
     *
     * @return mixed*/
    public function getPedimentoEmpresa($id_pedimento);

    /**
     * @param array $data
     *
     * @return mixed
     */
    public function updateOrCreate(array $data);

    /**
     * @param $pedimento
     *
     * @return mixed
     */
    public function findByPedimento($pedimento);


    /**
     * @param $id
     *
     * @return mixed
     */
    
    public function findById($id);

    /**
     * @param $id
     *
     * @return mixed
     */
    
    public function PedimReturnJson($id);

    /**
     * @param $idPedimento
     * @param $idExpediente
     *
     * @return mixed
     */
    
    public function findByIdExpediente($idPedimento,$idExpediente);
    /**
     * @param Sessions
     * @return mixed
     */
    public function findByIdEmpresa();
    /**
     * @param Sessions
     * @return mixed
     */
    public function getPedimentosAsignados($id);

    //public function getIdPedimento($id_empresa,$pedimento,$aduanaDespacho,$impExpNombre,$tipoOperacion,$expedienteId,$archivoM);

}


