<?php

namespace App\Repositories\Aduana;

interface IAduanaRepository
{
    /**
     * @return mixed
     */
    public function getAll();
}
