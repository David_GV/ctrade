<?php

namespace App\Repositories\Aduana;

use App\Aduana;

class EloquentAduanaRepository implements IAduanaRepository
{
    /**
     * @return mixed
     */
    public function getAll()
    {
        return Aduana::all();
    }
}
