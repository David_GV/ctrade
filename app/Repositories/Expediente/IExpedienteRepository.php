<?php

namespace App\Repositories\Expediente;

/**
 * Interface IExpedienteRepository
 * @package App\Repositories\Expediente
 */
interface IExpedienteRepository
{
    /**
     * @param int $id
     * @return mixed
     */
    public function getExpedientesByEmpresaId(int $id);

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data);

    /**
     * @param int $id
     * @return mixed
     */
    public function getExpedienteById(int $id);

    /**
     * @param array $data
     * @param int $id
     * 
     * @return mixed
     */
    public function update(array $data, int $id);

    /**
     * @param array $data
     * @param int $id
     * 
     * @return mixed
     */
    public function pagosFacturasExp($id);
    
    /**
     * Consulta los pagos de las facturas y realiza las operaciones para calcular el SALDO final del estado de cuenta
     * @param  $id 
     * @return array()
     */
    public function pagosFacturasEstCta($id);

}
