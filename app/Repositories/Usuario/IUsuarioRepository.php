<?php

namespace App\Repositories\Usuario;

interface IUsuarioRepository
{
    public function all();
    public function userAgencia($id_permiso,$id);
    public function find($id);
    public function findOrFail($id);
    public function create(array $data);
    public function update(array $data, $id);
    public function delete($id);
    public function asignarEmpresa($usuarioId, $empresaId);
    public function desasignarEmpresa($usuarioId, $empresaId);
    public function usuariosByPermission();
}
