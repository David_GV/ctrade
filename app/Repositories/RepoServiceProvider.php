<?php

namespace App\Repositories;

use Illuminate\Support\ServiceProvider;

class RepoServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('App\Repositories\Aduana\IAduanaRepository', 'App\Repositories\Aduana\EloquentAduanaRepository');
        $this->app->bind('App\Repositories\Agencia\IAgenciaRepository', 'App\Repositories\Agencia\EloquentAgenciaRepository');
        $this->app->bind('App\Repositories\Cove\ICoveRepository', 'App\Repositories\Cove\EloquentCoveRepository');

        $this->app->bind('App\Repositories\Empresa\IEmpresaRepository', 'App\Repositories\Empresa\EloquentEmpresaRepository');
        $this->app->bind('App\Repositories\Incoterms\IIncotermsRepository', 'App\Repositories\Incoterms\EloquentIncotermsRepository');
        $this->app->bind('App\Repositories\Pedimento\IPedimentoRepository', 'App\Repositories\Pedimento\EloquentPedimentoRepository');
        $this->app->bind('App\Repositories\PedimentoExternal\IPedimentoExternalRepository', 'App\Repositories\PedimentoExternal\EloquentPedimentoExternalRepository');
        $this->app->bind('App\Repositories\Usuario\IUsuarioRepository', 'App\Repositories\Usuario\EloquentUsuarioRepository');
        $this->app->bind('App\Repositories\Expediente\IExpedienteRepository', 'App\Repositories\Expediente\EloquentExpedienteRepository');
    }
}
