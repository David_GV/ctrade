<?php

namespace App\Repositories\Empresa;

interface IEmpresaRepository
{
    public function all();
    public function find($id);
    public function findOrFail($id);
    public function update(array $data, $id);
    public function create(array $data);
    public function destroy($id);
	public function empresasByPermission();
}
