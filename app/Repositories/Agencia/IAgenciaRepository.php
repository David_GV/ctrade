<?php

namespace App\Repositories\Agencia;

interface IAgenciaRepository
{

    public function getAll();
    public function agenciasByPermission();
    public function agenciasByIds($ids);
}
