<?php

namespace App\Repositories\Cove;

interface ICoveRepository
{
    /**
     * @return mixed
     */
    public function getAll();
    public function getCovePedimentoByidExp($id);
}
