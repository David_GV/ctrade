<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\Documento;
use Illuminate\Support\Facades\Http;
use Session;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\File as FacadesFile;
use ZipArchive;
use File;
use App\Http\Controllers\apiConfigController;

class DocumentoController extends Controller
{
    public function Urldominio(){
        $dominio =  app(apiConfigController::class)->dominio();
       // dd($dominio);
      // $this->Urldominio().
        return $dominio;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      
        $file = $request->file('files');
        $fileName = $file->getClientOriginalName();
        
        //obtenemos la configuracion de los datos de la empresa logiada
          //obtener datos de l $fileNamea empresa segun id.
          $id_empresa = Session::get('id');
         //url de l a api
         $url =  $this->Urldominio()."api/ConfigEmpresa/".$id_empresa;
          //tipo detoken
          $type = Session::get('token_type');
          //token
          $token = Session::get('api_token');
          //peticion get
          $curl = curl_init();
               curl_setopt_array($curl, array(
                      CURLOPT_URL => $url,
                      CURLOPT_RETURNTRANSFER => true,
                      CURLOPT_ENCODING => '',
                      CURLOPT_MAXREDIRS => 10,
                      CURLOPT_TIMEOUT => 0,
                      CURLOPT_FOLLOWLOCATION => true,
                      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                      CURLOPT_CUSTOMREQUEST => 'GET',
                      CURLOPT_HTTPHEADER => array(
                          'Content-type: application/x-www-form-urlencoded',
                          'Authorization: '.$type.' '.$token,
                          'Cookie: DT=DI0EIjjLIBMTtSqxAYujXFV6A'
                  ),
               ));
               $response = curl_exec($curl);
               curl_close($curl);
               $folderEmpresa =json_decode($response, true);
          $expediente_id = $request->input('expediente_id');
        
        $path = $folderEmpresa['value'].'/'.$expediente_id.'/Documentos_soporte/';
       
        $saved_file = Storage::put($path.$fileName, file_get_contents($file->getPathName()));
        $nota = "S/N";
        if( $saved_file ){
        /*if ($file->move($path, $fileName)) {*/
          //  $data_file = array( );
            $array = [
                "expediente_id" => $expediente_id,
                "nota" => $nota,
                "nombreDocumento" => $fileName
            ];
           // dd($data_file);
            //echo "se movio correctamente el archivo";
            try {

                $api = $this->Urldominio()."api/SaveDocument";
                //consumir la api de guardar los datos en la base de datos
                $data_json = json_encode($array);
                //realizamos la peticion a la api y mandamos valores
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $api);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: '.$type.' '.$token));
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $response  = curl_exec($ch);
                curl_close($ch);
                $documento = json_decode($response, true);
                //dd($documento);
                echo 'Se movio correctamente el archivo ' . $fileName;
                /*return redirect()->route('expediente.show', ['id' => $expediente_id]);*/
            } catch (Exception $e) {
                 echo $e;
            }
        } else {
             echo "hubo un error";
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
            //tipo de token
        $type = Session::get('token_type');
            //token
        $token = Session::get('api_token');
        $data = $request->all();
        $array = ["nota" =>  $data['note']];
        //url de api mpdificar
        $url = $this->Urldominio()."api/updateDocumet/".$id;
        //enviamos los datos a la api para el update
        $data_json = json_encode($array);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: '.$type.' '.$token, 'Content-Length: ' . strlen($data_json), ));
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response  = curl_exec($ch);
        curl_close($ch);
        $documento =json_decode($response, true);
        if($documento){
            return response()->json('OK');
        } else {
            // TODO: Enviar error de no actualizado.
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) //id documento
    {
        $type = Session::get('token_type');
                //token
        $token = Session::get('api_token');         
        //dd($id);
        $id_empresa = Session::get('id'); 
        $posteado = $id;
        $folderEmpresa = $this->folderEmpresa($id_empresa);
        $document = $this->obtenerDocumento($id);


        $file = storage_path('app\\'.$folderEmpresa['value'].'\\'.$document['expediente_id'].'\\Documentos_soporte\\'.$document['nombreDocumento']);
        if(is_file($file)){

            unlink($file);
            //eliminamos el registro de la api
            $doc = HTTP::withToken($type." ".$token)->delete($this->Urldominio()."api/deleteDocumento/".$id."?token=".Session::get('token'));


            if ($doc['mensaje'] == 1){
                $success = 'delete success';
                return response()->json($success);
            } else {
                return 'delete failed';
            }
        }else {
            echo 'El directorio no existe';
        }

    }

    #region api

        // guardamos un nuevo documento
        public function SaveDocument(Request $request){
            $expediente = Documento::create($request->all());
            return response()->json($expediente, 200);
        }

        //obtener lista de documentos por id de expediente
        public function getDocument($id){
            $documentos = Documento::getDocument($id);
            return response()->json($documentos, 200);
        }

        //obtener datos del coumento por su id
        public function getDocumentoid($documento_id){
            $documento = Documento::find($documento_id);
            return response()->json($documento, 200);
        }

        public function deleteDocumento($id){
            //busca la empresa
             $document = Documento::find($id);
     
             if(is_null($document)){
                 return response()->json(['mensaje'=>'0'], 404);
             }
            if($document->delete()){
             return response()->json(['mensaje'=>'1'], 200);
            }else{
                return response()->json(['mensaje'=>'2'], 502);
            }  
         }

         //modificar nota del documento
         public function updateDocumet(Request $request, $id){ //id del pedimento
            $nota = $request['nota'];
            $documento = Documento::where('id' ,$id)->update(['nota' => $nota]);
            return response()->json($documento, 200);
        }

    #endregion api
    #region view
        public function createDocument($expediente_id)
        {
            return view('documentos.create')
                                ->with("expediente_id", $expediente_id);
        }
    #endregion

    #region method logical
            //descarga de documentos individuales
            public function descargar_documento($documento_id){
                $id_empresa = Session::get('id'); 
                $folderEmpresa = $this->folderEmpresa($id_empresa);
                $rfc = $folderEmpresa['value'];

                $documento = $this->obtenerDocumento($documento_id);

                $expediente_id = $documento['expediente_id'];
                $fileName = $documento['nombreDocumento'];

                $path = $rfc . '/' . $expediente_id . '/Documentos_soporte/' . $fileName ;

                $content = Storage::get($path);
                $mime = Storage::mimeType($path);

                if($mime === 'application/pdf') {
                    return (new Response($content, 200))
                        ->header('Content-Type', $mime)
                        ->header('Expires', '0')
                        ->header('Cache-Control', 'must-revalidate')
                        ->header('Pragma', 'public');
                } else {
                    return (new Response($content, 200))
                        ->header('Content-Type', $mime)
                        ->header('Content-Disposition', 'attachment; filename='.$fileName);

                }
            }

            public function folderEmpresa($id){ //id empresa
                $type = Session::get('token_type');
                //token
                $token = Session::get('api_token');
           
                $folderEmpresa = HTTP::withToken($type." ".$token)->get($this->Urldominio()."api/ConfigEmpresa/".$id."?token=".Session::get('token'));
                $folder = $folderEmpresa->json();
                return $folder;
            }

            public function obtenerDocumento($id){ //id documento
                $type = Session::get('token_type');
                //token
                $token = Session::get('api_token');
                $documento = HTTP::withToken($type." ".$token)->get($this->Urldominio()."api/getDocumentoid/".$id."?token=".Session::get('token'));
                $documento = $documento->json();
                return $documento;
            }

            public function zipFile($id) //id del expediente
            {
                $type = Session::get('token_type');
                //token
                $token = Session::get('api_token');
                $id_empresa = Session::get('id');
                $zip = new ZipArchive; //instanciamos la clase
                //obtenemos la empresa en session
                $folderEmpresa = HTTP::withToken($type." ".$token)->get($this->Urldominio()."api/ConfigEmpresa/".$id_empresa."?token=".Session::get('token'));
                $folderEmpresa = $folderEmpresa->json();
              
                $rfc = $folderEmpresa['value']; //rfc de la empresa
                $folderZip = $rfc.'-'.$id.'.zip'; //nombre del archivo zip

                if($zip->open(public_path($folderZip), ZipArchive::CREATE) === TRUE)//creamos el zip
                { 
                    
                    $files = File::files(storage_path('app/'.$rfc.'/'.$id.'/Documentos_soporte/'));// ubicacion de los archivos
                    //recorremos los archivos existentes
                    foreach($files as $key => $value){
                        $RelativeNameInZipName = basename($value); //obtenemos el nombre del archivo
                        $zip->addFile($value, $RelativeNameInZipName); //agreamos los archivos al zip
                    }
                    //cerramos el zip
                    $zip->close();
                }
                
                //descargamos el zip automaticamente
                return response()->download(public_path($folderZip));
            }
    #endregion 
}
