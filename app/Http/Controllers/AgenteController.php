<?php

namespace App\Http\Controllers;

use App\Models\Agencia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Http\Controllers\apiConfigController;
use Session;

class AgenteController extends Controller
{

    public function Urldominio(){
        $dominio =  app(apiConfigController::class)->dominio();
       // dd($dominio);
        return $dominio;
    }

    #region view
        public function index()
        {
            //obtenemos los agentes (api)
            $url = $this->Urldominio()."api/getAgentes";
            //tipo token
            $type = Session::get('token_type');
            //token
            $token = Session::get('api_token');
            //peticion get
            $curl = curl_init();
                curl_setopt_array($curl, array(
                        CURLOPT_URL => $url,
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => '',
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 0,
                        CURLOPT_FOLLOWLOCATION => true,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => 'GET',
                        CURLOPT_HTTPHEADER => array(
                            'Content-type: application/x-www-form-urlencoded',
                            'Authorization: '.$type.' '.$token,
                            'Cookie: DT=DI0EIjjLIBMTtSqxAYujXFV6A'
                    ),
                ));
                $response = curl_exec($curl);
                curl_close($curl);
                $agencias =json_decode($response, true);
            
                return view("agentes.index")
                            ->with("agencias", $agencias);
        }
    #endregiion view


    #region api
        public function getAgentes(){
            return response()->json(Agencia::all(), 200);
        }
    #endregion api

    #region method logical

    #endregion method logical
}
