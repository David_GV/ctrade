<?php

namespace App\Http\Controllers;

use App\Models\Expediente;
use App\Http\Controllers\apiConfigController;
use App\Models\Agencia;
use App\Models\Api;
use App\Models\ConfigEmpresa;
use App\Models\Pedimento;
use App\Models\Aduana;
use App\Repositories\Aduana\IAduanaRepository;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use File;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use PhpParser\Node\Expr\FuncCall;
use Session;
use Carbon\Carbon;
use App\Collector\Collector;
use ZipArchive;
use RecursiveIteratorIterator;
use RecursiveDirectoryIterator;

class ExpedienteController extends Controller
{
    public function Urldominio()
    {
        $dominio =  app(apiConfigController::class)->dominio();
        // dd($dominio);
        // $this->Urldominio().
        return $dominio;
    }
    #region method view
    //view list of expedientes
    public function index()
    {
        //id de la empresa en session
        $id_empresa = Session::get('id');
        //hacemos uso de la api para obtener expedientes por id de empresa
        $route = 'getExpedientes/' . $id_empresa;

        $Api = new Api($route);
        $response = $Api->GET();
        $expedientes = json_decode($response, true);

        return view('expediente.index')->with('expedientes', $expedientes);
    }

    //view create expediente
    public function create()
    {
        $agencias = $this->agentesAll();

        return view('expediente.create')->with('agencias', $agencias);
    }

    //view edit expedientes
    public function edit($id)
    {
        //obtenemos todo los agentes aduanales
        $agencias = $this->agentesAll();
        //obtenemos los datos del expediente a ser editados
        $expediente = $this->getExpedienteId($id);


        return view("expediente.edit")
            ->with('expediente', $expediente)
            ->with('agencias', $agencias);
    }


    //view expediente and functions general
    public function show($id)
    { //id expediente
        //id empresa en session
        $id_empresa = Session::get('id');

        $route = "detalles-expediente/" . $id . "/" . $id_empresa;

        $Api = new Api($route);
        $response = $Api->GET();
        $data = json_decode($response, true);
        if (is_array($data)) {
            $arregloCoves = [];

            foreach ($data["coves"] as $key => $cove) {
                $collection = collect(json_decode($cove['json_cove'], true));
                $collection = new Collector($collection);
                $arregloCoves = $this->array_change_key_case_recursive($collection['comprobantes']);

                // modificamos el formato de fecha
                $fecha = '';
                if (isset($arregloCoves['fechaexpedicion'])) {
                    $fecha = Carbon::createFromFormat('Y-m-d', $arregloCoves['fechaexpedicion'])->format('d/m/Y');
                }

                $arregloCoves['fechaexpedicion'] = $fecha;
                $data["coves"][$key]["data"] = (object)$arregloCoves;
                $data["coves"][$key]["json"] =  (object)json_decode($cove['json'], true);
            }
        }

        return view('expediente.expediente',  $data);




        //tipo de token
        $type = Session::get('token_type');
        //token
        $token = Session::get('api_token');


        //url del expediente seleccionado
        $url_ex = $this->Urldominio() . 'api/getExpedienteById/' . $id;
        //peticion get de api
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url_ex,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Content-type: application/x-www-form-urlencoded',
                'Authorization: ' . $type . ' ' . $token,
                'Cookie: DT=DI0EIjjLIBMTtSqxAYujXFV6A'
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $expediente = json_decode($response, true);
        //configuracion de la carpestar de las empresas
        $folderEmpresa = $this->folder_empresa($id_empresa);

        //consumimos la api que obtiene los coves por id del expediente
        $url_c = $this->Urldominio() . "api/getCovePedimentoByidExp/" . $id;
        //peticion get de api
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url_c,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Content-type: application/x-www-form-urlencoded',
                'Authorization: ' . $type . ' ' . $token,
                'Cookie: DT=DI0EIjjLIBMTtSqxAYujXFV6A'
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $coves = json_decode($response, true);
        //pedimentos asignados al expediente en consulta (api)
        $url_pa = $this->Urldominio() . "api/getPedimentosAsignados/" . $id;
        //peticion get de api
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url_pa,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Content-type: application/x-www-form-urlencoded',
                'Authorization: ' . $type . ' ' . $token,
                'Cookie: DT=DI0EIjjLIBMTtSqxAYujXFV6A'
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $pedimentos_asignados = json_decode($response, true);



        //pedimentos cargados al expediente
        $url_p = $this->Urldominio() . "api/PedimentoAduanaExpedienteId/" . $id . "/" . $id_empresa;
        //peticion get de api
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url_p,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Content-type: application/x-www-form-urlencoded',
                'Authorization: ' . $type . ' ' . $token,
                'Cookie: DT=DI0EIjjLIBMTtSqxAYujXFV6A'
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $pedimentos = json_decode($response, true);



        //consulta de datos de facturas cargadas
        $url_p_e = $this->Urldominio() . "api/getFacturaExpediente/" . $id;
        //peticion get de api
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url_p_e,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Content-type: application/x-www-form-urlencoded',
                'Authorization: ' . $type . ' ' . $token,
                'Cookie: DT=DI0EIjjLIBMTtSqxAYujXFV6A'
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $pagos_External = json_decode($response, true);
        //dd($pagos_External);

        //consulta de anticipos
        $url_ant = $this->Urldominio() . "api/getMovimientoExp2/" . $id;
        //peticion get de api
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url_ant,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Content-type: application/x-www-form-urlencoded',
                'Authorization: ' . $type . ' ' . $token,
                'Cookie: DT=DI0EIjjLIBMTtSqxAYujXFV6A'
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $anticipos = json_decode($response, true);

        //consulta de pagos
        $url_pag = $this->Urldominio() . "api/pagosFacturasExp/" . $id;
        //peticion get de api
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url_pag,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Content-type: application/x-www-form-urlencoded',
                'Authorization: ' . $type . ' ' . $token,
                'Cookie: DT=DI0EIjjLIBMTtSqxAYujXFV6A'
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $pagos = json_decode($response, true);


        //consulta de <documentos>
        $url_doc = $this->Urldominio() . "api/getDocument/" . $id;
        //peticion get de api
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url_doc,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Content-type: application/x-www-form-urlencoded',
                'Authorization: ' . $type . ' ' . $token,
                'Cookie: DT=DI0EIjjLIBMTtSqxAYujXFV6A'
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $documentos = json_decode($response, true);


        $cco = 0;
        $arreglosCoves = [];
        foreach ($coves as $cove) {

            $collection = collect(json_decode($cove['json_cove'], true));
            $collection = new Collector($collection);
            $arregloCoves[$cco] = $this->array_change_key_case_recursive($collection['comprobantes']);

            // modificamos el formato de fecha
            $fecha = '';
            if (isset($arregloCoves[$cco]['fechaexpedicion'])) {
                $fecha = Carbon::createFromFormat('Y-m-d', $arregloCoves[$cco]['fechaexpedicion'])->format('d/m/Y');
            }
            $arregloCoves[$cco]['fechaexpedicion'] = $fecha;
            $cco++;
        }

        return view('expediente.expediente')
            ->with('expediente', $expediente)
            ->with('coves', $coves)
            ->with("folderEmpresa", $folderEmpresa)
            ->with('pedimentos_asignados', $pedimentos_asignados)
            ->with('pedimentos', $pedimentos)
            ->with("pagos_External", $pagos_External)
            ->with("anticipos", $anticipos)
            ->with("pagos", $pagos)
            ->with("documentos", $documentos);
    }


    public function detallesExpediente($id, $id_empresa)
    {
        $folderEmpresa = [];
        $coves = [];
        $pedimentos_asignados = [];
        $pedimentos = [];
        $pagos_External = [];
        $anticipos = [];
        $pagos = [];
        $documentos = [];

        $expediente = Expediente::getExpedienteById($id);

        if ($expediente) {
            $folderEmpresa =   $this->ConfigEmpresa($id_empresa)->original;
            $coves = $expediente->getCovePedimentoByidExp(); //por revisar
            $pedimentos_asignados = $expediente->getPedimentosAsignados();
            $pedimentos = $expediente->PedimentoAduanaExpedienteId();
            $pagos_External = $expediente->getFacturaExpediente();
            $anticipos = $expediente->getMovimientoExp_2();
            $pagos = $expediente->pagosFacturasExp();
            $documentos =  $expediente->getDocument();
        }

        return response()->json([
            "expediente" =>  $expediente,
            "folderEmpresa" => $folderEmpresa,
            "coves" =>  $coves,
            "pedimentos_asignados" =>  $pedimentos_asignados,
            "pedimentos" =>  $pedimentos,
            "pagos_External" =>  $pagos_External,
            "anticipos" =>  $anticipos,
            "pagos" =>  $pagos,
            "documentos" =>  $documentos
        ], 200);
    }



    public function array_change_key_case_recursive($input)
    {
        $input = array_change_key_case($input, CASE_LOWER);
        foreach ($input as $key => $array) {
            if (is_array($array)) {
                $input[$key] = $this->array_change_key_case_recursive($array);
            }
        }
        return $input;
    }

    //view pedimentos no asignados, para signar un pedimento al expediente
    public function pedimentosUnsigned(int $id) //id de expediente
    {
        $id_empresa = Session::get('id');
        //tipo de token
        $type = Session::get('token_type');
        //token
        $token = Session::get('api_token');
        //obtetenmos los pedimentos no asignados a un expediente
        $url = $this->Urldominio() . 'api/getPedimUsigned/' . $id_empresa;
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Content-type: application/x-www-form-urlencoded',
                'Authorization: ' . $type . ' ' . $token,
                'Cookie: DT=DI0EIjjLIBMTtSqxAYujXFV6A'
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $pedimentos = json_decode($response, true);

        return view("pedimento.unsigned")
            ->with("pedimentos", $pedimentos)
            ->with("id_expediente", $id);
    }

    //vista de cargar pedimento o cove
    public function cargaDocumentos($id_expediente)
    {
        return view('documentos.cargar', compact('id_expediente'));
    }
    #endregion

    #region method apis

    //consulta api por adimentos no asignados a un expediente
    public function pedimenUnsigned($id)
    { //id empresa
        $pedimentos = Pedimento::where('empresa_id', $id)
            ->whereNull('expediente_id')
            ->get();
        //$pedimentos = Pedimento::all();
        $pedim = response()->json($pedimentos, 200);
        return $pedim;
    }

    //genrando lista de coves por id de empresa, (api)
    public function getExpediente($id)
    {

        $expediente = Expediente::where('empresa_id', $id)->get();
        if (is_null($expediente)) {
            return response()->json(['Mensaje' => 'registro no encontrado', 400]);
        }
        $expedientes = response()->json($expediente, 200);
        return $expedientes;
    }

    //insertar empresa api
    public function insertExpediente(Request $request)
    {
        $expediente = Expediente::create($request->all());
        return response()->json($expediente, 200);
    }

    //actualizar empresa api
    public function updateExpediente(Request $request, $id)
    {
        try {
            //buscar el row por id
            $expediente = Expediente::find($id);
            if (is_null($expediente)) {
                return response()->json(['mensaje' => '0'], 404);
            }
            // $empresa->update($request->all());
            if ($expediente->update($request->all())) {
                return response()->json(['mensaje' => '1'], 200);
            }
            return response()->json(['mensaje' => '2'], 502);
        }
        catch (Exception $e) {
            return response()->json(['mensaje' => $e], 500);
        }
    }

    //lista de agentes aduanales api
    public function Agentes()
    {
        $agentes = Agencia::all();
        return response()->json($agentes, 200);
    }

    //api, obtener el ultimo id del expediente
    public function IdExpediente()
    {

        $expediente = Expediente::orderBy('id', 'desc')->first();

        if (!isset($expediente)) {
            $expediente = ["id" => 0];
            //si no encontro el id retorna 0
            return response()->json($expediente, 200);
        } else {
            //manda solo el valor del id
            return response()->json($expediente, 200);
        }
    }
    //api, obtiene el nombre de la configuracion de la carpeta, de la empresa en session
    public function ConfigEmpresa($id)
    {
        $folder_empresa = ConfigEmpresa::where('empresa_id', $id)
            ->where('configuracion', 'folder_storage')
            ->first();

        if(!$folder_empresa){
            $folder_empresa = array (
                  "id" => $id,
                  "empresa_id" => $id,
                  "configuracion" => "folder_storage",
                  "value" => 'CVI161117SM6',
                  "created_at" => null,
                  "updated_at" => null,
            );
        }

        return response()->json($folder_empresa, 200);
    }
    //api obtiene la los datos del expediente
    public function expedienteId($id)
    {
        $expediente  = Expediente::find($id);
        return response()->json($expediente, 200);
    }

    //api que trae los datos de la tabla expediente y agencia
    public function getExpedienteById($id)
    {
        $expedienteAgente = Expediente::where('expedientes.id', $id)
            ->leftJoin('agencias', 'expedientes.agente_aduanal', 'agencias.id')
            ->select('expedientes.*', 'agencias.nombre as nombre_agente')->first();

        return response()->json($expedienteAgente, 200);
    }

    //actualizar el campo aduana de un expediente (api)
    public function updateExpedienteAduana(Request $request, $id)
    { //id del expediente
        $id_aduana = $request['aduana_id'];
        $updateExpediente = Expediente::where('id', $id)->update(['aduana_id' => $id_aduana]);
        return response()->json($updateExpediente, 200);
    }


    public function pagosFacturasEstCta($id)
    {
        $facturasCargadas = DB::table('facturas_cargadas')
            ->leftJoin('movimientos', 'movimientos.id_facturacargada', '=', 'facturas_cargadas.id')
            ->select(
                'facturas_cargadas.tipo_factura',
                'json_cfdi',
                'total as total_factura',
                'monto_pagado',
                'facturas_cargadas.id',
                'movimientos.id as id_mov',
                'facturas_cargadas.status_factura',
                DB::raw('case
                        when total - monto_pagado is null then "0"
                        else total - monto_pagado
                        end as saldo')
            )
            ->where('id_expediente', $id)
            ->get();
        return response()->json($facturasCargadas, 200);
    }

    //
    public function expedienteXFecha(Request $request, $id_empresa)
    {
        $data = $request->all();
        $expedientes = Expediente::getExpedienteByEmpresaAndDate($id_empresa, $data['inicio'], $data['final']);

        return response()->json($expedientes, 200);
    }

    #endregion

    #region method logical
    //metodo que retorna la cofiguracion de la empresa
    public function folder_empresa($id_empresa)
    {
        //url de la api
        $url = $this->Urldominio() . "api/ConfigEmpresa/" . $id_empresa;
        //tipo de token
        $type = Session::get('token_type');
        //token
        $token = Session::get('api_token');
        //peticion get
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Content-type: application/x-www-form-urlencoded',
                'Authorization: ' . $type . ' ' . $token,
                'Cookie: DT=DI0EIjjLIBMTtSqxAYujXFV6A'
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $folder = json_decode($response, true);


        return $folder;
    }
    //obtener expediente especifica
    public function getExpedienteId($id)
    {
        //url de la api
        $url = $this->Urldominio() . "api/expedienteId/" . $id;
        //tipo de token
        $type = Session::get('token_type');
        //token
        $token = Session::get('api_token');
        //peticion get
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Content-type: application/x-www-form-urlencoded',
                'Authorization: ' . $type . ' ' . $token,
                'Cookie: DT=DI0EIjjLIBMTtSqxAYujXFV6A'
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $expediente = json_decode($response, true);

        return $expediente;
    }
    //obtener todos los agentes aduanales
    public function agentesAll()
    {
        //url de la api
        $url = $this->Urldominio() . 'api/agentes';
        //tipo de token
        $type = Session::get('token_type');
        //token
        $token = Session::get('api_token');
        //peticion get
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Content-type: application/x-www-form-urlencoded',
                'Authorization: ' . $type . ' ' . $token,
                'Cookie: DT=DI0EIjjLIBMTtSqxAYujXFV6A'
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $agencias = json_decode($response, true);
        return $agencias;
    }

    //metodo que realializa la logica de guardado de un expediente
    public function store(Request $request)
    {
        //ide empresa en session
        $id_empresa = Session::get('id');


        //consumo la api que me trae el ultimo id del expediente
        //$id_expediente = HTTP::get($this->Urldominio().'api/idExpediente?token='.Session::get('token'));
        $url = $this->Urldominio() . 'api/idExpediente';
       // dd($url, $id_empresa);
        //tipo de token
        $type = Session::get('token_type');
        //token
        $token = Session::get('api_token');
        //peticion get
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Content-type: application/x-www-form-urlencoded',
                'Authorization: ' . $type . ' ' . $token,
                'Cookie: DT=DI0EIjjLIBMTtSqxAYujXFV6A'
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $id_expediente = json_decode($response, true);


        //se esta creando el numero de expediente
        $expediente = str_pad($id_expediente['id'] + 1, 20, 0, STR_PAD_LEFT);
        $aduana = ""; //pendiente aun no se envia se desconoce la razon
        //se crea el arreglo para enviar los datos
        $array = [
            "expediente" => 'CTRADE-' . $expediente,
            "nombre" => $request['nombre'],
            "descripcion" => $request['descripcion'],
            "agente_aduanal" => $request['agente_aduanal'],
            "aduana_id" => $aduana,
            "empresa_id" => $id_empresa,
            "status" => "Abierto"
        ];
        //dd($array, $expediente, $response, $token);
        //direccion de la api para registrar el expediente
        $url_ex = $this->Urldominio() . "api/insertExpedientes";
        //dd($url_ex);
        //se consume la api que trae los datos de la empresa en sesion
        $folderEmpresa = $this->folder_empresa($id_empresa);
        //obtenemos el valor del value, la cual se llamara la carpeta
        $folder = $folderEmpresa['value'];
        //convertimos en json el arreglo
        $data_json = json_encode($array);
        //realizamos la peticion a la api y mandamos valores
        // $empresa = HTTP::post($url, $array);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url_ex);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: ' . $type . ' ' . $token));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response  = curl_exec($ch);
        curl_close($ch);

        $expediente = json_decode($response, true);

        //recojemos el id del registro que se acaba de hacer
        $id = $expediente['id'];
        $path = storage_path() . "/app/" . $folder . "/" . $id;
        //procedemos a crear el directorio con todo los permisos
        File::makeDirectory($path, $mode = 0777, true, true);
        if (!File::exists($path)) { //si no existe es xq no se creo
            // No se creo el folder correctamente
            echo 'No se creo la carpeta correctamente';
        }
        //redireccionamos a lista de expedientes
        return redirect('expedientes')->with('message', "Operacion Exitosa!");
    }

    //metodo que realiza la actualizacion del expedietne
    public function Update(Request $request, $id)
    {
        try{
            //variable que se enviara para no tener error
            $aduana_id = "";
            //obtenemos los datos del expediente
            $expediente = $this->getExpedienteId($id);
            //creamos un array para enviar datos actualizados
            $array = [
                "expediente" => $expediente['expediente'],
                "nombre" => $request['nombre'],
                "descripcion" => $request['descripcion'],
                "agente_aduanal" => $request['aduana'],
                "aduana_id" => $aduana_id,
                "empresa_id" => Session::get('id'),
                "status" => $request['status']
            ];

            //api para actualizar los datos
            $url = $this->Urldominio() . "api/updateExpedientes/" . $id;
            $data_json = json_encode($array);
            //tipo de token
            $type = Session::get('token_type');
            //token
            $token = Session::get('api_token');

            //realizamos la peticion a la api para la modificacion
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: ' . $type . ' ' . $token, 'Content-Length: ' . strlen($data_json),));
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response  = curl_exec($ch);

            $empresas = json_decode($response, true);
            //dd($empresas['mensaje']);
            if ($empresas['mensaje'] != '1'){
                return redirect('expedientes')->with('message', '¡Operacion Exitosa!');
            }

            return redirect('expedientes')->with('errors', '¡Ocurrió un error al intentar actualizar!');

        }catch (Exception $e) {
            return redirect('/expedientes')->with('errors', '¡Ocurrió un error al intentar actualizar!');
        }
    }

    public function estado_cuenta($id, $tipo_estado)
    {
        $type = Session::get('token_type');
        //token
        $token = Session::get('api_token');

        $result = '';
        $facturas = "sin resultado";
        //OBTENEMOS EL ESTADO DE CUENTA DEL EXPEDIENTE (API)
        //tipo de token
        $url = $this->Urldominio() . "api/pagosFacturasEstCta/" . $id;
        //peticion get
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Content-type: application/x-www-form-urlencoded',
                'Authorization: ' . $type . ' ' . $token,
                'Cookie: DT=DI0EIjjLIBMTtSqxAYujXFV6A'
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $facturasCargadas = json_decode($response, true);


        //obtenemos los pedimentos por id del expediente (api)
        $url_pedim = $this->Urldominio() . "api/getPedimentoIdExpediente/" . $id;
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url_pedim,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Content-type: application/x-www-form-urlencoded',
                'Authorization: ' . $type . ' ' . $token,
                'Cookie: DT=DI0EIjjLIBMTtSqxAYujXFV6A'
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $pedimentos = json_decode($response, true);

        if ($pedimentos[0] == 400) { //si no encuentra datos
            $pedimentos = array();
            $pedim_total = '0';
            $cove_total  = '0';
        } else {

            $pedimentos  = json_decode($pedimentos['0']['json'], true);
            // extraigo el valor total del cove y del pedimento
            // $pedim_total = $pedimentos['cuadro_liquidacion']['4']['total'];
            // $cove_total  = $pedimentos['valor_cove']['valor_aduana'];
        }

        //obtenemos los coves por id expediente (api);
        $url_cov = $this->Urldominio() . "api/getCoveIdExpediente/" . $id;
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url_cov,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Content-type: application/x-www-form-urlencoded',
                'Authorization: ' . $type . ' ' . $token,
                'Cookie: DT=DI0EIjjLIBMTtSqxAYujXFV6A'
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $cove = json_decode($response, true);

        if ($cove[0] == 400) { //si no trae ningun registro o no se realizo la consulta
            $cove = array();
        } else {
            $cove = json_decode($cove['0']['json_cove'], true);
        }

        //obtenemos los movimientos por expediente y tipo 1/pago

        $url_pagos = $this->Urldominio() . "api/getMovimientoIdexpediente/" . $id;
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url_pagos,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Content-type: application/x-www-form-urlencoded',
                'Authorization: ' . $type . ' ' . $token,
                'Cookie: DT=DI0EIjjLIBMTtSqxAYujXFV6A'
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $pagos = json_decode($response, true);


        if (count($facturasCargadas) > 0) {

            foreach ($facturasCargadas as $fac) {
                $facturas = json_decode($fac['json_cfdi'], true);
                // dd($facturas);
                $registros[]  = array(
                    'emisor_rfc'     => $facturas['Emisor']['rfc'],
                    'fecha'          => $facturas['cfdiComprobante']['fecha'],
                    'subtotal'       => $facturas['cfdiComprobante']['subTotal'],
                    'importe'        => $facturas['Traslado']['importe'],
                    'total'          => $fac['total_factura'],
                    'pago'           => $fac['monto_pagado'],
                    'saldo'          => $fac['saldo'],
                    'tipo_factura'   => $fac['tipo_factura'],
                    'status_factura' => $fac['status_factura'],
                    'json_cfdi'      => $fac['json_cfdi'],
                    'id'             => $fac['id'],
                    'id_mov'         => $fac['id_mov']
                );
            }


            //realizo la suma de los archivos
            $total = array(
                'subtotal' => array_sum(array_column($registros, 'subtotal')),
                'importe'  => array_sum(array_column($registros, 'importe')),
                'total'    => array_sum(array_column($registros, 'total')),
                'pago'     => array_sum(array_column($registros, 'pago')),
                'saldo'    => array_sum(array_column($registros, 'saldo')),
                // 'pedim_total' => $pedim_total,
                //'cove_total'  => $cove_total,
                //'status_saldo'=> array_sum(array_column($registros, 'saldo')),
            );

            //valido si hay saldo a favor
            $result = $this->valida_pago($id, $total);
        } else {
            $registros = array();
            $total = array();
        }
        /** codigo agregado y se quito los elseif*/
        switch ($tipo_estado) {
            case 'general':
                return view('expediente.estado_cta', [
                    'id_expediente' => $id,
                    'facturas'      => $registros,
                    'pedimentos'    => $pedimentos,
                    // 'pagos'      => $pagos,
                    'total'         => $total,
                    'message'       => $result
                ]);
                break;

            case 'agente':
                if ($facturas == "sin resultado") {
                    return view('expediente.estado_cta_agente', [
                        'id_expediente' => $id,
                        'facturas'      => $registros,
                        'factura'       => 'sin registro',
                        // 'factura'       => $facturas,
                        'pedimentos'    => $pedimentos,
                        'pagos'         => $pagos,
                        'total'         => $total,
                        'message'       => $result,
                        'agente'        => 'sin registro',
                        'agenterfc'     => 'sin registro'
                        // 'agente'        => $facturas['Emisor']['nombre'],
                        //'agenterfc'     => $facturas['Emisor']['rfc']
                    ]);
                } else {
                    return view('expediente.estado_cta_agente', [
                        'id_expediente' => $id,
                        'facturas'      => $registros,
                        // 'factura'       => 'sin registro',
                        'factura'       => $facturas,
                        'pedimentos'    => $pedimentos,
                        'pagos'         => $pagos,
                        'total'         => $total,
                        'message'       => $result,
                        // 'agente'        => 'sin registro',
                        // 'agenterfc'     => 'sin registro'
                        'agente'        => $facturas['Emisor']['nombre'],
                        'agenterfc'     => $facturas['Emisor']['rfc']
                    ]);
                }
                break;

            case 'impuestos':
                return view('expediente.estado_cta_impuestos', [
                    'id_expediente' => $id,
                    'facturas'      => $registros,
                    'pedimentos'    => $pedimentos,
                    // 'pagos'      => $pagos,
                    'total'         => $total,
                    'message'       => $result
                ]);
                break;
        }
    }

    public function valida_pago($id, $total)
    {
        //tipo de token
        $type = Session::get('token_type');
        //token
        $token = Session::get('api_token');
        //realizo la suma de las facturas asignadas al expediente (api)
        $url = $this->Urldominio() . "api/sumaFacturaExpediente/" . $id;
        //peticion get
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Content-type: application/x-www-form-urlencoded',
                'Authorization: ' . $type . ' ' . $token,
                'Cookie: DT=DI0EIjjLIBMTtSqxAYujXFV6A'
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $suma_facturas = json_decode($response, true);

        //extraigo el monto pagado a la cuenta de gastos del agente
        $url_mov = $this->Urldominio() . "api/movimientoCtaGastaos/" . $id;
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url_mov,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Content-type: application/x-www-form-urlencoded',
                'Authorization: ' . $type . ' ' . $token,
                'Cookie: DT=DI0EIjjLIBMTtSqxAYujXFV6A'
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $movimientoCtaGastos = json_decode($response, true);

        //api movimiento cuenta ccomprobantes
        $url_comp = $this->Urldominio() . "api/movimientosComprobantes/" . $id;
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url_comp,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Content-type: application/x-www-form-urlencoded',
                'Authorization: ' . $type . ' ' . $token,
                'Cookie: DT=DI0EIjjLIBMTtSqxAYujXFV6A'
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $movimientosComprobantes = json_decode($response, true);

        foreach ($movimientosComprobantes as $row) {

            if ($row['status_mov'] == "Sin movimiento") {

                //echo $row->total_factura.'<--'.$movimientoCtaGastos.'-->'. ($movimientoCtaGastos - $row->total_factura).'<br>';
            }
        }
        //pagos factura estado de cuenta segun id de expediente
        $url_fac = $this->Urldominio() . "api/pagosFacturasEstCta/" . $id;
        //peticion get
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url_fac,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Content-type: application/x-www-form-urlencoded',
                'Authorization: ' . $type . ' ' . $token,
                'Cookie: DT=DI0EIjjLIBMTtSqxAYujXFV6A'
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $facturasCargadas = json_decode($response, true);

        if (count($facturasCargadas) > 0) {

            foreach ($facturasCargadas as $fac) {
                $facturas = json_decode($fac['json_cfdi'], true);
                $total_facturas[]  = array(
                    'emisor_rfc'     => $facturas['Emisor']['rfc'],
                    'fecha'          => $facturas['cfdiComprobante']['fecha'],
                    'subtotal'       => $facturas['cfdiComprobante']['subTotal'],
                    'importe'        => $facturas['Traslado']['importe'],
                    'total'          => $fac['total_factura'],
                    'pago'           => $fac['monto_pagado'],
                    'saldo'          => $fac['saldo'],
                    'tipo_factura'   => $fac['tipo_factura'],
                    'status_factura' => $fac['status_factura'],
                    'json_cfdi'      => $fac['json_cfdi'],
                    'id'             => $fac['id'],
                    'id_mov'         => $fac['id_mov']
                );
            }
        } else {
            $total_facturas = array();
        }
        //valido que el saldo total de las facturas sea menor al monto de movimiento de cta de gastos
        //tambien se hace la validacion que la suma del total de las facturas restantes sea igual al monto de movimiento de cta de gastos
        if ($total['saldo'] < $movimientoCtaGastos && $movimientoCtaGastos == $suma_facturas) {
            $message  = "hay saldo a favor, desea realizar la aplicacion del gasto a todas las facturas.";
        } elseif ($total['saldo'] < $movimientoCtaGastos  && $total['saldo'] != 0) {
            $message = "hay saldo a favor, desea realizar la aplicacion del gasto.>";
        } elseif ($total['saldo'] == 0) {
            $message = "";
        } else {
            $message = "";
        }

        return $message;
    }

    //aplicar pago.
    public function aplicarPagoCreate(Request $request, $id)
    {
        //tipo de token
        $type = Session::get('token_type');
        //token
        $token = Session::get('api_token');

        $ids  = $request->input('ids');
        $id_cta_gastos = $request->input('id_cta_gastos');

        $formaDePago  = 'aplicacion de gasto';
        //    $ids           = explode(',',str_replace(' ','',$ids));


        //aregolo para enivar los datos y hacer la consulta.


        //obtenemos la consulta de facturas cargadas por id_expediente y su id
        $url = $this->Urldominio() . "api/FacturasCargadasId_idExpediente/" . $id . "/" . $ids;
        //peticion get
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Content-type: application/x-www-form-urlencoded',
                'Authorization: ' . $type . ' ' . $token,
                'Cookie: DT=DI0EIjjLIBMTtSqxAYujXFV6A'
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $data = json_decode($response, true);


        // $data          = FacturasCargadas::where('id_expediente',$id)->whereIn('id', $ids)->get();

        foreach ($data as $row) {
            //$mov = new Movimiento;

            $tipo              = 'Pago';
            $idTipo            = 1;

            $monto_pagado      = $row['total'];
            $monto_factura     = $row['total'];
            $rfc               = $row['emisor_rfc'];
            $idExpediente      = $id;

            $fechaPago         = date("Y-m-d\TH:i:s", strtotime(date('Y-m-d H:i:s')));
            $uidPago           = $formaDePago;
            $id_agencia        = $row['id_agente'];
            $id_empresa        = Session::get('id');
            $id_facturacargada = $row['id'];

            $respuesta = $this->registrarPagoWS($row['id']);
            $resp         = $respuesta->getBody();
            $codeResponse = $respuesta->getStatusCode();

            if ($codeResponse == 200) {
                //$string = '{result: true, mensaje: null, interno: 1, poliza:1, tipo: PE}';
                /*  $string = str_replace('{','{"',$resp);
                    $string = str_replace('}','"}',$string);
                    $string = str_replace(':','":"',$string);
                    $string = str_replace(', ','","',$string);
                    $datos  = json_decode($string);
                    $interno = $datos['interno'];
                    $poliza  = $datos['poliza'];
                    $tipo    = $datos['tipo'];*/

                $interno = '';
                $poliza  = '';
                $tipo    = '';
            } else {
                $interno = '';
                $poliza  = '';
                $tipo    = '';
            }

            $polizaContable    = $tipo . ' ' . $poliza;

            $array = [
                "tipo" => $tipo,
                "idTipo" => $idTipo,
                "monto_factura" => $monto_factura,
                "monto_pagado" => $monto_pagado,
                "monto_anterior" => $monto_factura,
                "rfc" =>  $rfc,
                "idExpediente" => $idExpediente,
                "fechaPago" => date("Y-m-d\TH:i:s", strtotime(date('Y-m-d H:i:s'))),
                "uidPago" => $uidPago,
                "id_agencia" => $id_agencia,
                "id_empresa" =>  $id_empresa,
                "polizaContable" => $polizaContable,
                "id_facturacargada" => $request->id_factura
            ];

            //guardamos un movimiento
            //enviamos los datos a la api para su registro
            $api = $this->Urldominio() . "api/insertMovimiento";

            //guardamos los datos, usando api
            $data_json = json_encode($array);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $api);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: ' . $type . ' ' . $token));
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response  = curl_exec($ch);
            curl_close($ch);

            $movimiento = json_decode($response, true);


            //modificamos la datos de factura cargada a estado de pagado
            $arrays = ["id_factura" => $row['id']];
            $url = $this->Urldominio() . "api/UpdateFacturaCargada";
            $data_jso = json_encode($arrays);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: ' . $type . ' ' . $token, 'Content-Length: ' . strlen($data_json),));
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_jso);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response  = curl_exec($ch);
            curl_close($ch);
            $movimientoUpdate = json_decode($response, true);

            //arraeglo para movimiento
            $movi = [
                "monto_pagado" => $row['total']
            ];

            //ACTUALIZA EL MONTO PAGADO DE MOVIMIENTOS
            $movi_json = json_encode($movi);
            $url_mov_up = $this->Urldominio() . "api/UpdateMontoP/" . $id_cta_gastos;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url_mov_up);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: ' . $type . ' ' . $token, 'Content-Length: ' . strlen($data_json),));
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
            curl_setopt($ch, CURLOPT_POSTFIELDS, $movi_json);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response  = curl_exec($ch);
            curl_close($ch);
            $movimientoUpdate = json_decode($response, true);
            // dd($movimientoUpdat);

        }
        return redirect()->back();
    }

    public function registrarPagoWS($id)
    {
        $fecha = date("Y-m-d");
        //obtenemos los datos de la factura por su id. (api)
        $url = $this->Urldominio() . "api/FacturasCargadasId/" . $id;
        $type = Session::get('token_type');
        //token
        $token = Session::get('api_token');
        //peticion get
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Content-type: application/x-www-form-urlencoded',
                'Authorization: ' . $type . ' ' . $token,
                'Cookie: DT=DI0EIjjLIBMTtSqxAYujXFV6A'
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $factura = json_decode($response, true);
        $json_cfdi = json_decode($factura[0]['json_cfdi'], true);

        /*$client = new \GuzzleHttp\Client();
            $response = $client->post(
                'https://www.cpavision.mx/cpareview/cpa/cx/documentos_cap2/ws_pagos_anticipos.php',[
                    'body'=> [
                        //PAGO PESOS
                        'asiento'        =>'505',//:505                                DOL 506, MN 505
                        'cambio'         => '',
                        'ctabanco'       => '102.01',
                        'documento'      => '12',//:12
                        'fecha2'         => $fecha,//:2017-07-01
                        'fechaV'         => $fecha,//:2017-07-01
                        'importe'        => $factura[0]['total'],//:1224
                        'modulo'         => '2',//:2                                1 CXC, 2 CXP
                        'moneda'         => '0',//:0
                        'nombre'         => $factura[0]['emisor_nombre'],
                        'nomContrato'    => $json_cfdi["cfdiComprobante"]["tipoDeComprobante"],
                        'serie'          => $json_cfdi["cfdiComprobante"]["folio"],
                        'tipo_documento' => '7',//:7                        DOL 8,  MN 7
                        'rfc_empresa'    => $factura[0]['receptor_rfc'],//:DPM140627I40
                        'rfc_proveedor'  => $factura[0]['emisor_rfc']//:FHI8704277E9 emisor
                    ]
                ]
            );*/
        $array = [
            'body' => [
                //PAGO PESOS
                'asiento'        => '505', //:505                                DOL 506, MN 505
                'cambio'         => '',
                'ctabanco'       => '102.01',
                'documento'      => '12', //:12
                'fecha2'         => $fecha, //:2017-07-01
                'fechaV'         => $fecha, //:2017-07-01
                'importe'        => $factura[0]['total'], //:1224
                'modulo'         => '2', //:2                                1 CXC, 2 CXP
                'moneda'         => '0', //:0
                'nombre'         => $factura[0]['emisor_nombre'],
                'nomContrato'    => $json_cfdi["cfdiComprobante"]["tipoDeComprobante"],
                'serie'          => $json_cfdi["cfdiComprobante"]["folio"],
                'tipo_documento' => '7', //:7                        DOL 8,  MN 7
                'rfc_empresa'    => $factura[0]['receptor_rfc'], //:DPM140627I40
                'rfc_proveedor'  => $factura[0]['emisor_rfc'] //:FHI8704277E9 emisor
            ]
        ];
        $response = HTTP::post('https://www.cpavision.mx/cpareview/cpa/cx/documentos_cap2/ws_pagos_anticipos.php', $array);

        return $response;
    }

    //filtro de documentos
    public function filtro_expedientes(Request $request)
    {
        $type = Session::get('token_type');
        //token
        $token = Session::get('api_token');

        $id_empresa = Session::get('id');

        $data = $request->all();
        $datos = $data;
        // dd($data['inicio']);
        $expedientes = null;
        if (!empty($data)) {
            $datos = [
                'inicio' => $data['inicio'],
                'final' => $data['final'],
                'token' => Session::get('token')
            ];
            //dd($this->Urldominio()."api/expedienteXFecha/".$id_empresa."?token=".Session::get('token'), $data);
            //consultamos los expedientes por fecha y por empresa

            $expedientes = HTTP::withToken($type . " " . $token)->get($this->Urldominio() . "api/expedienteXFecha/" . $id_empresa, $datos);
            $expedientes = $expedientes->json();
        }

        if (!isset($_REQUEST['descargar'])) {
            return view('expediente.expedientes', [
                'expedientes' => $expedientes
            ]);
        } else {
            $exp = [];
            foreach ($expedientes as $expediente) {
                $exp[] = $expediente['id'];
            }
            $zip = $this->descarga_expedientes($exp, $id_empresa);


            if ($zip) { //si trae datos
                //descarga el zip.
                return response()->download(public_path($zip))->deleteFileAfterSend(true);
            } else {
                return redirect()->back()->with("message", "Expedientes sin documento cargados");
            }
        }
    }

    //descarga de documentos segun el filtro
    public function descarga_expedientes($expedientes, $id_empresa)
    {
        $type = Session::get('token_type');
        //token
        $token = Session::get('api_token');
        $zipper = new ZipArchive; //instanciamos la clase
        //obtenemos la empresa en session
        $folderEmpresa = HTTP::withToken($type . " " . $token)->get($this->Urldominio() . "api/ConfigEmpresa/" . $id_empresa);
        $folderEmpresa = $folderEmpresa->json();

        $rfc = $folderEmpresa['value']; //obtenemos el rfc de la empresa

        $folderZip = $rfc . '_General.zip'; //nombre del archivo zip
        $folder = storage_path('app/' . $rfc); // ubicacion de los archivos
        if (!file_exists($folder)) {
            return false;
        }

        $files = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($folder),
            RecursiveIteratorIterator::LEAVES_ONLY
        );
        //dd($folderZip);
        if ($zipper->open(public_path($folderZip), ZipArchive::CREATE) === TRUE) //creamos el archivo
        {

            foreach ($files as $name => $file) {
                //dd($file->isDir());

                // Skip directories (they would be added automatically)
                if (!$file->isDir()) {

                    foreach ($expedientes as $id) {
                        // Get real and relative path for current file
                        $filePath = $file->getRealPath();

                        //vamos a remplazar id del expediente que trae por la foreach
                        $d = " \ ";
                        $new_dir = str_replace($d[1], "|", $filePath);
                        $new_data = explode("|", $new_dir);

                        $path = "";
                        $count = 0;

                        for ($i = 0; $i < sizeof($new_data); $i++) {
                            if ($new_data[$i] == $rfc) {
                                $count = $i + 1;
                            }
                            if ($i == sizeof($new_data) - 1) {
                                $path .= $new_data[$i];
                            } else {

                                if ($i == $count) {
                                    if ($count > 0) {
                                        $path .= $id . "" . $d[1];
                                    } else {
                                        $path .= $new_data[$i] . "" . $d[1];
                                    }
                                } else {
                                    $path .= $new_data[$i] . "" . $d[1];
                                }
                            }
                        }
                        $relativePath = substr($path, strlen($folder) + 1);
                        $zipper->addFile($path, $relativePath);
                    }
                }
            }
            //cerramos el zip
            $zipper->close();
        }
        //realizamos la descarga
        return $folderZip;
    }

    #endregion
}
