<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Http\Request;
use Laravel\Sanctum\Http\Controllers\CsrfCookieController;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use PhpParser\Node\Expr\FuncCall;
use Session;

class OktaController extends Controller
{
    public function OktaLogin(Request $request)
    {
        set_time_limit(0);
        $url = "https://dev-534696.okta.com/oauth2/ausdw4dezmhhvT9uP4x6/v1/introspect"; //desifrar|iniciar sesion
        //$request);
        $token_Okta = $request->get('access_token'); //obtener token

        $ClientId = env('ClientId');
        $ClientSecret = env('ClientSecret');
        Log::info($token_Okta);
        if ($token_Okta) { //si hay token realiza lo siguiente
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => 'token=' . $token_Okta . '&token_type_hint=access_token',
                CURLOPT_HTTPHEADER => array(
                    'Content-type: application/x-www-form-urlencoded',
                    'Authorization: Basic ' . base64_encode($ClientId . ":" . $ClientSecret),
                    'Cookie: DT=DI0EIjjLIBMTtSqxAYujXFV6A'
                ),
            ));
            $response = curl_exec($curl);

            curl_close($curl);
            // dd($response);
            //mandamos para guardaros en sesion
            $userData = $this->SaveUser($response, $token_Okta);
            //ejecutamos el metodo para obtener el token para la sesion
            $api_token = $this->ApiVerify();

            if ($userData == 1) {
                return redirect("/index");
                // return redirect("https://customsandtrade.cpavision.mx/ctrade");
            }
        } else { //no hay toquen  redirecciona a inicio de sesion

            //return redirect('https://dev-534696.okta.com/oauth2/ausdw4dezmhhvT9uP4x6'); //no encontro la pagina
            return redirect('https://cpavision.mx/');
        }
    }

    //guardamos los datos en sesion para su posterior uso
    public function SaveUser($response, $token_Okta)
    {
        $response = json_decode($response);
        // dd($response);
        //obtenemos los datos con el id de la empresa
        $empresas = json_decode($response->empresas);
        $perfil = $empresas->rows[0]->perfiles->rows[0]->perfil;
        //dd($perfil);
        $descripcion_perfil = $empresas->rows[0]->perfiles->rows[0]->descripcion;

        //se agregan los datos de la empresa a la sesion para poder ser utilizadas en otras vistas y/o controladores
        Session::put('id_usuario', $response->usuario);
        Session::put('nombre', $response->nombre);
        Session::put('email', $response->email);
        Session::put('perfil', $perfil);
        Session::put('descripcion_perfil', $descripcion_perfil);
        Session::put('token', $token_Okta);
        Session::put('empresas', json_encode($empresas->rows));

        return 1;
    }

    public function cerrar_sesion()
    {

        $url = "https://dev-534696.okta.com/oauth2/ausdw4dezmhhvT9uP4x6/v1/revoke"; //matar|cerrar sesion
        $token_Okta = Session::get('token'); //obtener token
        $ClientId = env('ClientId');
        $ClientSecret = env('ClientSecret');

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => 'token=' . $token_Okta . '&token_type_hint=access_token',
            CURLOPT_HTTPHEADER => array(
                'Content-type: application/x-www-form-urlencoded',
                'Authorization: Basic ' . base64_encode($ClientId . ":" . $ClientSecret),
                'Cookie: DT=DI0EIjjLIBMTtSqxAYujXFV6A'
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $sesionDelete = $this->DeleteDataSession();
        //return redirect('https://dev-534696.okta.com/oauth2/ausdw4dezmhhvT9uP4x6'); //no encontro la pagina
        return redirect('https://cpavision.mx/');
    }

    public function DeleteDataSession()
    {
        //usuario logeado
        //olvidamos esos datos de la sesion,
        session()->forget('id_usuario');
        session()->forget('nombre');
        session()->forget('email');
        session()->forget('perfil');
        session()->forget('descripcion_perfil');
        session()->forget('token');
        session()->forget('empresas');
        //servicio de api
        session()->forget('token_type');
        session()->forget('api_token');

        //empresa
        //si existe un id es porque se ha seleccionado la empresa de lo
        //contrario nos marcario un error
        if (session()->has('id')) {
            session()->forget('id');
            session()->forget('rfc');
            session()->forget('empresa');
        }

        setcookie("api_token", "", time() - 3600);
    }

    //metodo que permitira tener el token para acceder a las apis.
    public function ApiVerify()
    {
        $url = "https://dev-38451189.okta.com/oauth2/default/v1/token"; //obtencion de token
        //$request);
        $ClientId = env('OKTA_CLIENT_ID'); //id cliente para obtener token api
        $ClientSecret = env('OKTA_CLIENT_SECRET'); //client secret para obtener token api

        //peticion post, mandamos un arreglo con las credenciales para obtener el token para la api
        $fields = array('grant_type' => 'client_credentials', 'client_id' => $ClientId, 'client_secret' => $ClientSecret, 'scope' => 'ctrade_api');
        $fields_string = http_build_query($fields);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $data = curl_exec($ch);
        curl_close($ch);
        //obtenemo los valores que trae la peticion
        $response = json_decode($data);

        //mandamos tipo de token  y access token a la session
        Session::put('token_type', $response->token_type);
        Session::put('api_token', $response->access_token);

        setcookie("api_token", $response->access_token, time() + $response->expires_in, "/");

        Log::info("API TOKEN: " . $response->access_token);
    }
}
