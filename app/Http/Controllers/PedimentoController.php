<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Expediente;
use App\Models\Empresa;
use App\Models\Pedimento;
use App\Models\PedimentosAsignado;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Http;
use Session;
use Illuminate\Support\Facades\DB;
use PHPUnit\Framework\Constraint\IsEmpty;
use Illuminate\Http\Response;
use App\Http\Controllers\apiConfigController;
use Illuminate\Support\Facades\View;

class PedimentoController extends Controller
{
    public function Urldominio()
    {
        $dominio =  app(apiConfigController::class)->dominio();
        // dd($dominio);
        // $this->Urldominio().
        return $dominio;
    }

    #region Method Views
    public function index($id)
    {
        $url = $this->Urldominio() . 'api/getpedimento/' . $id;
        //tipo de token
        $type = Session::get('token_type');
        //token
        $token = Session::get('api_token');
        //peticion get
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Content-type: application/x-www-form-urlencoded',
                'Authorization: ' . $type . ' ' . $token,
                'Cookie: DT=DI0EIjjLIBMTtSqxAYujXFV6A'
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $pedimentos = json_decode($response, true);

        if (!$pedimentos){
            $pedimentos = [];
        }

        return view('pedimento.index', ['pedimentos' => $pedimentos]);
    }

    public function cargar_pedimento($id_empresa)
    {
        $expedientes = Expediente::where('empresa_id', '=', $id_empresa)
            ->whereIn('status', ['Abierto', 'Proceso'])->get();

        return view('pedimento.cargar', [
            'expedientes' => $expedientes
        ]);
    }

    public function reporte(Request $request)
    {
        $ejercicio =  $request->ejercicio;
        $periodo = $request->periodo;

        return view('pedimento.pedimentos', ['ejercicio' => $ejercicio, 'periodo' => $periodo]);
    }

    public function getReportePedimentos(Request $request)
    {
        $ejercicio =  $request->ejercicio;
        $periodo = $request->periodo;

        if ($ejercicio &&  $periodo) {
            $id_empresa = Session::get('id');

            if ($id_empresa) {
                $pedimentos =  Pedimento::reportePeriodoPedimentos($id_empresa, $ejercicio, $periodo);

                return response()->json([
                    'message' => 'ok',
                    'pedimentos' =>    View::make('pedimento.pdmtos', [
                        'pedimentos' =>   $pedimentos,
                        'ejercicio' => $ejercicio,
                        'periodo' => $periodo
                    ])->render()
                ]);
            }

            return response()->json([
                "message" => "Seleccione una empresa para poder continuar."
            ], 400);
        } else {
            $id_empresa = Session::get('id');
            $pedimentos =  Pedimento::reportePedimentos($id_empresa);

            return response()->json([
                "message" => "ok",
                "pedimentos" =>    View::make('pedimento.pdmtos', [
                    "pedimentos" =>   $pedimentos
                ])->render()
            ]);
        }

        return response()->json([
            "message" => "Fecha y periodo son requeridos."
        ], 400);
    }

    public function pedimentoDetails(Request $request, $id)
    {
        $type = Session::get('token_type');
        //token
        $token = Session::get('api_token');
        //pedimentos asignados al expediente en consulta (api)

        $ejercicio =  $request->ejercicio;
        $periodo = $request->periodo;


        if ($ejercicio &&  $periodo) {
            $pedimento = HTTP::withToken($type . " " . $token)->get($this->Urldominio() . "api/getpedimentoId/" . $id . "?token=" . Session::get('token'));
            $pedimento = $pedimento->json();
            $pedimento = json_decode($pedimento['json'], true);

            $backUrl = url("reporte?ejercicio={$ejercicio}&periodo={$periodo}");

            return view('pedimento.pedimento', ['pedimento' => $pedimento, 'back' => $backUrl]);
        }

        return redirect()->back();
    }

    public function matchFacreview()
    {
        return view('pedimento.facreview');
    }

    #endregion

    #region Method API´S

    //obtener pedimento por empresa api
    public function getPedimento($id)
    {
        $pedimento = Pedimento::where('empresa_id', $id)->get();
        if (is_null($pedimento)) {
            return response()->json(['Mensaje' => 'registro no encontrado', 400]);
        }
        $empresa = response()->json($pedimento, 200);
        return $empresa;
    }

    //insertar pedimentoo api
    public function insertPedimento(Request $request)
    {
        $pedimento = Pedimento::create($request->all());
        return response()->json($pedimento, 200);
    }

    //insertar pedimentoasigando pi
    public function insertPedimAsignado(Request $request)
    {
        $pedimAsigado = PedimentosAsignado::create($request->all());
        return response()->json($pedimAsigado, 200);
    }

    //obtenemos pedimento por id
    public function getPedimentoId($id)
    {

        $pedimento = Pedimento::where('id', $id)->firstOrFail();
        if (is_null($pedimento)) {
            return response()->json(['Mensaje' => 'registro no encontrado', 400]);
        }
        $pedimentos = response()->json($pedimento, 200);
        return $pedimentos;
    }
    //api actualizar pedimento, agregar un expediente al pedimento
    public function updatePedimentoExpediente(Request $request, $id)
    { //id del pedimento
        $id_expediente = $request['expediente_id'];
        $updatePedimento = Pedimento::where('id', $id)->update(['expediente_id' => $id_expediente]);
        return response()->json($updatePedimento, 200);
    }

    public function getPedimentoEmpresa($id_pedimento)
    {
        $pedimento  = Pedimento::where('pedimentos.id', $id_pedimento)->leftJoin('empresas', 'pedimentos.empresa_id', '=', 'empresas.id')->first();
        return response()->json($pedimento, 200);
    }

    //obtenemos pedimento por id del expediente
    public function getPedimentoIdExpediente($id)
    {
        $pedimento = Pedimento::where('expediente_id', $id)->get();
        if ($pedimento == "[]") {
            return response()->json(['Mensaje' => 'registro no encontrado', 400]);
        } else {
            $pedimentos = response()->json($pedimento, 200);
            return $pedimentos;
        }
    }

    //obtenemos la lista de pedimentos asigbnados al expediente
    public function getPedimentosAsignados($id)
    { //id_expediente
        $pedimentos_asignados = DB::table('pedimentos')
            ->leftJoin('pedimento_asignados', 'pedimentos.id', 'pedimento_asignados.id_pedimento')
            ->where('pedimentos.expediente_id', $id)
            ->select('pedimentos.*')
            ->get();
        return response()->json($pedimentos_asignados, 200);
    }

    //lista de pedimetnos  por id de expedietne con aduan id_empresa
    public function PedimentoAduanaExpedienteId($id, $id_empresa)
    {
        $pedimentos = Pedimento::with(['aduana'])->where([['empresa_id', $id_empresa], ['expediente_id', $id]])->get();
        return response()->json($pedimentos, 200);
    }
    #endregion api

    #region Method logical

    public function upload_pedimento(Request $request,$id )
    {
        // validar archivos y extension
        $this->validate($request, [
            'pedim_file_m' => 'required',
            'pedim_file_pdf' => 'required|mimetypes:application/pdf,pdf'
        ], [
            'pedim_file_m.required' => 'El archivo M es requerido',
            'pedim_file_pdf.required' => 'El archivo PDF es requerido',
            'pedim_file_pdf.mimetypes' => 'El archivo no es un PDF '
        ]);

        // crear input con la variable para validar nombre del archivo
        $request->request->set('filem', $request->file('pedim_file_m')->getClientOriginalName());

        // validar que sea una archivo m, cometnado por el deyvid
        $this->validate($request, [
            'filem' => 'required|regex:/(^m\d{7}\.\d{3}$)/u'
        ], [
            'filem.regex' => 'El archivo ingresado no se detecto como archivo M'
        ]);

        $pdf = $request->file('pedim_file_pdf'); //obtenemos datos del pdf
        $pdf_name = $pdf->getClientOriginalName(); //optenemos el nombre del pdf

        $id_expediente = $request->input('expediente');
        $inputs        = ['pedim_file_m', 'pedim_file_pdf'];



        //obtener datos de la empresa segun id.
        $id_empresa = Session::get('id');
        //url de la api
        $url = $this->Urldominio() . 'api/getEmpresaId/' . $id_empresa;
        //tipo de token
        $type = Session::get('token_type');
        //token
        $token = Session::get('api_token');
        //peticion get
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Content-type: application/x-www-form-urlencoded',
                'Authorization: ' . $type . ' ' . $token,
                'Cookie: DT=DI0EIjjLIBMTtSqxAYujXFV6A'
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $empresa = json_decode($response, true);
        $empresa['rfc'] = Session::get('rfc');


        foreach ($inputs as $input) {
            if ($request->file($input)) {

                $file     = $request->file($input);
                $fileName = $file->getClientOriginalName();
                $mime     = $file->getClientMimeType();

                //extraigo la primera letra par avalidar que es un archivo M
                $fileM = substr($fileName, 0, 1);
                $fileM = strtoupper($fileM);
                if ($fileM == 'M' && strcmp($mime, "application/pdf") && strcmp($mime, "text/pdf")) {
                    /*$this->nombreArchivo = $fileName;*/
                    $path = $empresa['rfc'] . '/pedimentos/' . $fileName;
                    if (!empty($id_expediente)) {
                        $path = $empresa['rfc'] . '/' . $id_expediente . '/pedimentos/' . $fileName;
                    }

                    Storage::put($path, file_get_contents($request->file('pedim_file_m')->getPathName()));

                    // Obtiene el json que se guardara en la base de datos
                    $pedimentos_json = $this->pedimento_json(storage_path('app/' . $path));


                    foreach ($pedimentos_json['datos_pedimento'] as $pedimento_json) {
                        $json_pedimento = json_encode($pedimentos_json);

                        //api de insertar pedimetno
                        $url = $this->Urldominio() . "api/insertPedimento";
                        //se crea un arreglo para mandarsela a la api con los datos a insertar

                        $array = [
                            "pedimento" => $pedimento_json['num_pedimento'],
                            "aduanaDespacho" => $pedimento_json['id_aduana'],
                            "impExpNombre" => $pedimento_json['nombre_imp_exp'],
                            "tipoOperacion" => $pedimento_json['tipoOperacion'],
                            "empresa_id" => Session::get('id'),
                            "expediente_id" => $id_expediente,
                            "archivoM" => $fileName,
                            "archivoPDF" => $pdf_name,
                            "json" => $json_pedimento
                        ];
                        //tipo de token
                        $type = Session::get('token_type');
                        //token
                        $token = Session::get('api_token');
                        $data_json = json_encode($array);
                        //realizamos la peticion a la api y mandamos valores
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, $url);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: ' . $type . ' ' . $token));
                        curl_setopt($ch, CURLOPT_POST, 1);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        $response  = curl_exec($ch);
                        curl_close($ch);
                        $pedimento = json_decode($response, true);
                        // obtenemos el id con el que se guardo el registro
                        $id_pedimento = $pedimento['id'];
                        //  $this->id_pedimento = $id_pedimento;

                        if (!empty($id_expediente)) {
                            //api de insertar pedimentoasignado
                            $url = $this->Urldominio() . "api/insertPedimAsig";
                            //se crea un arreglo para mandarsela a la api con los datos a insertar
                            $array = [
                                "id_expediente " => $id_expediente,
                                "id_pedimento" => $id_pedimento,
                            ];
                            $data_json = json_encode($array);
                            //realizamos la peticion a la api y mandamos valores
                            $ch = curl_init();
                            curl_setopt($ch, CURLOPT_URL, $url);
                            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: ' . $type . ' ' . $token));
                            curl_setopt($ch, CURLOPT_POST, 1);
                            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            $response  = curl_exec($ch);
                            curl_close($ch);
                            $pedimento = json_decode($response, true);
                        }
                    }
                } elseif (!strcmp($mime, "application/pdf") || !strcmp($mime, "text/pdf")) {
                    $path = $empresa['rfc'] . '/pedimentos/' . $fileName;
                    if (!empty($id_expediente)) {
                        $path = $empresa['rfc'] . '/' . $id_expediente . '/pedimentos/' . $fileName;
                    }

                    //  Storage::put($path, file_get_contents($request->file($input)->getRealPath()));
                    Storage::put($path, file_get_contents($request->file('pedim_file_pdf')->getPathName()));
                    // $pedimento = Pedimento::find($this->id_pedimento);
                    // $pedimento->update(['archivoPDF' => $fileName]);
                }
            }
        }

        if (!empty($id_expediente)) {
            $notificacion = array(
                'mensaje' => 'Carga realizada exitosamente.',
                'alert-type' => 'success'
            );

            return view('documentos.cargar', compact('id_expediente'))->with('operacion_pedimento', $notificacion);
        }

        //$file->move($path, $fileName);
        return redirect('pedimentos/' . Session::get('id'))
            ->with('message', 'Se cargo el pedimento exitosamente');
    }
    //metodo que convierte el contenido en json
    public function pedimento_json($path)
    {
        // Abriendo el archivo
        // Abriendo el archivo

        $archivo  = fopen($path, "r");
        $numlinea = 0;
        $array    = array();

        // Recorremos todas las lineas del archivo
        while (!feof($archivo)) {
            // Leyendo una linea
            $traer = fgets($archivo);

            $id = substr($traer, 0, 3);
            //$id = nl2br(substr($traer,0,3));
            //identifico las keys del array
            $array[$id] = array();
        }


        // Cerrando el archivo
        fclose($archivo);


        $file = fopen($path, "r");

        while (!feof($file)) {
            // Leyendo una linea
            $traer = fgets($file);
            $id    = nl2br(substr($traer, 0, 3));
            if (array_key_exists($id, $array)) {

                $array_add = $traer;
                array_push($array[$id], $array_add);
            }
        }
        fclose($file);

        $collection = collect($array);

        $datoCove = $collection->only('505');
        foreach ($datoCove[505] as $key => $value) {
            //$validate = collect(explode('|',$value))->contains('12672.15');
            //if($validate){
            $columna = collect(explode('|', $value));
            // dd($columna);
            $datos_cove[] = array(
                'num_pedimento'       => $columna[1],
                'fecha_cove'          => $columna[2],
                'cove'                => $columna[3],
                'incoterm'            => $columna[4],
                'moneda_fact'         => $columna[5],
                'valorTotalDollar'    => $columna[6],
                'valorTotalMoneda'    => $columna[7],
                'pais'                => $columna[8],
                'id_fiscal'           => $columna[10],
                'nombre_proveedor'    => $columna[11],
                'direccion_proveedor' => $columna[12] . ' ' . $columna[13] . ', ' . $columna[14] . ', ' . $columna[15] . ' ' . $columna[16]
            );
            $datos_pedimento['datos_cove'] =  $datos_cove;
            //dd($datos_pedimento);

            // }
        }

        //extraigo el num_pedimento para realizar la busqueda en los demas valores
        //$num_pedimento = $datos_pedimento['datos_cove']['num_pedimento'];

        $datoPedimento = $collection->only('501');

        foreach ($datoPedimento[501] as $key => $value) {
            // $validate = collect(explode('|',$value))->contains($num_pedimento);
            //if($validate){
            $columna = collect(explode('|', $value));
            $dato_pedimento[] = array(
                'num_pedimento'     => $columna[2],
                'id_aduana'         => $columna[3],
                'tipoOperacion'     => $columna[4],
                'cve_pedimento'     => $columna[5],
                'rfc_importador'    => $columna[8],
                'tipo_cambio'       => $columna[10],
                'peso_bruto'        => $columna[16],
                'salida'            => $columna[17],
                'entrada'           => $columna[18],
                'entrada_salida'    => $columna[19],
                'destino_origen'    => $columna[20],
                'nombre_imp_exp'    => $columna[21],
                'direccion_imp_exp' => $columna[22] . ' ' . $columna[23] . ' ' . $columna[24] . ' ' . $columna[25] . ' ' . $columna[26] . ' ' . $columna[27] . ' ' . $columna[28],
            );

            $datos_pedimento['datos_pedimento'] =  $dato_pedimento;

            // }
        }



        return ($datos_pedimento);

        //$json_pedimento= json_encode($datos_pedimento);
        //$json_pedimento = json_decode($json_pedimento, true);
        //dd($json_pedimento);
    }


    public function asigna_pedimentos($id_pedimento, $id_expediente)
    {
        //creamos un arrreglo para enviar datos a la api de guardar pedimentos asignados
        $array = [
            "id_pedimento"  => $id_pedimento,
            "id_expediente" => $id_expediente
        ];
        //realizamos una cosulta  a pedimentos y traemos datos_pedimentos
        //url de la api
        $url = $this->Urldominio() . "api/getpedimentoId/" . $id_pedimento;
        //type token
        $type = Session::get('token_type');
        //token
        $token = Session::get('api_token');
        //peticion get
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Content-type: application/x-www-form-urlencoded',
                'Authorization: ' . $type . ' ' . $token,
                'Cookie: DT=DI0EIjjLIBMTtSqxAYujXFV6A'
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $pedimento = json_decode($response, true);
        $pedimentos = json_decode($pedimento['json'], true);

        foreach ($pedimentos["datos_pedimento"] as $key => $value) {
            $validate = collect($pedimentos["datos_pedimento"])->values()->contains('num_pedimento');
            if ($validate) {
                //vardump($value);
            }
        }

        $id_aduana = ["aduana_id" => $pedimentos["datos_pedimento"][0]["id_aduana"]];
        //guardamos el array que es el asignamiento del pedimento

        //api de insertar pedimentoasignado
        $url_api = $this->Urldominio() . "api/insertPedimAsig";
        //hacemos el regitro de pedimento asignado haciendo usa de la api
        $data_json = json_encode($array);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url_api);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: ' . $type . ' ' . $token));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response  = curl_exec($ch);
        curl_close($ch);

        $pedimento_asignado = json_decode($response, true);


        if ($pedimento_asignado) {
            //si se realizo la insercion


            //api para actualizar el id_expediente para pediemento
            $api_exp = $this->Urldominio() . "api/updatePedimentoExpediente/" . $id_pedimento;
            //realizamos un arreglo para enviar el id del expediente
            $arrary_expediente = ["expediente_id" => $id_expediente];
            //actualizar el pedimento con su id y le actualiza el campo id_expediente de null y se pone el id del expediente que se trajo
            $data_json = json_encode($arrary_expediente);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $api_exp);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: ' . $type . ' ' . $token, 'Content-Length: ' . strlen($data_json),));
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response  = curl_exec($ch);
            curl_close($ch);
            $pedimento_update = json_decode($response, true);

            //api para acutalizar la aduana id del expediente
            $api = $this->Urldominio() . "api/updateExpedienteAduana/" . $id_expediente;
            //actualizamos el expediente y le ponemos aduana_id  nulo, se le pondra id de la aduana que se recupero
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $api);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: ' . $type . ' ' . $token, 'Content-Length: ' . strlen($data_json),));
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response  = curl_exec($ch);
            curl_close($ch);
            $expediente_update = json_decode($response, true);


            //obtenemos todas la consulta de empresa_pedimento
            $url_pe = $this->Urldominio() . "api/getPedimentoEmpresa/" . $id_pedimento;
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url_pe,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_HTTPHEADER => array(
                    'Content-type: application/x-www-form-urlencoded',
                    'Authorization: ' . $type . ' ' . $token,
                    'Cookie: DT=DI0EIjjLIBMTtSqxAYujXFV6A'
                ),
            ));
            $response = curl_exec($curl);
            curl_close($curl);
            $pedimento = json_decode($response, true);

            $empresa['rfc'] = Session::get('rfc');
            //se movera los archivos de la ubicacion actal a su nuevo ubicacion
            $path = storage_path('app/' . $empresa['rfc'] . '/' . $pedimento['expediente_id'] . '/pedimentos/' . $pedimento['archivoM']);
            if(!file_exists($path)) {
                print_r([$empresa['rfc'] . '/pedimentos/' . $pedimento['archivoM'], $empresa['rfc'] . '/' . $pedimento['expediente_id'] . '/pedimentos/' . $pedimento['archivoM']]);
                Storage::move($empresa['rfc'] . '/pedimentos/' . $pedimento['archivoM'], $empresa['rfc'] . '/' . $id_expediente . '/pedimentos/' . $pedimento['archivoM']);
            }

            $path = storage_path('app/' . $empresa['rfc'] . '/' . $pedimento['archivoM'] . '.pdf');
            if (file_exists($path)) {
                Storage::move($empresa['rfc'] . '/pedimentos/' . $pedimento['archivoM'] . '.pdf', $empresa['rfc'] . '/' . $id_expediente . '/pedimentos/' . $pedimento['archivoM'] . '.pdf');
            }

            //Storage::move($expediente->

            return redirect('expediente/pedimento/unsigned/' . $id_expediente)
                ->with("message", "¡operación Exitosa!");
        } else {
            return redirect('expediente/pedimento/unsigned/' . $id_expediente)->with("message", "¡operación Exitosa!");
        }
    }

    //descarga de documetnos xml
    public function pedimentosXML($pedimento_id)
    {
        $type = Session::get('token_type');
        //token
        $token = Session::get('api_token');

        $empresa_id = session()->get('id');

        //obtenemos datos de la empresa por su id (api)
        $empresa = HTTP::withToken($type . " " . $token)->get($this->Urldominio() . "api/getEmpresaId/" . $empresa_id . "?token=" . Session::get('token'));
        $empresa = $empresa->json();
        //obtenemos los datos del pedimento (api)
        $pedimento = HTTP::withToken($type . " " . $token)->get($this->Urldominio() . "api/getpedimentoId/" . $pedimento_id . "?token=" . Session::get('token'));
        $pedimento = $pedimento->json();

        //asigansmo los valores para la busqueda
        $expediente_id = $pedimento['expediente_id'];
        $pedimento_name = $pedimento['archivoM'];

        $path = $empresa['rfc'] . '/' . $expediente_id . '/pedimentos/' . $pedimento_name;

        $content = Storage::get($path);
        $mime = Storage::mimeType($path);

        return (new Response($content, 200))
            ->header('Content-Type', $mime)
            ->header('Content-Disposition', 'attachment; filename=' . $pedimento_name);
    }

    public function pedimentosPDF($pedimento_id)
    {
        $type = Session::get('token_type');
        //token
        $token = Session::get('api_token');
        $empresa_id = session()->get('id');
        $rfc = session()->get('rfc');

        //obtenemos datos de la empresa por su id (api)
        $empresa = HTTP::withToken($type . " " . $token)->get($this->Urldominio() . "api/getEmpresaId/" . $empresa_id . "?token=" . Session::get('token'));
        $empresa = $empresa->json();
        //obtenemos los datos del pedimento (api)
        $pedimento = HTTP::withToken($type . " " . $token)->get($this->Urldominio() . "api/getpedimentoId/" . $pedimento_id . "?token=" . Session::get('token'));
        $pedimento = $pedimento->json();

        //asigansmo los valores para la busqueda
        $expediente_id = $pedimento['expediente_id'];
        $pedimento_name = $pedimento['archivoPDF'];

        //$path = $empresa['rfc'] . '/' . $expediente_id . '/pedimentos/' . $pedimento_name;
        $path = $rfc . '/' . $expediente_id . '/pedimentos/' . $pedimento_name;

        $content = Storage::get($path);
        $mime = Storage::mimeType($path);

        return (new Response($content, 200))
            ->header('Content-Type', $mime)
            ->header('Expires', '0')
            ->header('Cache-Control', 'must-revalidate')
            ->header('Pragma', 'public');
    }

    //vista del pedimetno seleccionado
    public function pedimento_vista($id, $expediente_id)
    {
        $type = Session::get('token_type');
        //token
        $token = Session::get('api_token');
        //pedimentos asignados al expediente en consulta (api)
        $pedimentos_asignados = HTTP::withToken($type . " " . $token)->get($this->Urldominio() . "api/getPedimentosAsignados/" . $id . "?token=" . Session::get('token'));
        $pedimento = $pedimentos_asignados->json();

        if (empty($pedimento)) {

            //obtenemos los datos del pedimento (api)
            $pedimento = HTTP::withToken($type . " " . $token)->get($this->Urldominio() . "api/getpedimentoId/" . $id . "?token=" . Session::get('token'));
            $pedimento = $pedimento->json();
        }
        $pedimento = json_decode($pedimento['json'], true);

        return view('pedimento.pedimento', ['pedimento' => $pedimento, 'expediente_id' => $expediente_id]);
    }
    #endregion

}
