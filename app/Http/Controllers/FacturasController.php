<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Models\FacturasCargadas;
use App\Models\ConfigEmpresa;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Response;
use PDF;
use Session;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\apiConfigController;
use SimpleXMLElement;


class FacturasController extends Controller
{
    public function Urldominio()
    {
        $dominio =  app(apiConfigController::class)->dominio();
        // dd($dominio);
        // $this->Urldominio().
        return $dominio;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $url = $this->Urldominio() . "api/getFacturaExpediente/" . $id;
        //tipo de token
        $type = Session::get('token_type');
        //token
        $token = Session::get('api_token');
        //peticion get
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Content-type: application/x-www-form-urlencoded',
                'Authorization: ' . $type . ' ' . $token,
                'Cookie: DT=DI0EIjjLIBMTtSqxAYujXFV6A'
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $facturas = json_decode($response, true);
        return view('facturas.index', ['expediente_id' => $id, 'facturas' => $facturas]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    #region methos logical
    public function subirFacturaPagos($id, $tipo_factura)
    {
        return view('facturas.cargarfactura', ['expediente_id' => $id, 'tipo_factura' => $tipo_factura]);
    }

    public function uploadFiles(Request $request, $id_empresa)
    {
        $expediente_id = $request->input('expediente_id');
        try{
            $this->validate($request, [
                'factura_xml' => 'required|mimetypes:application/xml',
                'factura_xml' => 'required',
                'factura_pdf' => 'required|mimetypes:application/pdf',
                'factura_pdf' => 'required'
                //Codigo de validacion de contenido de xml
                //   'factura_xml'=> 'bail|required|mimes:application/xml,xml|max:10000',
            ], [
                'facturas_xml.required' => 'El archivo XML es requerido',
                'facturas_pdf.required' => 'El archivo PDF es requerido',
                'factura_pdf.mimetypes' => 'El archivo no es un PDF',
                'factura_xml.mimetypes' => 'El archivo no es un PDF'
            ]);
            $redir = 0;
            // las variables request que se usaran
            $inputs        = ['factura_xml', 'factura_pdf'];
            $tipo_factura  = $request->input('tipo_factura');

            //obtenemos la configuracion de la carpeta de la empresa
            $url = $this->Urldominio() . "api/ConfigEmpresa/" . $id_empresa;
            //type token
            $type = Session::get('token_type');
            //token
            $token = Session::get('api_token');
            //peticion get
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_HTTPHEADER => array(
                    'Content-type: application/x-www-form-urlencoded',
                    'Authorization: ' . $type . ' ' . $token,
                    'Cookie: DT=DI0EIjjLIBMTtSqxAYujXFV6A'
                ),
            ));
            $response = curl_exec($curl);
            curl_close($curl);
            $folderEmpresa = json_decode($response, true);
            $folderEmpresa = $folderEmpresa['value'];
            //dd($folderEmpresa);

            $expediente_id = $request->input('expediente_id');
            $ruta          = storage_path("app/" . $folderEmpresa . "/" . $expediente_id . "/facturas_xml/");
            $path          = $folderEmpresa . "/" . $expediente_id . "/facturas_xml/";

            // recorremos cada input para buscar las variables
            foreach ($inputs as $input) {
                $file     = $request->file($input);
                $fileName = $file->getClientOriginalName();
                $mime = $file->getMimeType();
                // validamos el tipo de archivo que se carga
                if (!strcmp($mime, 'application/xml') || !strcmp($mime, 'text/xml')) {
                    $this->fileName = $fileName;

                    // dd($path.$fileName); //agregado
                    Storage::put($path . $fileName, file_get_contents($file->getPathName()));
                    //se llama el metodo save_xml para guardar los datos
                    //  $this->save_xml($request, $ruta.$fileName, $expediente_id, $tipo_factura, $fileName, $path.$fileName);

                    //se llama el metodo save_xml para guardar los datos y guardamos la respuesta en la variable redir
                    $redir =   $this->save_xml($request, $ruta . $fileName, $expediente_id, $tipo_factura, $fileName, $path . $fileName);
                } elseif (!strcmp($mime, 'application/pdf') || !strcmp($mime, 'text/pdf')) {
                    // se genera el archivo pdf con el mismo nombre que el archivo xml y se guarda
                    $archivo = explode('.', $this->fileName);
                    Storage::put($path . $archivo[0] . '.pdf', file_get_contents($file->getPathName()));
                }
            }
            // $facturas = FacturasCargadas::where('id_expediente', $expediente_id)->get();


            if ($redir == 2) {
                // return view('facturas.cargarfactura',['id'=>$expediente_id,'tipo'=>$facturas]);
                // return redirect('subirFacturaPagos',['id'=>$expediente_id,'tipo'=>$facturas]);
                // return redirect('facturas.cargarfactura');
                return redirect('subir_facturas/' . $expediente_id . '/' . $tipo_factura)
                    ->with('success', 'Contenido del archivo xml incorrecto');
            } else {
                //return view('facturas.index',['expediente_id'=>$expediente_id,'facturas'=>$facturas]);
                return redirect('facturas/' . $expediente_id)->with('success', 'Los archivos se guardaron correctamente.');
            }

        }catch (Exception $e) {
            return redirect('facturas/' . $expediente_id)->with('success', 'Los archivos se guardaron correctamente.');
        }
    }

    //guardar datos de xml
    public function save_xml($request, $upload, $expediente_id, $tipo_factura, $fileName, $ruta)
    {
        try {
            $file = $upload;
            $xml_contenido = file_get_contents($file);

            $xml = simplexml_load_file($file);

            $ns  = $xml->getNamespaces(true);
            $xml->registerXPathNamespace('c', $ns['cfdi']);
            $xml->registerXPathNamespace('t', $ns['tfd']);

            $array = array();

            $redir = 2;
            //empiezo a leer la informacion del CFDI
            if ($xml->xpath('//cfdi:Comprobante')) {

                foreach ($xml->xpath('//cfdi:Comprobante') as $cfdiComprobante) {

                    $infoArray                = (array) $cfdiComprobante;
                    $infoArray                = ($infoArray['@attributes']);
                    $array['cfdiComprobante'] =  $infoArray;
                }
            } else {
                return $redir;
            }

            if ($xml->xpath('//cfdi:Comprobante//cfdi:Emisor')) {
                foreach ($xml->xpath('//cfdi:Comprobante//cfdi:Emisor') as $Emisor) {

                    $infoArray       = (array) $Emisor;
                    $infoArray       = ($infoArray['@attributes']);
                    $array['Emisor'] = $infoArray;
                }
            } else {
                return $redir;
            }


            if ($xml->xpath('//cfdi:Comprobante//cfdi:Emisor//cfdi:DomicilioFiscal')) {

                foreach ($xml->xpath('//cfdi:Comprobante//cfdi:Emisor//cfdi:DomicilioFiscal') as $DomicilioFiscal) {
                    $infoArray                = (array) $DomicilioFiscal;
                    $infoArray                = ($infoArray['@attributes']);
                    $array['DomicilioFiscal'] = $infoArray;
                }
            } else {
                return $redir;
            }

            if ($xml->xpath('//cfdi:Comprobante//cfdi:Emisor//cfdi:ExpedidoEn')) {
                foreach ($xml->xpath('//cfdi:Comprobante//cfdi:Emisor//cfdi:ExpedidoEn') as $ExpedidoEn) {
                    $infoArray           = (array) $ExpedidoEn;
                    $infoArray           = ($infoArray['@attributes']);
                    $array['ExpedidoEn'] = $infoArray;
                }
            } else {
                return $redir;
            }

            if ($xml->xpath('//cfdi:Comprobante//cfdi:Receptor')) {
                foreach ($xml->xpath('//cfdi:Comprobante//cfdi:Receptor') as $Receptor) {
                    $infoArray         = (array) $Receptor;
                    $infoArray         = ($infoArray['@attributes']);
                    $array['Receptor'] = $infoArray;
                }
            } else {
                return $redir;
            }

            if ($xml->xpath('//cfdi:Comprobante//cfdi:Receptor//cfdi:Domicilio')) {
                foreach ($xml->xpath('//cfdi:Comprobante//cfdi:Receptor//cfdi:Domicilio') as $ReceptorDomicilio) {
                    $infoArray                  = (array) $ReceptorDomicilio;
                    $infoArray                  = ($infoArray['@attributes']);
                    $array['ReceptorDomicilio'] = $infoArray;
                }
            } else {
                return $redir;
            }

            //conteo de conceptos de pago
            $conteo_concepto = count(($xml->xpath('//cfdi:Comprobante//cfdi:Conceptos//cfdi:Concepto')));
            $data_concepto = $xml->xpath('//cfdi:Comprobante//cfdi:Conceptos//cfdi:Concepto');
            $infoArray = json_decode(json_encode((array)$data_concepto), TRUE);
            //for ($i=0; $i < $conteo_concepto; $i++) {

            foreach ($infoArray as $row) {
                $conceptos[] = array(
                    'cantidad'      => $row['@attributes']['Cantidad'], // *
                    //'unidad'        => $row['@attributes']['Unidad'], // *
                    'descripcion'   => $row['@attributes']['Descripcion'], // *
                    'valorUnitario' => $row['@attributes']['ValorUnitario'], // *
                    'importe'       => $row['@attributes']['Importe'] // *
                );
            }
            //}
            $array['Conceptos']  = $conceptos;

            foreach ($xml->xpath('//cfdi:Comprobante//cfdi:Impuestos//cfdi:Traslados//cfdi:Traslado') as $Traslado) {
                $infoArray         = (array) $Traslado;
                $infoArray         = ($infoArray['@attributes']);
                $array['Traslado'] = $infoArray;
            }

            foreach ($xml->xpath('//t:TimbreFiscalDigital') as $tfd) {
                $infoArray    = (array) $tfd;
                $infoArray['@attributes']['UUID'] = strtoupper($infoArray['@attributes']['UUID']);
                $infoArray    = ($infoArray['@attributes']);
                $array['tfd'] = $infoArray;
            }
            //convierte json los datos extraidos del archivo
            $json_cfdi = json_encode($array);
            $data      = json_decode($json_cfdi, true);
            //realiza el registro de la factura(api cpa vision)
            $respuesta    = $this->registrarFacturaWS($data);
            $codeResponse = $respuesta->getStatusCode();
            $resp         = $respuesta->getBody()->getContents();
            //convierte en json la respuesta
            $data_resp    = json_decode($resp);


            if ($codeResponse === 200 && $data_resp->result === true) {
                $interno = $data_resp->interno;
                $poliza  = $data_resp->poliza;
                $tipo    = $data_resp->tipo;
            } elseif ($codeResponse === 200 && $data_resp->result === 11) {
                $interno = $data_resp->interno;
                $poliza  = $data_resp->result;
                $tipo    = $data_resp->result;
                echo  'La factura ' . $fileName . ' ' . $data_resp->mensaje;
                exit();
            } elseif ($codeResponse === 200 && $data_resp->result === 10 || $data_resp->result === 9) {
                $interno = '';
                $poliza  = '';
                $tipo    = $data_resp->result;
                echo  'Error ' . $data_resp->mensaje . '- ' . $fileName;
            } else {
                $interno = '';
                $poliza  = '0';
                $tipo    = '0';
                echo  'Error ' . $data_resp->mensaje . '- ' . $fileName;
            }

            // $FacturasCargadas = new FacturasCargadas;
            // $expediente       = Expediente::findOrFail($expediente_id);
            //consulta de expediente para traer el agente aduanal;
            $url = $this->Urldominio() . "api/expedienteId/" . $expediente_id;
            //tipo de token
            $type = Session::get('token_type');
            //token
            $token = Session::get('api_token');
            //peticion get
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_HTTPHEADER => array(
                    'Content-type: application/x-www-form-urlencoded',
                    'Authorization: ' . $type . ' ' . $token,
                    'Cookie: DT=DI0EIjjLIBMTtSqxAYujXFV6A'
                ),
            ));
            $response = curl_exec($curl);
            curl_close($curl);
            $expediente = json_decode($response, true);

            $receptor_nombre = "";
            if (array_key_exists('Nombre', $data['Receptor'])) {
                $receptor_nombre = $data['Receptor']['Nombre'];
            }
            //id del usuario logiado
            $id_usuario = Session::get('id_usuario');
            //arreglo para poder enviar los datos a la bd
            $array = [
                "id_agente" => $expediente['agente_aduanal'],
                "id_usuario" => $id_usuario,       //session()->get('id_usuario') pendiente el id de usuario
                "id_expediente" => $expediente_id,
                "formaDePago" => $data['cfdiComprobante']['FormaPago'],
                "tipo_factura" => $tipo_factura,
                "poliza" => $interno . '-' . $poliza,
                "emisor_rfc" => $data['Emisor']['Rfc'],
                "emisor_nombre" => $data['Emisor']['Nombre'],
                "receptor_rfc" => $data['Receptor']['Rfc'],
                "receptor_nombre" => $receptor_nombre,
                "total" => $data['cfdiComprobante']['Total'],
                "fecha" => $data['cfdiComprobante']['Fecha'],
                "xml_file" => $ruta,
                "json_cfdi" => $json_cfdi
            ];

            //url de la api
            $url = $this->Urldominio() . "api/SaveFacturas";
            //realizamos el guardado
            $data_json = json_encode($array);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: ' . $type . ' ' . $token));
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response  = curl_exec($ch);
            curl_close($ch);

            $FacturasCargadas = json_decode($response, true);

            if ($FacturasCargadas['mensaje'] == 1) {
                echo 'La factura ' . $fileName . ' se inserto correctamente';
            } else {
                echo 'ups hubo una falla al cargar la factura ' . $fileName;
            }

        }catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function registrarFacturaWS($data)
    {
        /* $fecha = date("Y-m-d");
            $client = new \GuzzleHttp\Client();
            $body = [
                'anio_s'        => date("Y"),
                'fecha1'        => $fecha,
                'fecha2'        => $fecha,
                'fecha3'        => $fecha,
                'idfiscal'      => $data["cfdiComprobante"]["Folio"], // *
                'mes_s'         => date("m"),
                'moneda1'       => '0',
                'montofactura'  => $data['cfdiComprobante']['Total'], // *
                'montopesos'    => $data['cfdiComprobante']['Total'], // *
                'nombre'        => $data['Emisor']['Nombre'], // *
                'numfactura'    => $data["cfdiComprobante"]["Folio"], // *
                'rfc_empresa'   => $data['Receptor']['Rfc'], // *
                'rfc_proveedor' => $data['Emisor']['Rfc'] // *
            ];

            $response = $client->post('https://www.cpavision.mx/cpareview/cpa/cx/exportacion_cap2/ws_factura_extranjera.php', ['body'=> $body]);
            return $response;*/

        $fecha = date("Y-m-d");
        //$client = new \GuzzleHttp\Client();
        $body = [
            'anio_s'        => date("Y"),
            'fecha1'        => $fecha,
            'fecha2'        => $fecha,
            'fecha3'        => $fecha,
            'idfiscal'      => $data["cfdiComprobante"]["Folio"], // *
            'mes_s'         => date("m"),
            'moneda1'       => '0',
            'montofactura'  => $data['cfdiComprobante']['Total'], // *
            'montopesos'    => $data['cfdiComprobante']['Total'], // *
            'nombre'        => $data['Emisor']['Nombre'], // *
            'numfactura'    => $data["cfdiComprobante"]["Folio"], // *
            'rfc_empresa'   => $data['Receptor']['Rfc'], // *
            'rfc_proveedor' => $data['Emisor']['Rfc'] // *
        ];

        $response = HTTP::post('https://www.cpavision.mx/cpareview/cpa/cx/exportacion_cap2/ws_factura_extranjera.php', ['body' => $body]);
        return $response;
    }


    //view factura cargada
    public function show_facturaCargada($id, $expediente_id)
    {
        $factura_view = $this->obtenerFacturId($id);

        foreach ($factura_view as  $row) {
            $factura = json_decode($row['json_cfdi'], true);
        }
        // dd($factura);
        return view('facturas.vista_factura', [
            'expediente_id' => $expediente_id,
            'factura' => $factura
        ]);
    }

    //dowload factura xml
    public function download($id)
    {
        $factura = $this->obtenerFacturId($id);
        $formato = 'xml';

        $pathFile = $factura['0']['xml_file'];
        $file_name = $factura['0']['xml_file'];
        $file = $file_name . '.' . $formato;

        if (!Storage::exists($pathFile)) {
            abort(404);
        }

        $content = Storage::get($pathFile);
        $mime = Storage::mimeType($pathFile);

        return (new Response($content, 200))
            ->header('Content-Type', $mime)
            ->header('Content-Disposition', 'attachment; filename=' . $file);
    }
    //dowload factura pdf
    public function facturaPDF($factura_id)
    {
        $type = Session::get('token_type');
        //token
        $token = Session::get('api_token');
        $id_empresa = Session::get('id');
        $factura = $this->obtenerFacturId($factura_id);
        $folderEmpresa = HTTP::withToken($type . " " . $token)->get($this->Urldominio() . "api/ConfigEmpresa/" . $id_empresa);
        $folderEmpresa = $folderEmpresa->json();

        $empresa = $folderEmpresa['value'];
        $factura_ruta = explode('/', $factura['0']['xml_file']);
        $factura_nombre = explode('.', $factura_ruta[count($factura_ruta) - 1]);
        $path = $empresa . '/' . $factura['0']['id_expediente'] . '/facturas_xml/' . $factura_nombre[0] . '.pdf';
        if (!Storage::exists($path)) {
            abort(404);
        }
        $content = Storage::get($path);
        $mime = Storage::mimeType($path);

        return (new Response($content, 200))
            ->header('Content-Type', $mime)
            ->header('Expires', '0')
            ->header('Cache-Control', 'must-revalidate')
            ->header('Pragma', 'public');
    }

    //metodo obtner facturas
    public function obtenerFacturId($id)
    {
        $type = Session::get('token_type');
        //token
        $token = Session::get('api_token');
        $factura_view = HTTP::withToken($type . " " . $token)->get($this->Urldominio() . "api/FacturasCargadasId/" . $id);
        $factura_view = $factura_view->json();
        return $factura_view;
    }
    #endregion

    #region apis
    //obtiene lisado de facturas
    public function getFacturaExpediente($id)
    {
        $facturas = FacturasCargadas::getFacturaExpediente($id);
        return response()->json($facturas, 200);
    }

    public function folderEmpresa($id)
    {
        $folderEmpresa = ConfigEmpresa::where('empresa_id', $id)
            ->where('configuracion', 'folder_storage')
            ->first();
        return response()->json($folderEmpresa, 200);
    }

    public function SaveFacturas(Request $request)
    {

        if ($facturas = FacturasCargadas::create($request->all())) {
            return response()->json(['mensaje' => '1'], 200);
        }
        return response()->json(['mensaje' => '0'], 404);
    }

    public function FacturasCargadasId($id)
    {

        $facturas  = FacturasCargadas::where('id', $id)->get();
        if ($facturas == "[]") {
            return response()->json(['mensaje' => '0', 404]);
        } else {
            $factura = response()->json($facturas, 200);
            return $factura;
        }
    }

    //modificar estado de la factura cargada
    public function UpdateFacturaCargada(Request $request)
    {
        $update =  FacturasCargadas::where('id', $request->id_factura)
            ->update(['status_factura' => 'Pagado']);

        return response()->json($update, 200);
    }

    //realizo la suma de las facturas asignadas al expediente ()
    public function sumaFacturaExpediente($id)
    {
        $suma_facturas = FacturasCargadas::where('id_expediente', $id)
            ->sum('total');
        if ($suma_facturas == "[]") {
            return response()->json(['mensaje' => '0', 400]);
        } else {
            $suma_facturas = response()->json($suma_facturas, 200);
            return $suma_facturas;
        }
    }

    //pagos estado de cuenta segun el id del expediente
    public function pagosFacturasEstCta($id)
    {
        $facturasCargadas = DB::table('facturas_cargadas')
            ->leftJoin('movimientos', 'movimientos.id_facturacargada', '=', 'facturas_cargadas.id')
            ->select(
                'facturas_cargadas.tipo_factura',
                'json_cfdi',
                'total as total_factura',
                'monto_pagado',
                'facturas_cargadas.id',
                'movimientos.id as id_mov',
                'facturas_cargadas.status_factura',
                DB::raw('case
                        when total - monto_pagado is null then "0"
                        else total - monto_pagado

                        end as saldo')
            )
            ->where('id_expediente', $id)
            ->get();
        return response()->json($facturasCargadas, 200);
    }

    //se obtieene la factura cargada por id_expiente y su id
    public function FacturasCargadasId_idExpediente($id, $ids)
    {
        // dd($id);
        $facturas = FacturasCargadas::where('id_expediente', $id)->where('id', $ids)->get();
        return response()->json($facturas, 200);
    }
    #endregion apis
}
