<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\FacturasCargadas;
use App\Models\Expediente;
use App\Models\CatalogoTipoPago;
use App\Models\Agencia;
use App\Models\Movimiento;
use Illuminate\Support\Facades\Http;
use Session;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\apiConfigController;

class MovimientoController extends Controller
{
    public function Urldominio()
    {
        $dominio = app(apiConfigController::class)->dominio();
        // dd($dominio);
        // $this->Urldominio().
        return $dominio;
    }

    #region vistas
    //abre el modal de registrar pagos
    public function getFormPago(int $id) //id_expediente
    {
        //se consume la api que trae la factura por expediente y status_factura null
        $facturas = $this->obtenerFactura($id);

        //$nombre_agencia=session()->get('nombre_agencia'); codigo pendiente
        //obtenemos datos del expediente por su id, (api)
        $expediente = $this->obtenerExpediente($id);
        //obtenemos todos los tipos de pagos(api)
        $tipoPago = $this->ObtenerTipoPago();
        //obtenemos la informacion de los agentes aduanales (api)
        $url = $this->Urldominio() . "api/agentes";
        $type = Session::get('token_type');
        //token
        $token = Session::get('api_token');
        //peticion get
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Content-type: application/x-www-form-urlencoded',
                'Authorization: ' . $type . ' ' . $token,
                'Cookie: DT=DI0EIjjLIBMTtSqxAYujXFV6A'
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $agencias = json_decode($response, true);

        if (count($facturas) > 0) {
            foreach ($facturas as $key => $factura) {
                $facturas[$key]['factura_json'] = json_decode($factura['json_cfdi'], true);
            }
        }

        return view('movimiento.registrar_pago')
            ->with("expediente", $expediente)
            ->with("tipoPago", $tipoPago)
            ->with("facturas", $facturas)
            ->with("agencias", $agencias);
    }

    public function getFormAnticipo(int $id)
    {

        //obtenemos datos del expediente por su id, (api)
        $expediente = $this->obtenerExpediente($id);

        //obtenemos todos los tipos de pagos(api)
        $tipoPago = $this->ObtenerTipoPago();
        //obtenemos los datos del agente de acuerdo al expediente
        $id_agente = $expediente['agente_aduanal'];

        $url = $this->Urldominio() . "api/AgenciaId/" . $id_agente;
        $type = Session::get('token_type');
        //token
        $token = Session::get('api_token');
        //peticion get
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Content-type: application/x-www-form-urlencoded',
                'Authorization: ' . $type . ' ' . $token,
                'Cookie: DT=DI0EIjjLIBMTtSqxAYujXFV6A'
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $agencia = json_decode($response, true);

        return view('movimiento.registrar_anticipo')
            ->with("expediente", $expediente)
            ->with("tipoPago", $tipoPago)
            ->with("id", $id)
            ->with("agencia", $agencia);
    }

    #endregion vistas

    #region api

    //trae la lista de facturas cargadas
    public function getFacturas($id)
    { //id de expediente
        $facturas = FacturasCargadas::where('id_expediente', $id)
            //en caso que sea null
            ->whereNull('status_factura')
            //en caso que sea vacio
            ->orWhere('status_factura', "")
            ->get();
        return response()->json($facturas, 200);
    }

    public function TipoPago()
    {
        $tipoPago = CatalogoTipoPago::all();
        return response()->json($tipoPago, 200);
    }

    public function insertMovimiento(Request $request)
    {

        if ($movimiento = Movimiento::create($request->all())) {
            return response()->json(['mensaje' => '1'], 200);
        } else {
            return response()->json(['mensaje' => '0'], 404);
        }
    }

    //obtener agente aduanal segun el id que trae el expediente
    public function AgenciaId($id)
    {
        $agencia = Agencia::findORFail($id);
        return response()->json($agencia, 200);
    }

    //obtenemos los movimientos por id expediente y tipo 1
    public function getMovimientoIdexpediente($id)
    {
        $movimiento = Movimiento::where('idExpediente', $id)->where('idTipo', 1)->get();
        if ($movimiento == "[]") {
            return response()->json(['mensaje' => 'registro no encontrado'], 404);
        } else {
            $movimiento = response()->json($movimiento, 200);
            return $movimiento;
        }
    }

    //movimiento cuenta de gastos
    public function movimientoCtaGastaos($id)
    {
        $movimientoCtaGastos = Movimiento::where('idExpediente', $id)
            ->leftJoin('facturas_cargadas', 'movimientos.id_facturacargada', '=', 'facturas_cargadas.id')
            ->where('idTipo', 1)  //del tipo Pago
            ->where('tipo_factura', 'cta_gastos') //que la factura sea del tipo cta_gastos
            ->sum('monto_factura');
        $movimientoCtaGastos = response()->json($movimientoCtaGastos, 200);
        return $movimientoCtaGastos;
    }

    public function movimientosComprobantes($id)
    {
        $movimientosComprobantes = DB::table('facturas_cargadas')
            ->leftJoin('movimientos', 'movimientos.id_facturacargada', '=', 'facturas_cargadas.id')
            ->select('facturas_cargadas.tipo_factura', 'json_cfdi', 'total as total_factura', 'monto_pagado', 'facturas_cargadas.id', 'movimientos.id as id_mov', 'facturas_cargadas.status_factura',
                DB::raw('total - monto_factura as saldo'),
                DB::raw('case
                    when total - monto_factura IS NULL then "Sin movimiento"
                    else "Con movimiento"
                        end as status_mov'))
            ->where('id_expediente', $id)
            ->get();
        return response()->json($movimientosComprobantes, 200);
    }

    public function UpdateMontoP(Request $request, $id)
    {

        $movimiento = DB::table('movimientos')
            ->where('id', $id)
            ->update(['monto_pagado' => DB::raw('monto_pagado - ' . $request->monto_pagado)]);
        return response()->json($movimiento, 200);
    }

    //lista de anticipos
    public function getMovimientoExp_2($id)
    { //id_expediente
        $anticipos = Movimiento::where('idExpediente', $id)->where('idTipo', 2)->get();
        return response()->json($anticipos, 200);
    }

    //consulta de pagos por expedietne
    public function pagosFacturasExp($id)
    {
        return Movimiento::pagosFacturasExp($id);
    }

    #endregion api

    #region method logical
    public function store(Request $request, $id_expediente)
    {
        //de acuerdo al id->forma_pago recibido es la forma de pago
        switch ($request->forma_pago) {
            case '1':
                $formaDePago = "EFECTIVO";
                break;
            case '2':
                $formaDePago = "CHEQUE";
                break;
            case '3':
                $formaDePago = "TRANSFERENCIA";
                break;
            default:
                $formaDePago = "OTRO";
                break;
        }


        if ($request->tipoPago == 2) {
            $tipo = "Anticipo";
            $id_tipo = 2;

        } else {
            $tipo = "Pago";
            $id_tipo = 1;
        }

        if($request->id_factura){ //si la vista trae un id la tomamos
            $id_factura = $request->id_factura;
        }else{ //de lo contrario consultamos la tabla factura con el id de expediente.

            $url = $this->Urldominio()."api/getFacturaExpediente/".$id_expediente;
            $type = Session::get('token_type');
            //token
            $token = Session::get('api_token');
            //peticion get
            $curl = curl_init();
                 curl_setopt_array($curl, array(
                        CURLOPT_URL => $url,
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => '',
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 0,
                        CURLOPT_FOLLOWLOCATION => true,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => 'GET',
                        CURLOPT_HTTPHEADER => array(
                            'Content-type: application/x-www-form-urlencoded',
                            'Authorization: '.$type.' '.$token,
                            'Cookie: DT=DI0EIjjLIBMTtSqxAYujXFV6A'
                    ),
                 ));
                 $response = curl_exec($curl);
                 curl_close($curl);
                 $FacturasCargadas =json_decode($response, true);
            if($FacturasCargadas){ //si trae datos
                 //tomamos el id
                 $id_factura = $FacturasCargadas[0]['id'];
            }else{ //si no trae datos mandamos un 0 a id_factura, para provocar
                //un estatus 404 y no realizar operacion
                $id_factura = 0;
            }
        }

              //realializamos consulta de factura para extraer ciertos datos(api)
        $url = $this->Urldominio()."api/FacturasCargadasId/".$id_factura;
        $type = Session::get('token_type');
        //token
        $token = Session::get('api_token');
        //peticion get
        $curl = curl_init();
             curl_setopt_array($curl, array(
                    CURLOPT_URL => $url,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'GET',
                    CURLOPT_HTTPHEADER => array(
                        'Content-type: application/x-www-form-urlencoded',
                        'Authorization: '.$type.' '.$token,
                        'Cookie: DT=DI0EIjjLIBMTtSqxAYujXFV6A'
                ),
             ));
             $response = curl_exec($curl);
             curl_close($curl);
             $factura =json_decode($response, true);

        $cantidad = str_ireplace("$","", $request->monto);
        $cantidad = str_ireplace(",","", $cantidad);
        if($factura[0] == 404){

               return redirect()->back()
            ->with('success','¡operacion no realizada, No se encontro factura cargada!');

        }else{
            //creamos un array para almacenar informacion del movimiento
            $array = [
                "tipo" => $tipo,
                "idTipo" => $id_tipo,
                "monto_factura" => $factura['0']['total'],
                "monto_pagado" => $cantidad,
                "monto_anterior" => $cantidad,
                "rfc" => $request->rfc,
                "idExpediente" => $id_expediente,
                "fechaPago" => date("Y-m-d\TH:i:s", strtotime(date('Y-m-d H:i:s'))),
                "uidPago" => $formaDePago,
                "id_agencia" => $factura['0']['id_agente'],
                "id_empresa" => Session::get('id'),
                "polizaContable" => "M".' '."mk",
                "id_facturacargada" => $id_factura
            ];

            //enviamos los datos a la api para su registro url
            $api = $this->Urldominio()."api/insertMovimiento";
            //guardamos los datos, usando api
            $data_json = json_encode($array);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $api);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: '.$type.' '.$token));
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response  = curl_exec($ch);
            curl_close($ch);
            $movimiento = json_decode($response, true);

            $array = ["id_factura" => $id_factura];
            $data_json = json_encode($array);
            //modificamos la datos de factura cargada a estado de pagado
            $url_up = $this->Urldominio()."api/UpdateFacturaCargada";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url_up);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: '.$type.' '.$token, 'Content-Length: ' . strlen($data_json), ));
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
            curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response  = curl_exec($ch);
            curl_close($ch);
            $movimientoUpdate =json_decode($response, true);

            return redirect()->back()
            ->with('success','¡operacion realizada exitosamente!');
        }

    }

    public function obtenerFactura($id){
            $url = $this->Urldominio()."api/getFacturas/".$id;
            $type = Session::get('token_type');
            //token
            $token = Session::get('api_token');
            //peticion get
            $curl = curl_init();
                 curl_setopt_array($curl, array(
                        CURLOPT_URL => $url,
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => '',
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 0,
                        CURLOPT_FOLLOWLOCATION => true,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => 'GET',
                        CURLOPT_HTTPHEADER => array(
                            'Content-type: application/x-www-form-urlencoded',
                            'Authorization: '.$type.' '.$token,
                            'Cookie: DT=DI0EIjjLIBMTtSqxAYujXFV6A'
                    ),
                 ));
                 $response = curl_exec($curl);
                 curl_close($curl);
                 $facturas =json_decode($response, true);
            return $facturas;
    }

    public function obtenerExpediente($id){
            $url = $this->Urldominio()."api/expedienteId/".$id;
            $type = Session::get('token_type');
            //token
            $token = Session::get('api_token');
            //peticion get
            $curl = curl_init();
                 curl_setopt_array($curl, array(
                        CURLOPT_URL => $url,
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => '',
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 0,
                        CURLOPT_FOLLOWLOCATION => true,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => 'GET',
                        CURLOPT_HTTPHEADER => array(
                            'Content-type: application/x-www-form-urlencoded',
                            'Authorization: '.$type.' '.$token,
                            'Cookie: DT=DI0EIjjLIBMTtSqxAYujXFV6A'
                    ),
                 ));
                 $response = curl_exec($curl);
                 curl_close($curl);
                 $expediente =json_decode($response, true);
            return $expediente;
    }

    public function ObtenerTipoPago(){
        $url = $this->Urldominio()."api/TipoPago";
        $type = Session::get('token_type');
        //token
        $token = Session::get('api_token');
        //peticion get
        $curl = curl_init();
             curl_setopt_array($curl, array(
                    CURLOPT_URL => $url,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'GET',
                    CURLOPT_HTTPHEADER => array(
                        'Content-type: application/x-www-form-urlencoded',
                        'Authorization: '.$type.' '.$token,
                        'Cookie: DT=DI0EIjjLIBMTtSqxAYujXFV6A'
                ),
             ));
             $response = curl_exec($curl);
             curl_close($curl);
             $tipoPago =json_decode($response, true);

        return $tipoPago;
    }
    #endregion
}
