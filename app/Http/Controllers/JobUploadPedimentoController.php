<?php

namespace App\Http\Controllers;

use App\Models\datosConexionEmpresa;
use Session;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;
use App\Http\Controllers\apiConfigController;
use App\Models\Api;

class JobUploadPedimentoController extends Controller
{
    public function Urldominio()
    {
        return  app(apiConfigController::class)->dominio();
    }

    #region method view
    public function index()
    {
        //id de la empresa
        $id = Session::get('id');
        $route = "getsftp/" . $id;

        $Api = new Api($route);
        $response = $Api->GET();
        $sftp = json_decode($response, true);

        if ($sftp['0'] == 400) {
            // return view('sftp.index', ['sftp' => $sftp]);
            return redirect('programacion_pedimento/create');
        } else {
            //return redirect('programacion_pedimento/create');
            return view('sftp.index', ['sftp' => $sftp]);
        }
    }

    public function edit($id)
    {
        $url = $this->Urldominio() . 'api/getsftpid/' . $id;
        //tipo de token
        $type = Session::get('token_type');
        //token
        $token = Session::get('api_token');
        //peticion get
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Content-type: application/x-www-form-urlencoded',
                'Authorization: ' . $type . ' ' . $token,
                'Cookie: DT=DI0EIjjLIBMTtSqxAYujXFV6A'
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $sftp = json_decode($response, true);

        return view('sftp.edit', ['sftp' => $sftp]);
    }

    public function create()
    {
        return view('sftp.create');
    }
    #endregion

    #region method api
    //busqueda de conexion por id de empresa, (api)
    public function getSFTP($id)
    {

        $conexion = datosConexionEmpresa::where('id_empresa', $id)->get();
        if ($conexion == "[]") { //si la consulta viene vacia
            return response()->json(['Mensaje' => '0', 400]);
        } else {
            $conexionD = response()->json($conexion, 200);
            return $conexionD;
        }
    }

    //busqueda de conexion por id, (api)
    public function getSFTPid($id)
    {

        $conexion = datosConexionEmpresa::where('id', $id)->get();
        if ($conexion == "[]") { //si la consulta viene vacia
            return response()->json(['Mensaje' => '0', 400]);
        } else {
            $conexionD = response()->json($conexion, 200);
            return $conexionD;
        }
    }

    //insertar/crear nuevo  sftp api
    public function insertSFTP(Request $request)
    {

        if ($conexion = datosConexionEmpresa::create($request->all())) {
            return response()->json(['mensaje' => '1'], 200);
        } else {
            return response()->json(['mensaje' => '0'], 404);
        }
    }

    //modificar sftp
    public function updateSFTP(Request $request, $id)
    {
        //buscar el row por id
        $conexion = datosConexionEmpresa::find($id);
        if (is_null($conexion)) {
            return response()->json(['mensaje' => '0'], 404);
        }
        // $empresa->update($request->all());
        if ($conexion->update($request->all())) {
            return response()->json(['mensaje' => '1'], 200);
        }
        return response()->json(['mensaje' => '2'], 502);
    }
    #endregion  

    #region Method Logical
    public function update(Request $request, $id)
    {
        //url del dominio
        $url = $this->Urldominio() . 'api/update/' . $id;
        //obtenemos solo los datos a utilizar
        $array = [
            'host' => $request['host'],
            'user' => $request['user'],
            'password' => $request['password'],
            'path' => $request['path']
        ];

        //realizamos la peticion a la api para la modificacion
        //  $empresa = HTTP::put($url, $array);


        $data_json = json_encode($array);
        //tipo de token
        $type = Session::get('token_type');
        //token
        $token = Session::get('api_token');
        //realizamos la peticion a la api para la modificacion
        //   $empresa = HTTP::put($url, $array);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: ' . $type . ' ' . $token, 'Content-Length: ' . strlen($data_json),));
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response  = curl_exec($ch);
        curl_close($ch);

        $empresa = json_decode($response, true);

        //si el mensaje es 1 retorna la vista y mensaje exitoso
        if ($empresa['mensaje'] == 1) {
            $mensaje = "1";
            return redirect('programacion_pedimento')->with("message", $mensaje);
        } else {
            $mensaje = "0";
            return redirect('programacion_pedimento')->with("message", $mensaje);
        }
    }

    //metodo para guardar nueva conexion en bd
    public function store(Request $request)
    {

        $url = $this->Urldominio() . 'api/insertSFTP';

        //obtenemos solo los datos a utilizar
        $array = [
            'id_empresa' => Session::get('id'),
            'host' => $request['host'],
            'user' => $request['user'],
            'password' => $request['password'],
            'path' => $request['path']
        ];

        //tipo de token
        $type = Session::get('token_type');
        //token
        $token = Session::get('api_token');
        //pasamos a formato json el arreglo
        $data_json = json_encode($array);
        //realizamos la peticion a la api y mandamos valores

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: ' . $type . ' ' . $token));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response  = curl_exec($ch);
        curl_close($ch);

        $sftp = json_decode($response, true);

        //si el mensaje es 1 retorna la vista y mensaje exitoso
        if ($sftp['mensaje'] == 1) {
            $mensaje = "1";
            return redirect('programacion_pedimento')->with("message", $mensaje);
        } else {
            $mensaje = "0";
            return redirect('programacion_pedimento')->with("message", $mensaje);
        }
    }
    #endregion
}
