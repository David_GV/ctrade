<?php

namespace App\Http\Controllers;

use App\Models\Empresa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use PhpParser\Node\Expr\AssignOp\Concat;
use Session;
use App\Http\Controllers\apiConfigController;
use GrahamCampbell\ResultType\Result;
use Illuminate\Support\Facades\Log;
use App\Models\Api;

class EmpresaController extends Controller
{
    public function Urldominio()
    {
        $dominio =  app(apiConfigController::class)->dominio();
        // dd($dominio);
        // $this->Urldominio().
        return $dominio;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $route = "getEmpresas";

        $Api = new Api($route);
        $response = $Api->GET();
        $empresas = json_decode($response, true);

        Log::info("EMPRESA HTTP => " . $response);

        return view('empresa.index')->with('empresas', $empresas);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    public function viewcreate()
    {
        return view('empresa.create');
    }
    //obtener empresa api
    public function getEmpresa()
    {
        return response()->json(Empresa::all(), 200);
    }

    //insertar empresa api
    public function insertEmpresa(Request $request)
    {

        if ($empresa = Empresa::create($request->all())) {
            return response()->json(['mensaje' => '1'], 200);
        } else {
            return response()->json(['mensaje' => '0'], 404);
        }
    }

    //api eliminar empresa
    public function deleteEmpresa($id)
    {
        //busca la empresa
        $empresa = Empresa::find($id);

        if (is_null($empresa)) {
            return response()->json(['mensaje' => '0'], 404);
        }
        if ($empresa->delete()) {
            return response()->json(['mensaje' => '1'], 200);
        }
        return response()->json(['mensaje' => '0'], 404);
    }

    //api update empresa
    public function updateEmpresa(Request $request, $id)
    {
        //buscar el row por id
        $empresa = Empresa::find($id);
        if (is_null($empresa)) {
            return response()->json(['mensaje' => '0'], 404);
        }
        // $empresa->update($request->all());
        if ($empresa->update($request->all())) {
            return response()->json(['mensaje' => '1'], 200);
        }
        return response()->json(['mensaje' => '2'], 502);
    }

    //api consulta empresa especifica
    public function getEmpresaId($id)
    {
        // return response()->json(Cove::all(), 200);
        $emp = $this->getCompanyBySession($id);
        //$emp = Empresa::find($id);
        if (is_null($emp)) {
            return response()->json(['Mensaje' => 'registro no encontrado', 400]);
        }
        //$empresa = response()->json($emp::find($id), 200);
        $empresa = response()->json($emp, 200);
        return $empresa;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) //registrar una nueva empresa
    {
        //creamos la una variable para la url de la api mas el id del registro a modificar
        $url = $this->Urldominio() . 'api/insertEmpresa';

        //tipo de token
        $type = Session::get('token_type');
        //token
        $token = Session::get('api_token');
        //arreglo para enviar datos y registrar
        $array = [
            'rfc' => $request['rfc'],
            'nombre' => $request['nombre']
        ];

        $data_json = json_encode($array);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: ' . $type . ' ' . $token));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response  = curl_exec($ch);
        curl_close($ch);

        $empresa = json_decode($response, true);
        // dd($empresa['mensaje']);

        //si el mensaje es 1 retorna la vista y mensaje exitoso
        if ($empresa['mensaje'] == 1) {
            $mensaje = "1";
            return redirect('empresas')->with("message", $mensaje);
        } else {
            $mensaje = "0";
            return redirect('empresas')->with("message", $mensaje);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id) //view edit
    {
        // dd($request);
        //obtenemos datos de la empresa
        $empresa = $this->obtenerempresaId($id);

        return view('empresa.edit')->with('empresa', $empresa); //retornamos la vista mas los datos
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) //upadate empresa
    {
        //creamos la una variable para la url de la api mas el id del registro a modificar
        $url = $this->Urldominio() . 'api/updateEmpresa/' . $id;

        //obtenemos solo los datos a utilizar
        $array = [
            'rfc' => $request['rfc'],
            'nombre' => $request['nombre']
        ];
        $data_json = json_encode($array);
        //tipo de token
        $type = Session::get('token_type');
        //token
        $token = Session::get('api_token');
        //realizamos la peticion a la api para la modificacion
        //   $empresa = HTTP::put($url, $array);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: ' . $type . ' ' . $token, 'Content-Length: ' . strlen($data_json),));
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response  = curl_exec($ch);
        curl_close($ch);
        $empresas = json_decode($response, true);

        //si el mensaje es 1 retorna la vista y mensaje exitoso
        if ($empresas['mensaje'] == 1) {
            $mensaje = "1";
            return redirect('empresas')->with("message", $mensaje);
        } else {
            $mensaje = "0";
            return redirect('empresas')->with("message", $mensaje);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) //delete empresa, used api delete
    {
        // $mensaje = HTTP::delete($this->Urldominio().'api/deleteEmpresa/'.$id."?token=".Session::get('token'));
        $url = $this->Urldominio() . 'api/deleteEmpresa/' . $id;
        //tipo de token
        $type = Session::get('token_type');
        //token
        $token = Session::get('api_token');
        //realizamos la peticion a la api para la modificacion
        //   $empresa = HTTP::put($url, $array);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: ' . $type . ' ' . $token));
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response  = curl_exec($ch);
        curl_close($ch);

        $mensaje = json_decode($response, true);
        //dd($mensaje);
        if ($mensaje['mensaje'] == 1) {
            return redirect('empresas')->with('message', '1');
        } else {
            return redirect('empresas')->with('message', '0');
        }
    }

    public function registrarEmpresa($id)
    {

        //obtenemos los datos con el id de la empresa
        //$empresa = $this->obtenerempresaId($id);
        $empresa = $this->getCompanyBySession($id);
        //dd($empresa);

        //se agregan los datos de la empresa a la sesion para poder ser utilizadas en otras vistas y/o controladores
        Session::put('id', $empresa['id']);
        Session::put('rfc', $empresa['rfc']);
        Session::put('empresa', $empresa['nombre']);

        //redireccionamos al inicio
        return redirect('/index');
    }

    public function getCompanyBySession($id){
        $companies = json_decode(Session::get('empresas'));

        if($companies){
            foreach ($companies as $company){
                if($company->empresa === $id){
                    return [
                        'id'        => $company->empresa,
                        'nombre'    => $company->nombreGrupo,
                        'rfc'       => $company->rfc,
                    ];
                }
            }
        }

        return null;
    }

    //obtenes los datos de la empresa por su id, desde la api
    public function obtenerempresaId($id)
    {

        $url = $this->Urldominio() . 'api/getEmpresaId/' . $id;
        //tipo de token
        $type = Session::get('token_type');
        //token
        $token = Session::get('api_token');
        //peticion get
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Content-type: application/x-www-form-urlencoded',
                'Authorization: ' . $type . ' ' . $token,
                'Cookie: DT=DI0EIjjLIBMTtSqxAYujXFV6A'
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $empresas = json_decode($response, true);
        // dd($empresa);
        return $empresas;
    }
}
