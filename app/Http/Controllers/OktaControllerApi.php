<?php

namespace App\Http\Controllers;

use App\Api\Okta\OktaService;
use App\Models\Empresa;
use App\Models\Grupo;
use App\Models\User;
use App\Services\OktaServicesApi;
use Illuminate\Http\Request;

class OktaControllerApi extends Controller
{

    protected $grupos = [];

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function obtainTokenOkta(Request $request)
    {
        try {
            $request->validate([
                'user' => 'required',
                'password' => 'required',
            ]);
            $req = $request->all();
            $okta = new OktaServicesApi();
            $tokenResponse = $okta->linkTokenOkta($req['user'], $req['password']);
            $tokenResponse = json_decode($tokenResponse);
            if (!$tokenResponse->token)
                throw new \Exception("Unauthorized, wrong username or password", 401);
            /** @var User $usuario */
            $usuario = User::where('email', $tokenResponse->usuario->email)->first();
            if (!empty($usuario)) {
                if (!empty($usuario->api_token)) {
                    $okta->killTokenOkta($usuario->api_token);
                }
                //$usuario->update(['api_token' => null]);
                $usuario->api_token = $tokenResponse->token;
                $usuario->save();
                $arrEmp = $this->saveEmpresas($tokenResponse->usuario->empresas->rows, $tokenResponse->usuario->email);
                $this->saveSyncCompany($usuario, $arrEmp);
            } else {
                $usuario = User::updateOrCreate(
                    ["name" => (empty($tokenResponse->usuario->nombre)) ? $tokenResponse->usuario->email : $tokenResponse->usuario->nombre, 'email' => $tokenResponse->usuario->email, 'password' => 'e31927f54d3d', 'role' => 'admin'],
                    ['api_token' => $tokenResponse->token]
                );
                $usuario->api_token = $tokenResponse->token;
                $usuario->save();
                $arrEmp = $this->saveEmpresas($tokenResponse->usuario->empresas->rows, $tokenResponse->usuario->email);
                $this->saveSyncCompany($usuario, $arrEmp);
            }
            $response = ['status' => 'success', 'token' => $tokenResponse->token];
        } catch (\Exception $e) {
            $message = ['error' => true, 'message' => $e->getMessage()];
            return response()->json($message, 400);
        }

        return response()->json($response);
    }

    /**
     * @param $data
     * @return Empresa|false
     */
    private function makeEmpresaRequest($data)
    {
        $grupo = $this->makeGrupoRequest($data);
        if (strlen($data->rfc) <= 13) {
            /** @var Empresa $empresa */
            $empresa = new Empresa();
            $empresa->Rfc = $data->rfc;
            $empresa->RazonSocial = $data->razonSocial;
            $empresa->empresa_id = $data->empresa;
            $empresa->grupo_id = $grupo->id;
            $empresa->save();
            return $empresa;
        } else {
            return null;
        }
    }

    /**
     * @param $data
     * @return Grupo|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    private function makeGrupoRequest($data)
    {
        $grupo = Grupo::query()
            ->where('grupo_id', $data->id_grupo)
            ->first();
        if (empty($grupo)) {
            /** @var Grupo $grupo */
            $grupo = Grupo::create([
                'grupo_id' => $data->id_grupo,
                'nombre' => $data->nombreGrupo,
                'is_active' => true,
            ]);
        }
        if (!in_array($grupo->id, $this->grupos)) {
            $this->grupos[] = $grupo->id;
        }
        return $grupo;
    }

    /**
     * @param $empresas
     * @param $email
     * @return mixed
     */
    private function saveEmpresas($empresas, $email)
    {
        //Se registran las empresas o en su caso se ignora y se sincroniza con las empresas registradas
        foreach ($empresas as $empresa) {
            /** @var Empresa $emp */
            $emp = Empresa::where('rfc', $empresa->rfc)->first();
            if ($emp) {
                $arrEmp[] = $emp->id;
            } else {
                //Se crea la empresa dentro de la Api.
                $empSave = $this->makeEmpresaRequest($empresa);
                if (empty($empSave)) {
                    continue;
                }
                $arrEmp[] = $empSave->id;

            }
            unset($emp, $empSave);
        }
        return $arrEmp;
    }

    /**
     * @param User $usuario
     * @param $data
     * Se ingresan las empresas que contiene Okta y se enlaza con el usuario ingresado.
     */
    private function saveSyncCompany(User $usuario, $data)
    {
        $usuario->empresas()->sync($data);
    }
}
