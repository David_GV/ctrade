<?php

namespace App\Http\Controllers;

use App\Models\Usuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Models\CatUsertype;
use App\Http\Controllers\apiConfigController;
use Illuminate\Support\Facades\Log;
use Session;
use App\Models\Api;
use App\Models\Empresa;
use Illuminate\Support\Facades\View;

class UsuarioController extends Controller
{
    public function Urldominio()
    {
        $dominio =  app(apiConfigController::class)->dominio();
        // dd($dominio);
        // $this->Urldominio().
        return $dominio;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //obtenemos los valores desde la api
        $usuario = HTTP::get($this->Urldominio() . 'api/getUsuarios');
        $user = $usuario->json();

        //obtenemos los tipos de usuario
        $typeUser = HTTP::get($this->Urldominio() . 'api/getTypeUser');
        $type = $typeUser->json();

        return view('usuarios.index')->with('user', $user)->with('type', $type);
    }

    public function empresas()
    {
        //url de la api
        Log::info("URL => api/getEmpresas");

        // $Api = new Api('getEmpresas');
        // $response = $Api->GET();
        // $empresas = json_decode($response, true);


        $Api = new Api('getEmpresas');
        $response = $Api->GET();
        $empresas = json_decode($response, true);

        Log::info("EMPRESA HTTP => " . $response);

        return view('usuarios.empresas')->with('empresas', $empresas);
    }

    //api para listar todos los usuario
    public function getUsuario()
    {
        return response()->json(Usuario::all(), 200);
    }

    //obtener tipo de usuario

    public function getTypeUser()
    {
        return response()->json(CatUsertype::all(), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $emp = HTTP::get($this->Urldominio() . 'api/getEmpresas');
        $typeUser = HTTP::get($this->Urldominio() . 'api/getTypeUser');
        $type = $typeUser->json();

        return view('usuarios.create')->with('type', $type)->with('emp', $emp);
    }

    //registrar un nuevo usuario
    public function insertUsuario(Request $request)
    {

        $usuario = Usuario::create($request->all());
        $erro = ['mensaje' => 'error al guardar'];
        $ok = response($usuario, 200);
        if ($ok) {
            return redirect('usuarios');
        } else {
            return $erro;
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public  function getEmpresas()
    {
        $companies = json_decode(Session::get('empresas'));
        $showCompanies = [];
        foreach ( $companies as $company){
            $showCompanies[] = [
                'id'        => $company->empresa,
                'nombre'    => $company->nombreGrupo,
                'rfc'       => $company->rfc,
            ];
        }

        return response()->json(
            View::make('usuarios.empresas', [
             //"empresas" => Empresa::all()
                "empresas" => $showCompanies
            ])->render(),
            200
        );
    }
}
