<?php


namespace App\Services;


use GuzzleHttp\Client;

class OktaServicesApi
{
    /** @var Client */
    protected $client;
    /** @var string Constante que marca la URL base para Okta */
    const URL_BASE = 'https://access.cpavision.mx/';

    /**
     * OktaService constructor.
     * @param Client $client
     */
    public function __construct()
    {
        $this->client = new Client();
    }

    /**
     * @param $usuario
     * @param $password
     * @return false|string
     */
    public function linkTokenOkta($usuario, $password)
    {
        try {
            $body = $this->makeBodyRequest($usuario, $password);
            $response = $this->client->post(self::URL_BASE . 'getTokenApi', ['json' => $body])->getBody()->getContents();
        } catch (\Exception $e) {
            throw new \Exception("Unauthorized, usuario o contraseña incorrectos",401);
        }
        return $response;
    }

    /**
     * @param $token
     * @return false|string
     */
    public function getTokenOkta($token)
    {
        $response = $this->client->get(self::URL_BASE . 'introspectTokenApi/' . $token)->getBody()->getContents();
        return $response;
    }

    /**
     * @param $token
     * @return false|string
     */
    public function killTokenOkta($token)
    {
        $response = $this->client->get(self::URL_BASE . 'logoutApi/' . $token)->getBody()->getContents();
        return $response;
    }

    /**
     * @param $usuario
     * @param $password
     * @return array
     */
    protected function makeBodyRequest($usuario, $password)
    {
        $response = [
            "usuario" => $usuario,
            "password" => $password,
            "producto" => 24
        ];
        return $response;
    }
}
