<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cove;
use App\Models\ConfigEmpresa;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use App\Models\XmlToArray;
use Illuminate\Support\Facades\DB;
use Session;
use App\Http\Controllers\apiConfigController;
use Illuminate\Http\Response;
class CovesController extends Controller
{
    public function Urldominio(){
        $dominio =  app(apiConfigController::class)->dominio();
       // dd($dominio);
      // $this->Urldominio().
        return $dominio;
    }

    #region  view
    //
    public function index($id) //id de la empresa
    {
        $url = $this->Urldominio().'api/getCove/'.$id;
        //tipo de token
        $type = Session::get('token_type');
        //token
        $token = Session::get('api_token');
        //peticion get
        $curl = curl_init();
             curl_setopt_array($curl, array(
                    CURLOPT_URL => $url,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'GET',
                    CURLOPT_HTTPHEADER => array(
                        'Content-type: application/x-www-form-urlencoded',
                        'Authorization: '.$type.' '.$token,
                        'Cookie: DT=DI0EIjjLIBMTtSqxAYujXFV6A'
                ),
             ));
             $response = curl_exec($curl);
             curl_close($curl);
             $coves =json_decode($response, true);

        return view('cove.index', ['coves'=> $coves]);
    }

    public function show_unsigned_cove(Request $request, $id_expediente){
        //ide de empresa en sesion
        $id_empresa = Session::get('id');
        //url de la api
        $url = $this->Urldominio()."api/CoveExpediente/".$id_empresa;
        //tipo token
        $type = Session::get('token_type');
        //token
        $token = Session::get('api_token');
        //peticion get
        $curl = curl_init();
             curl_setopt_array($curl, array(
                    CURLOPT_URL => $url,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'GET',
                    CURLOPT_HTTPHEADER => array(
                        'Content-type: application/x-www-form-urlencoded',
                        'Authorization: '.$type.' '.$token,
                        'Cookie: DT=DI0EIjjLIBMTtSqxAYujXFV6A'
                ),
             ));
             $response = curl_exec($curl);
             curl_close($curl);
             $coves =json_decode($response, true);




        //$coves = Cove::whereNull('id_expediente')->where('id_empresa','=',Session::get('id'))->get();

        return view('cove.unsigned',['id_expediente'=>$id_expediente,'coves'=> $coves]);

    }
    #endregion view

    #region api
    //ver los coves en expedientes
        public function getCovePedimentoByidExp($id){
            $coves = Cove::getCovePedimentoByidExp($id);

            return response()->json($coves, 200);
        }
        //trae la consulta del cove que no teiene expediente
        public function CoveExpediente($id){ //id_empresa
            $coves = Cove::whereNull('id_expediente')->where('id_empresa',$id)->get();
            return response()->json($coves, 200);
        }

        //genrando lista de coves por id de empresa, (api)
        public function getCove($id){

            $cove = Cove::where('id_empresa', $id )->get();
            if(is_null($cove)){
                return response()->json(['Mensaje'=>'registro no encontrado', 400]);
            }
            $empresa = response()->json($cove, 200);
            return $empresa;

        }

           //genrando lista de coves por id, (api)
           public function getCoveId($id){

            $cove = Cove::where('id', $id )->get();
            if(is_null($cove)){
                return response()->json(['Mensaje'=>'registro no encontrado', 400]);
            }
            return response()->json($cove, 200);


        }

        //api guardar la informacion del cove
        public function GuardarCove(Request $request){
            if($empresa = Cove::create($request->all())){
                return response()->json(['mensaje'=>'1'], 200);
            }
            else{
                return response()->json(['mensaje'=>'0'], 404);
            }
        }

        //obtener cove por id de expediente
         public function getCoveIdExpediente($id){
            $cove = Cove::where('id_expediente', $id)->get();
            if($cove == "[]" ){
                return response()->json(['Mensaje'=>'registro no encontrado', 400]);
            }else{
                $cove = response()->json($cove, 200);
                 return $cove;
            }
        }

        //modificar id de expediente del documento
        public function updateCoveIdExpediente(Request $request, $id){ //id del pedimento
            $idExpediente = $request['id_expediente'];
            $cove = Cove::where('id' ,$id)->update(['id_expediente' => $idExpediente]);
            return response()->json($cove, 200);
        }


    #endregion

    public function cargar_cove(){
       return view('cove.cargar');
    }

    public function upload_cove(Request $request, $id)
    { //el id es de la empresa

        //validamos que traiga un archivo
        $this->validate($request, [
            'cove_xml' => 'required',
            'cove_pdf' => 'required'
        ],[
            'cove_xml.required' => 'El archivo XML es requerido',
            'cove_pdf.required' => 'El archivo PDF es requerido'
        ]);

        //obtenemos el campo file definido en el formulario
        $inputs =['cove_xml','cove_pdf'];
        //estas lineas son para mandarle el nuevo nombre del pdf a cargar en bd
        $pdf_s = $request->file('cove_xml');
        $pdf_n = $pdf_s->getClientOriginalName();
        $pdf_e = explode(".", $pdf_n);
        $pdf = $pdf_e[0].".pdf";

        $name_cove = $request->input('num_cove');
        $id_expediente = $request->input('expediente');

        foreach ($inputs as $input) {

            if($request->file($input)){
                $file = $request->file($input);
                // se obtiene el mimetype para validar que los tipos de archivo sean los correctos, se realiza aqui porque la funcion validate mimetype no funciona con text/xml
                $mime = $file->getClientMimeType();
                // validar el mimetype del archivo xml
                if(!strcmp($input,'cove_xml') && strcmp($mime,'application/xml') && strcmp($mime,'text/xml')){
                    return redirect()->back()->withErrors(['cove_xml'=>'El tipo de archivo ingresado no coincide con el tipo de archivo XML']);
                    // validar el mimetype del archivo pdf
                }elseif(!strcmp($input,'cove_pdf') && strcmp($mime, 'application/pdf') && strcmp($mime, 'text/pdf')){
                    return redirect()->back()->withErrors(['cove_pdf'=>'El tipo de archivo ingresado no coincide con el tipo de archivo PDF']);
                }
                // Guarda archivo y otras cosas
                $resultado = $this->save_file($request, $file, $name_cove, $id_expediente, $id);
                //guarda los registros en la base de datos
                if(!empty($resultado)){
                    $respuesta = $this->save_info($name_cove, $resultado, $id_expediente, $pdf);
                    if ($respuesta) {
                       // $coves = Cove::where('id_empresa','=',Session::get('id'))->get();
                        $id_empresa = Session::get('id');
                        //url de la api
                        $url = $this->Urldominio().'api/getCove/'.$id_empresa;
                         //tipo de token
                        $type = Session::get('token_type');
                        //token
                        $token = Session::get('api_token');
                        //peticion get
                        $curl = curl_init();
                            curl_setopt_array($curl, array(
                                    CURLOPT_URL => $url,
                                    CURLOPT_RETURNTRANSFER => true,
                                    CURLOPT_ENCODING => '',
                                    CURLOPT_MAXREDIRS => 10,
                                    CURLOPT_TIMEOUT => 0,
                                    CURLOPT_FOLLOWLOCATION => true,
                                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                    CURLOPT_CUSTOMREQUEST => 'GET',
                                    CURLOPT_HTTPHEADER => array(
                                        'Content-type: application/x-www-form-urlencoded',
                                        'Authorization: '.$type.' '.$token,
                                        'Cookie: DT=DI0EIjjLIBMTtSqxAYujXFV6A'
                                ),
                            ));
                            $response = curl_exec($curl);
                            curl_close($curl);
                            $coves =json_decode($response, true);
                    }else{
                        return redirect()
                            ->back()
                            ->with('message','Error en la carga.');
                    }
                }else{
                    return redirect()
                        ->back()
                        ->with('message','Error en la carga.');
                }
            }
        }
        if(!empty($id_expediente)) {
            $notificacion = array(
                'mensaje' => 'Carga realizada exitosamente',
                'alert-type' => 'success'
            );
            return view('documentos.cargar', compact('id_expediente'))->with('operacion_coves',$notificacion);
        }
        return redirect('coves/' . $id);
    }


    //metodo que almacena el archivo en el servidor
    public function save_file($request, $file, $name_cove, $id_expediente, $id)
    {

        // Obtiene el tipo de contenido del archivo
        $mime = $file->getClientMimeType();
        // Obtenemos el nombre del archivo
        $fileName   = $file->getClientOriginalName();
        $datos_cove = [];
        $id_empresa = Session::get('id');
        //url api
        $url = $this->Urldominio()."api/ConfigEmpresa/".$id_empresa;
         //tipo de token
         $type = Session::get('token_type');
         //token
         $token = Session::get('api_token');
         //peticion get
         $curl = curl_init();
              curl_setopt_array($curl, array(
                     CURLOPT_URL => $url,
                     CURLOPT_RETURNTRANSFER => true,
                     CURLOPT_ENCODING => '',
                     CURLOPT_MAXREDIRS => 10,
                     CURLOPT_TIMEOUT => 0,
                     CURLOPT_FOLLOWLOCATION => true,
                     CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                     CURLOPT_CUSTOMREQUEST => 'GET',
                     CURLOPT_HTTPHEADER => array(
                         'Content-type: application/x-www-form-urlencoded',
                         'Authorization: '.$type.' '.$token,
                         'Cookie: DT=DI0EIjjLIBMTtSqxAYujXFV6A'
                 ),
              ));
              $response = curl_exec($curl);
              curl_close($curl);
              $folderEmpresa =json_decode($response, true);

        if ($id_expediente == null) {
            $ruta = $folderEmpresa['value'] . '/coves/';
        } else {
            $ruta = $folderEmpresa['value'] . '/' . $id_expediente . '/coves/';
        }
        $this->pathCove = $path = storage_path("app/" . $ruta . $fileName);
        /* $path= "uploads/".$name_cove.'/'; */
        if (!strcmp($mime, "application/xml") || !strcmp($mime, "text/xml")) {

            if ($file->isValid()) {
                //die($request->file('cove_xml'));

                Storage::put($ruta.$fileName, file_get_contents($request->file('cove_xml')->getPathName()));
                $this->filename = explode('.', $fileName);
                //INICIO-convierto el xml en un JSON
                //aca hay que validar el contenido del xml
                $xml_contenido = file_get_contents($path);

                $array_cove = XmlToArray::convert($xml_contenido);

                if(!isset($array_cove['comprobantes'])){

                    $array_ok['comprobantes'] = $array_cove['Body']['solicitarRecibirCoveServicio']['comprobantes'];
                    $array_ok['_root'] = $array_cove['_root'];
                    $array_cove = $array_ok;
                }

                $json_cove = collect($array_cove)->tojson();
                $datos_cove = array (
                    'xml'       => $fileName,
                    'json_cove' => $json_cove
                );
            }
        } elseif (!strcmp($mime, "application/pdf") || !strcmp($mime, "text/pdf")) {
            //Storage::put($ruta . $fileName, file_get_contents($request->file('cove_pdf')->getPathName()));
            Storage::put($ruta .'/'. $this->filename[0].'.pdf', file_get_contents($request->file('cove_pdf')->getPathName()));
        }
        return $datos_cove;
    }

    //metodo que guarda la informacion del cove
    public function save_info($name_cove,$datos_cove,$id_expediente, $pdf){
          //id del usuario logiado
          $id_usuario = Session::get('id_usuario');

        //creamos un arreglo para mandar los datos del cove a la api
       $array = [
            "id_agente" => "0", //pendiente
            "id_usuario" => $id_usuario, //el uno es temporal
            "id_expediente" => $id_expediente,
            "id_empresa" => Session::get('id'),
            "usr_num_cove" => $name_cove,
            "id_fiscal" => "0", //pediente
            "xml" => $datos_cove['xml'],
            "json_cove" => $datos_cove['json_cove'],
            "pdfs" => $pdf
        ];
        //url de la api para guardar la informacion
        $url = $this->Urldominio()."api/insertCove";
         //tipo de token
         $type = Session::get('token_type');
         //token
         $token = Session::get('api_token');
          //arreglo para enviar datos y registrar
          $data_json = json_encode($array);
          //realizamos la peticion a la api y mandamos valores
          $ch = curl_init();
          curl_setopt($ch, CURLOPT_URL, $url);
          curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: '.$type.' '.$token));
          curl_setopt($ch, CURLOPT_POST, 1);
          curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          $response  = curl_exec($ch);
          curl_close($ch);

          $cove = json_decode($response, true);

         //si el mensaje es 1 retorna la vista y mensaje exitoso
         if($cove['mensaje'] == 1){
            return true;
         }else{
            return false;
        }

    }


    //ver detalles de cove al asigar a un expediente
    public function coveFacturaShow($id_cove,$id_expediente){

        //consultamos la api la lista de cove
        $url = $this->Urldominio()."api/getCoveId/".$id_cove;
        $type = Session::get('token_type');
        //token
        $token = Session::get('api_token');
        //peticion get
        $curl = curl_init();
             curl_setopt_array($curl, array(
                    CURLOPT_URL => $url,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'GET',
                    CURLOPT_HTTPHEADER => array(
                        'Content-type: application/x-www-form-urlencoded',
                        'Authorization: '.$type.' '.$token,
                        'Cookie: DT=DI0EIjjLIBMTtSqxAYujXFV6A'
                ),
             ));
             $response = curl_exec($curl);
             curl_close($curl);
             $coves =json_decode($response, true);


        $bandera =  1; //ayudara a la redireccion de vistas
        if($coves[0]['id_expediente']){
            $bandera = 0;
        }

        $cove = json_decode($coves[0]['json_cove'] ,true);
        //dd($cove);
        return view ('cove.vista_factura',['cove'=>$cove,'id_expediente'=>$id_expediente,'bandera' => $bandera]);
    }

    //descargar pdf del cove
    public function descargarPDFCove($xml,$expediente_id,$empresa)
    {
        $file = explode('.', $xml);
        $path = $empresa . '/' . $expediente_id . '/coves/' . $file[0] . '.pdf';

        $content = Storage::get($path);
        $mime = Storage::mimeType($path);

        return (new Response($content, 200))
            ->header('Content-Type', $mime)
            ->header('Expires', '0')
            ->header('Cache-Control', 'must-revalidate')
            ->header('Pragma', 'public');
    }
    //descargar xml del cove
    public function descargarXMLCove($xml, $expediente_id,$empresa){
        $path = $empresa . '/' . $expediente_id . '/coves/' . $xml ;

        $content = Storage::get($path);
        $mime = Storage::mimeType($path);

        return (new Response($content, 200))
            ->header('Content-Type', $mime)
            ->header('Content-Disposition', 'attachment; filename='.$xml);
    }

    //asiganr cove a expediente
    public function asigna_cove($id_cove, $id_expediente){
        /*
        // $id = Agente Aduanal
        $id = Expediente::where('id',$id_expediente)->select('agente_aduanal')->first();

        $coves = Cove::where('id','=',$id_cove)->first();
        if ($coves) {
            Cove::where('id', $id_cove)
            ->update(['id_expediente'=> $id_expediente,'id_agente'=>$id->agente_aduanal]);

            //Muevo el archivo de Posición
            $folderEmpresa = ConfigEmpresa::where('empresa_id',session()->get('id'))
                            ->where('configuracion','folder_storage')
                            ->first();

            //File::move("storage/$folderEmpresa->value/$coves->xml",  "storage/$folderEmpresa->value/$id_expediente/$coves->xml");
            // Mueve el archivo de carpeta en Storage/{Empresa}/{id_expediente}/
            //dd($folderEmpresa->value.'/'.$id_expediente.'/'.$coves->xml);
              $path = "/var/www/html/php72/ctrade/storage/app/";

            if(!file_exists($path.$folderEmpresa->value.'/'.$id_expediente.'/coves/')){
                mkdir($path.$folderEmpresa->value.'/'.$id_expediente.'/coves/',0777,true);
            }else{
                echo "YA ESTa";
            }

                if (copy($path.$folderEmpresa->value.'/coves/'.$coves->xml,$path.$folderEmpresa->value.'/'.$id_expediente.'/coves/'.$coves->xml))
                {
                    echo "OK";
                }


                //Storage::move($folderEmpresa->value.'/coves/'.$coves->xml, $folderEmpresa->value.'/'.$id_expediente.'/coves/'.$coves->xml);

            $archivos = explode('.',$coves->xml);
            $ruta = storage_path('app/'.$folderEmpresa->value.'/coves/'.$archivos[0].'.pdf');
            echo $ruta;
            if(file_exists($ruta)){
                Storage::move($folderEmpresa->value.'/coves/'.$archivos[0].'.pdf', $folderEmpresa->value.'/'.$id_expediente.'/coves/'.$archivos[0].'.pdf');
            }
            header("location: /ctrade/expedientes/$id_expediente");
            die();
            //return redirect()->route('expediente.show', array('id' => $id_expediente));

        } else {
           echo "error";
        }

        $coves = Cove::where('id_expediente','=0')->get();
        */
    }

    //metodo para asignar un pedimento al expediente
    public function asigna_coveExpediente($id_cove, $id_expediente)
    {
        //obtenemos id de la empresa
        $id_empresa = Session::get('id');
        //obtenemos la configuracion de la carpeta de la empresa
        $url = $this->Urldominio()."api/ConfigEmpresa/".$id_empresa;
        //type token
        $type = Session::get('token_type');
        //token
        $token = Session::get('api_token');
        //peticion get
        $curl = curl_init();
             curl_setopt_array($curl, array(
                    CURLOPT_URL => $url,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'GET',
                    CURLOPT_HTTPHEADER => array(
                        'Content-type: application/x-www-form-urlencoded',
                        'Authorization: '.$type.' '.$token,
                        'Cookie: DT=DI0EIjjLIBMTtSqxAYujXFV6A'
                ),
             ));
             $response = curl_exec($curl);
             curl_close($curl);
             $folderEmpresa =json_decode($response, true);


        $path = storage_path("app/".$folderEmpresa['value']."/coves"); //ruta actual
        $new_path = storage_path("app/".$folderEmpresa['value']."/".$id_expediente."/coves"); //nueva ruta

        //consultamos el cove para saber el nombre del archivo xml y pdf
        $url_cove = $this->Urldominio()."api/getCoveId/".$id_cove;
        $curl = curl_init();
        curl_setopt_array($curl, array(
               CURLOPT_URL => $url_cove,
               CURLOPT_RETURNTRANSFER => true,
               CURLOPT_ENCODING => '',
               CURLOPT_MAXREDIRS => 10,
               CURLOPT_TIMEOUT => 0,
               CURLOPT_FOLLOWLOCATION => true,
               CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
               CURLOPT_CUSTOMREQUEST => 'GET',
               CURLOPT_HTTPHEADER => array(
                   'Content-type: application/x-www-form-urlencoded',
                   'Authorization: '.$type.' '.$token,
                   'Cookie: DT=DI0EIjjLIBMTtSqxAYujXFV6A'
           ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $dataCove =json_decode($response, true);
        //dd($dataCove);
        //obtenmos los nombres de los archivos xml y pdf
        $file_xml = $dataCove[0]['xml'];
        $file_pdf = $dataCove[0]['pdfs'];

        //creamos actuales
        $path_xml = $path."/".$file_xml; //ruta actual completa del xml
        //dd($path_xml);

        //movemos los archivos a la nueva ruta
        if(file_exists($path_xml)){ //si existe se movera los dos archivos

            if(file_exists($new_path."/".$file_xml)){
               // dd($id_expediente);
                 //si existe un archivo con el mismo nombre
                return redirect('cove/unsigned_cove/'.$id_expediente)
                                        ->with("message", "Operacion No realizada ¡Se encontraron nombres iguales de cove xml y pdf!");
            }else{
                Storage::move($folderEmpresa['value']."/coves/".$file_xml, $folderEmpresa['value']."/".$id_expediente."/coves/".$file_xml); //movemos el xml
                Storage::move($folderEmpresa['value']."/coves/".$file_pdf, $folderEmpresa['value']."/".$id_expediente."/coves/".$file_pdf); //movemos el pdf
               // dd('entro');
               //actualiza los campos en la base de datos, (al campo id_expediente le ponemos el expediente que se trae)
               $array = ["id_expediente" =>  $id_expediente];
               //url de api mpdificar
               $url = $this->Urldominio()."api/updateCoveIdExpediente/".$id_cove;
               //enviamos los datos a la api para el update
               $data_json = json_encode($array);
               $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: '.$type.' '.$token, 'Content-Length: ' . strlen($data_json), ));
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
                curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $response  = curl_exec($ch);
                curl_close($ch);

                $cove =json_decode($response, true);

                //si todo salio bien hacemos un return
                return redirect('cove/unsigned_cove/'.$id_expediente)
                                        ->with("message", "¡operación Exitosa!");
            }

        }
        //si no se logro mover los archivos ni modificar en bd
        return redirect('cove/unsigned_cove/'.$id_expediente)
                                                ->with("message", "¡operación No Realizada!");
    }


}
