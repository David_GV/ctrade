<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;


class Auth_okta
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
       
       $token = $request->session()->get('token');
      
        if(isset($token)){
            return $next($request);
        }else{
           //de lo contrarion redireccionara al login de okta
       //return redirect('https://dev-534696.okta.com/oauth2/ausdw4dezmhhvT9uP4x6'); //no encontro la pagina
         return redirect('https://cpavision.mx/');
        }
    }
}
