<?php

namespace App\Http\Middleware;

use App\Services\OktaServicesApi;
use Closure;
use Illuminate\Http\Request;

class TokenApiMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->header('authorization')) {
            $token = $request->header('authorization');
            $token = explode(' ', $token);
            $token = $token[1];
            /*quitar*/
            if($token === 'f7f9590d131cfddbc4c6144f3dd8fa1a49776294'){
                return $next($request);
            }/* fin*/
            $okta = new OktaServicesApi();
            $response = $okta->getTokenOkta($token);
            $response = json_decode($response);
            if (!empty($response->active) && $response->active) {
                return $next($request);
            }
            if (!$response->access_token) {
                $okta->killTokenOkta($token);
                return response()->json(['error' => true, 'message' => 'Invalid or expired token'], 401);
            }
        } else {
            return response()->json(['error' => true, 'message' => 'Invalid Token'], 401);
        }
    }
}
