<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Session;

class ApiMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
       // dd(Session::get('token'));
       $token = session()->get('token'); //obtengo el token de la session
     
        if(isset($request['token'])){
               return $next($request);
        }else{
              return response()->json(['status' => 'acceso no autorizado']);
        }
    } 
}
