<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Session;
use App\Http\Controllers\OktaController;

class Api extends Model
{
    use HasFactory;

    public $dominio;
    public $route;
    public $url;
    public $type;
    private $token;
    
    //Configuracion 
    public function __construct($route)
    {
        $this->dominio = env('API_DOMINIO'); //dominio
        $this->route = $route; //routa al metodo
        $this->url = $this->dominio."api/$route";

        $this->type = Session::get('token_type');  //tipo de token
        $this->token = Session::get('api_token'); //token
    }   

    //peticion get
    public  function GET()
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Content-type: application/x-www-form-urlencoded',
                'Authorization: ' . $this->type . ' ' . $this->token,
                'Cookie: DT=DI0EIjjLIBMTtSqxAYujXFV6A'
            ),
        ));

        $response = curl_exec($curl);
        $status = curl_getinfo($curl);
        curl_close($curl);

        $status = $status['http_code'];

        if ($status === 200) {
         return $response;
        } else {
            return   app(OktaController::class)->cerrar_sesion();
        }
    }
}
