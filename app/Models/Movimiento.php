<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Movimiento extends Model
{
    use HasFactory;
    protected $table = 'movimientos';
    protected $fillable = [
        "tipo",
        "idTipo",
        "monto_factura",
        "monto_pagado",
        "monto_anterior",
        "rfc",
        "idExpediente",
        "fechaPago",
        "uidPago",
        "id_agencia",
        "id_empresa",
        "polizaContable",
        "id_facturacargada"
    ];


    public static function getMovimientoExp_2($id)
    { //id_expediente
        return  Movimiento::where('idExpediente', $id)->where('idTipo', 2)->get();
    }

    public static function pagosFacturasExp($id)
    {
        return Movimiento::where('idExpediente', $id)
            ->leftJoin('facturas_cargadas', 'facturas_cargadas.id', 'movimientos.id_facturacargada')
            ->select('facturas_cargadas.*', 'movimientos.polizaContable', 'movimientos.monto_anterior', 'movimientos.uidPago', 'movimientos.fechaPago', 'movimientos.monto_factura')
            ->where('idTipo', 1)->get();
    }
}
