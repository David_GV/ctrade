<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class datosConexionEmpresa extends Model
{
    use HasFactory;
    protected $table = "datos_conexion_empresas";
    protected $fillable =['id_empresa','host','user','password','path'];
}
