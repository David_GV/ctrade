<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ConfigEmpresa extends Model
{
    use HasFactory;
    protected $table = 'config_empresa';
    protected $fillable = ['empresa_id','configuracion','value'];


   //api, obtiene el nombre de la configuracion de la carpeta, de la empresa en session
   public static function ConfigEmpresa($id)
   {
      return ConfigEmpresa::where('empresa_id', $id)
           ->where('configuracion', 'folder_storage')
           ->first();
   }
}
