<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Aduana extends Model
{
    use HasFactory;
    protected $table = 'catalogos_aduanas';
    //public $timestamps = false;
    protected $fillable = ['aduana', 'seccion', 'denominacion', 'compuesto'];
   // protected $hidden = ['id'];

    public static function nombreAduana($aid)
    {
        $aduana = self::where('compuesto', $aid)->first();
        if ($aduana->count() > 0) {
            return $aduana->denominacion;
        }

        return 'No definido';
    }
}
