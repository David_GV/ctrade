<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PedimentosAsignado extends Model
{
    use HasFactory;
    protected $table = 'pedimento_asignados';
    protected $fillable = ['id_pedimento', 'id_expediente'];
}
