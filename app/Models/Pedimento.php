<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pedimento extends Model
{
    use HasFactory;
    /**
     * @var string
     */
    protected $table = 'pedimentos';

    /**
     * @var array
     */
    protected $fillable = [
        'pedimento',
        'aduanaDespacho',
        'fechaPago',
        'created_at',
        'impExpNombre',
        'tipoOperacion',
        'empresa_id',
        'expediente_id',
        'json',
        'archivoM',
        'archivoPDF'
    ];

    /**
     * @var array
     */
    protected $hidden = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function aduana()
    {
        return $this->hasOne('App\Models\Aduana', 'compuesto', 'aduanaDespacho');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function empresa()
    {
        return $this->belongsTo(Empresa::class);
    }


    public function pedimentosAsignado()
    {
        return $this->belongsToMany('App\PedimentosAsignado');
    }

    public static function getPedimentosAsignados($id)
    {
        return   Pedimento::select('pedimentos.*')
            ->leftJoin('pedimento_asignados', 'pedimentos.id', 'pedimento_asignados.id_pedimento')
            ->where('pedimentos.expediente_id', $id)
            ->get();
    }

    public static function reportePeriodoPedimentos($empresaId, $ejercicio, $periodo)
    {
        return  Pedimento::select(
            'id',
            'pedimento',
            'aduanaDespacho',
            'created_at',
            'impExpNombre',
            'tipoOperacion'
        )
            ->where('empresa_id', $empresaId)
            ->whereYear('created_at',  $ejercicio)
            ->whereMonth('created_at',   $periodo)
            ->get();
    }

    
    public static function reportePedimentos($empresaId)
    {
        return  Pedimento::select(
            'id',
            'pedimento',
            'aduanaDespacho',
            'created_at',
            'impExpNombre',
            'tipoOperacion'
        )
            ->where('empresa_id', $empresaId)
            ->get();
    }
}
