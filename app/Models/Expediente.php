<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\ConfigEmpresa;
use App\Models\FacturasCargadas;
use App\Models\Pedimento;
use App\Models\Movimiento;
use App\Models\Documento;
use Session;

class Expediente extends Model
{
    use HasFactory;
    protected $table = "expedientes";
    protected $fillable = ['expediente', 'agente_aduanal', 'nombre', 'descripcion', 'empresa_id', 'aduana_id', 'status'];


    /**
     * @var array
     */
    protected $hidden = ['empresa_id', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function empresa()
    {
        return $this->belongsTo(Empresa::class);
    }

    public static function getExpedienteById($id)
    {
        return Expediente::where('expedientes.id', $id)
            ->leftJoin('agencias', 'expedientes.agente_aduanal', 'agencias.id')
            ->select('expedientes.*', 'agencias.nombre as nombre_agente')
            ->first();
    }

    public static function getExpedienteByEmpresaAndDate($id_empresa, $inicio, $final)
    {
        return Expediente::where('empresa_id', $id_empresa)
            ->whereDate('expedientes.created_at', ">=", $inicio)
            ->whereDate('expedientes.created_at', "<=", $final)
            ->get();
    }

    public function ConfigEmpresa($empresaId)
    {
        return ConfigEmpresa::ConfigEmpresa($empresaId);
    }

    public function  getCovePedimentoByidExp()
    {
        return Cove::getCovePedimentoByidExp($this->id);
    }

    public  function getPedimentosAsignados()
    {
        return   Pedimento::select('pedimentos.*')
            ->leftJoin('pedimento_asignados', 'pedimentos.id', 'pedimento_asignados.id_pedimento')
            ->where('pedimentos.expediente_id', $this->id)
            ->get();
    }

    public function PedimentoAduanaExpedienteId()
    {
        return  Pedimento::select('pedimentos.*')
            ->leftJoin('pedimento_asignados', 'pedimentos.id', 'pedimento_asignados.id_pedimento')
            ->where('pedimentos.expediente_id', $this->id)
            ->get();
    }

    public function getFacturaExpediente()
    {
        return FacturasCargadas::where('id_expediente', $this->id)->get();
    }

    public function getMovimientoExp_2()
    {
        return  Movimiento::where('idExpediente',  $this->id)->where('idTipo', 2)->get();
    }

    public function pagosFacturasExp()
    {
        return Movimiento::where('idExpediente', $this->id)
            ->leftJoin('facturas_cargadas', 'facturas_cargadas.id', 'movimientos.id_facturacargada')
            ->select('facturas_cargadas.*', 'movimientos.polizaContable', 'movimientos.monto_anterior', 'movimientos.uidPago', 'movimientos.fechaPago', 'movimientos.monto_factura')
            ->where('idTipo', 1)->get();
    }

    public function getDocument()
    {
        return Documento::where('expediente_id',  $this->id)->get();
    }
}
