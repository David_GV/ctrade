<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Documento extends Model
{
    use HasFactory;
    protected $table = "documentos";
    protected $fillable = ['expediente_id', 'nota', 'nombreDocumento'];

    public function expediente()
    {
        return $this->belongsTo(Expediente::class);
    }

    public static function getDocument($id)
    {
        return Documento::where('expediente_id', $id)->get();
    }
}
