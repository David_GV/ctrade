<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FacturasCargadas extends Model
{
    use HasFactory;
    protected $table = "facturas_cargadas";
    protected $fillable = [
        "id_agente",
        "id_usuario",
        "id_expediente",
        "formaDePago",
        "tipo_factura",
        "emisor_rfc",
        "emisor_nombre",
        "receptor_rfc",
        "receptor_nombre",
        "total",
        "fecha",
        "poliza",
        "folio",
        "xml_file",
        "json_cfdi",
        "status_factura"
    ];

    public  static function getFacturaExpediente($id)
    {
        return FacturasCargadas::where('id_expediente', $id)->get();
    }
}
