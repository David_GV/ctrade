<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
   // use HasFactory;
   /* protected $table = 'empresa'; //indica la tabla a seleccionar
    protected $fillable = ['id', 'rfc', 'nombre'];*/

    protected $table = 'empresas';
    protected $fillable = ['id', 'rfc', 'nombre'];
    protected $hidden = ['updated_at', 'created_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function pedimentos()
    {
        return $this->hasMany(Pedimento::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function expedientes()
    {
        return $this->hasMany(Expediente::class);
    }

    static function getEmpresaByRFC($rfc){
        $empresa = Empresa::where('rfc',$rfc)->get()->first();;
        return $empresa;

    }
}
