<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CatalogoTipoPago extends Model
{
    use HasFactory;
    protected $table = "catalogo_tipo_pagos";
}
