<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cove extends Model
{
  use HasFactory;

  protected $table = 'coves';
  protected $fillable = ['id', 'id_agente', 'id_usuario', 'id_expediente', 'id_empresa', 'usr_num_cove', 'id_fiscal', 'xml', 'json_cove', 'pdfs'];


  public static function getCovePedimentoByidExp($id)
  {
    return Cove::select('coves.*', 'pedimentos.json')
      ->where('id_expediente', $id)
      ->leftJoin('pedimentos', 'pedimentos.expediente_id', 'coves.id_expediente')
      ->leftJoin('expedientes', 'expedientes.id', 'coves.id_expediente')
      ->get();
  }
}
