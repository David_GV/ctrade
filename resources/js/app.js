require("./bootstrap");
import Swal from "sweetalert2";

const cookies = {
    get: function (name) {
        var c = document.cookie.match("(^|;) ?" + name + "=([^;]*)(;|$)");

        return {
            name: name,
            value: c ? c[2] : "",
        };
    },
};

$(function () {
    const statusBtns = {
        disable: function (btn) {
            btn.setAttribute("disabled", true);
        },
        enable: function (btn) {
            btn.removeAttribute("disabled");
        },
    };

    const logout = () => {
        document.querySelector("#logout").click();
    };

    const loader = {
        html: function (type = "small", color = "blue") {
            const div = document.createElement("div");
            div.id = "loader";
            div.classList.add("center-align");
            div.innerHTML = `<div class="loader ${type} ${color}"></div>`;

            return div;
        },
        show: function (el, type = null, color = null) {
            this.hide(el);
            el.classList.add("loader-active");
            el.append(this.html(type, color));
        },
        hide: function (el) {
            const loader = el.querySelector("#loader");
            el.classList.remove("loader-active");
            if (loader) {
                loader.remove();
            }
        },
    };

    $('[data-toggle="tooltip"]').tooltip();

    $("[data-modal]").on("click", function () {
        $("#modal-content").load(
            $(this).attr("data-href"),
            function (response, status, xhr) {
                if (status == "error") {
                    alert("Error al obtener la información");
                    return;
                }
                $("#modal").modal();
            }
        );
    });

    $("#definirEmpresa").on("click", function () {
        const token = cookies.get("api_token");
        const tokenValue = token.value;

        statusBtns.disable(this);

        return fetch($(this).attr("data-href"), {
            method: "post",
            headers: {
                Authorization: "Bearer " + tokenValue,
            },
        })
            .then(async (response) => {
                if (!response.ok) {
                    await httpStatusErrors(response);
                }

                return response.json();
            })
            .then((response) => {
                $("#modal-content").append(response);
                $("#modal").modal();

                statusBtns.enable(this);
            })
            .catch((error) => {
                statusBtns.enable(this);

                Swal.fire({
                    title: error.message,
                    icon: "error",
                    showCloseButton: true,
                    showCancelButton: false,
                    focusConfirm: false,
                    confirmButtonText: "Ok",
                    timer: 2000,
                    timerProgressBar: true,
                });
            });
    });

    const btnConsultarPedimentos = document.querySelector(
        "#consultar-pedimentos"
    );

    if (btnConsultarPedimentos) {
        btnConsultarPedimentos.addEventListener("click", eventReportePdmnts);

        reportePdmntsInit();
    }

    async function reportePdmntsInit() {
        const url = window.location.href;

        var newURL = new URL(url);
        var ejercicio = newURL.searchParams.get("ejercicio");
        var periodo = newURL.searchParams.get("periodo");

        if (ejercicio && periodo) {
            const container = document.querySelector(
                "#content-data-pagination"
            );

            loader.show(container, "small");
            var response = await consultarPedimetos(ejercicio, periodo);
            loader.hide(container);

            if (response) {
                container.innerHTML = response.pedimentos;

                table("#table-pedimentos");
            }
        }
    }

    async function eventReportePdmnts(e) {
        e.preventDefault();
        const ejercicio = document.querySelector("#ejercicio").value;
        const periodo = document.querySelector("#periodo").value;
        const container = document.querySelector("#content-data-pagination");

        if (ejercicio.length == 0 || periodo.length == 0) {
            Swal.fire({
                title: "Ejercicio y periodo son requeridos.",
                icon: "info",
                showCloseButton: true,
                showCancelButton: false,
                focusConfirm: false,
                confirmButtonText: "Ok",
                timer: 2000,
                timerProgressBar: true,
            });

            return;
        }

        statusBtns.disable(this);
        loader.show(container, "small");

        var response = await consultarPedimetos(ejercicio, periodo);

        statusBtns.enable(this);
        loader.hide(container);

        if (response) {
            container.innerHTML = response.pedimentos;

            table("#table-pedimentos");
        }
    }

    async function consultarPedimetos(ejercicio = "", periodo = "") {
        const token = cookies.get("api_token");
        const tokenValue = token.value;

        var myInit = {
            method: "GET",
            headers: {
                Authorization: "Bearer " + tokenValue,
            },
            mode: "cors",
            cache: "default",
        };

        var myRequest = new Request(
            "api/reporte-pedimentos?ejercicio=" +
                ejercicio +
                "&periodo=" +
                periodo,
            myInit
        );

        return await fetch(myRequest)
            .then(async (response) => {
                if (!response.ok) {
                    httpStatusErrors(await response);
                }

                return response.json();
            })
            .then((response) => {
                return response;
            })
            .catch((error) => {
                Swal.fire({
                    title: error.message,
                    icon: "error",
                    showCloseButton: true,
                    showCancelButton: false,
                    focusConfirm: false,
                    confirmButtonText: "Ok",
                    timer: 2000,
                    timerProgressBar: true,
                });

                return error;
            });
    }

    function httpStatusErrors(response) {
        switch (response.status) {
            case 500:
                throw new Error(
                    "Ocurrió un error, intenta nuevamente en unos minutos."
                );
                break;
            case 419:
                throw new Error("La sesión ha expirado.");
                break;
            case 422:
                throw new Error("Error al obtener la información.");
                break;
            case 401:
                logout();
                throw new Error("No autorizado.");
                break;
            case 400:
                var r = response.json();

                throw new Error(r.message);
                break;

            default:
                var r = response.json();

                throw new Error(r.message);
                break;
        }
    }

    function table(el) {
        $(el).DataTable({
            language: {
                url: "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json",
            },
            lengthMenu: [
                [10, 25, 50, -1],
                ["10", "25", "50", "Ver Todo"],
            ],
            buttons: ["pageLength"],
        });
    }
});
