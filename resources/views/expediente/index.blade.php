@extends('layout.master')
@section('content')
    <div class="col-md-10 col-md-offset-1 col-sm-8 col-sm-offset-2 col-xs-12">
       @if(Session::has('message'))
        <div class="alert alert-success alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button> <strong>{{ Session::get('message') }}</strong>
        </div>
        @endif
        <a href="{{url('expedientes/create')}}" class="btn btn-sm btn-success btn-addon"><i class="glyphicon glyphicon-plus"></i> Crear expediente</a>
        <div class="table-responsive">
            <table class="table table-striped txt-medium" id="tableExpediente">
                <thead>
                <tr>
                    <th>N. Expediente</th>
                    <th>Nombre</th>
                    <th>Descripcion</th>
                    <th>Creado</th>
                    <th>Acciones</th>
                    <th>Estatus</th>
                </tr>
                </thead>
                <tbody>
                    @isset($expedientes)
                        @foreach($expedientes as $expediente)
                        <tr>
                            <td>N.E.: {{ $expediente['expediente'] }}</td>
                            <td>{{ $expediente['nombre'] }}</td>
                            <td>{{ $expediente['descripcion']}}</td>
                            <td>{{ date('d-m-Y', strtotime($expediente['created_at'])) }}</td>
                            <td>
                                <a class="btn btn-default btn-sm" href="{{url("expedientes/edit",$expediente['id'])}}" role="button" data-toggle="tooltip" data-placement="bottom" title="Editar Expediente"><span class="glyphicon glyphicon-pencil"></span></a>
                                <a class="btn btn-default btn-sm" href="{{url('expedientes/show',$expediente['id'])}}" role="button" data-toggle="tooltip" data-placement="bottom" title="Ver Expediente"><span class="glyphicon glyphicon-folder-open"></span></a>
                            </td>

                            <td>
                                @if ($expediente['status'] =='Abierto')
                                <span class="btn btn-success" >
                                {{ $expediente['status']}} <span class="icon-lock-open"></span>
                                </span>
                                @elseif ($expediente['status'] =='Cerrado')
                                <span class="btn btn-danger" >
                                {{ $expediente['status'] }} <span class="icon-lock"></span>
                                </span> 
                                @elseif ($expediente['status'] =='Proceso')
                                <span class="btn btn-info" >
                                {{ $expediente['status'] }} <span class="icon-lock"></span>
                                </span>                             
                                @endif                           
                             </td>
                        </tr>
                    @endforeach
                    
                    @endisset
                </tbody>
            </table>
        </div>
    </div>

@push('scripts')
    $(document).ready(function(){
        $('#tableExpediente').DataTable( {
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
            },
            lengthMenu: [
                [10, 25, 50, -1],
                ['10', '25', '50', 'Ver Todo']
            ],
            buttons: ['pageLength']
        });
    });
@endpush
@endsection