@extends('layout.master')
@section('content')
<div class="col-md-12">
	<div class="page-header">
    <h4></h4>
    <div class="col-md-12">
      <p><strong>Total pedimentos:</strong> </p>
      <p><strong>Total pedimentos encontrados:</strong> </p>
      <p><strong>Total pedimentos no encontrados:</strong> </p>
    </div>
    <a class="btn btn-default" href="" role="button"><span class="glyphicon glyphicon-ok"></span> Encontrados</a>
    <a class="btn btn-default " href="" role="button"><span class="glyphicon glyphicon-remove"></span> No Encontrados</a>
 
  </div>    
  <div class="col-md-12">
    <div class="table-responsive">
      <table id="table-pedimentos" class="table table-striped txt-small">
        <thead>
          <tr>
            <th>PEDIMENTO</th>
            <th>EMISOR</th>
            <th>EMISOR RFC</th>
            <th>RECEPTOR</th>
            <th>RECEPTOR RFC</th>
            <th>UUID</th>
            <th>.</th>
          </tr>
        </thead>
        <tbody id="pedimentos-loader">
      
        </tbody>
      </table>
      
    </div>
  </div> 
</div> 
@endsection