@extends('layout.master')
@section('content')
<div>
	<div class="page-header">
		<h4>Consulta de pedimentos</h4>
	</div>
	
	<form autocomplete="off" action="" class="form-inline mrg_top_bottom" method="GET">
		<div class="form-group">
			<select class="form-control" name="ejercicio" id="ejercicio">
				<option value="" selected="selected">Ejercicio</option>
				@for($i=(int)date("Y")-2;$i<=(int)date("Y");$i++)
				<option value="{{ $i }}" @if($i==(isset($ejercicio) ? $ejercicio :'')) selected="selected" @endif>{{ $i }}</option>
				@endfor
			</select>
		</div>
		<div class="form-group">
			<select class="form-control" name="periodo" id="periodo">
				<option value="" selected="selected">Periodo</option>
				@for($i=1;$i<13;$i++)
				<option value="{{ $i }}" @if($i==(isset($periodo)? $periodo: '')) selected="selected" @endif>{{ $i }}</option>
				@endfor
			</select>
		</div>
		<button id="consultar-pedimentos" type="sumbit" class="btn btn-primary">Consultar</button>
	</form>
    <div id="content-data-pagination">
		@include('pedimento.pdmtos')
		<div class="center-align">
			<h4>Seleccione el periodo</h4>
		</div>
    </div>   
</div> 
@endsection

@push('scripts')
<script src="{{ asset('js/ajax.pagination.js') }}"></script>
@endpush