@extends('layout.master')
@section('content')
 
    <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12">
        
        @if(Session::has('message'))
             @if(Session::get('message') == 0)
                <div class="alert alert-danger alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button> <strong>¡Operacion no realizada!</strong>
                 </div> 
            @endif 
            @if(Session::get('message') == 1)
            <div class="alert alert-success alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button> <strong>¡Operacion Exitosa!</strong>
             </div>
             @endif 
        @endif

        <a href="{{ url('Addempresa') }}" class="btn btn-sm btn-success btn-addon"><i class="glyphicon glyphicon-plus"></i> Registrar Empresa</a>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>RFC</th>
                        <th>Nombre</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @isset($empresas)
                        @foreach ($empresas as $emp)
                        <tr>
                            <td>{{$emp['rfc']}}</td>
                            <td>{{$emp['nombre']}}</td>
                            <td>
                                <div style="display: flex">
                                    <a href="{{url('empresas/edit/'.$emp['id'])}}" class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-pencil"></span> </a>
                                    &nbsp;&nbsp;
                                    {!! Form::open(['method' => 'GET', 'url' => ['empresa/delete/'.$emp['id']], 'onsubmit' =>'return confirm("¿Estas seguro de eliminar este producto?")']) !!}
                                        {{--<input type='submit' value='Eliminar producto' class='btn btn-danger btn-sm'>--}}
                                        <button type="submit" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-trash"></i>&nbsp;</button>
                                    {!! Form::close() !!}
                                    {{--<a  id="eliminar_empresa" onclick="eliminar();" href="" class="btn btn-sm btn-danger"><span class="glyphicon glyphicon-trash"></span>eliminar </a>} --}}
                                </div>
                            </td>
                        </tr> 
                        <script>
                            function eliminar(){
                               let ok = confirm("¿Estas seguro de eliminar esta Empresa?");
                            }
                   
                       </script>
                        @endforeach
                    @endisset
                </tbody>
            </table>
        </div>
    </div>
   
@endsection