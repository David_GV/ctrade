@extends('layout.master')
@section('content') 
<div>
	<div class="panel panel-default">
     <div class="panel-heading">Coves no asignados<a class="btn btn-default btn-xs pull-right" href="{{ url('expedientes/show/'.$id_expediente) }}" role="button"><span class="glyphicon glyphicon-arrow-left"></span> Atras</a></div>
	  <div class="panel-body">
        <div class="col-md-12">
            @if(Session::has('message'))
                <div class="alert alert-success alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button> <strong>{{Session::get('message')}}</strong>
                    </div>
            @endif

        <div class="col-md-12">
            <div class="table-responsive">
                <table id="table-pedimentos" class="table table-striped ">
                    <thead>
                        <tr>
                            <td>N° Cove</td>
                            <td>Tipo operación</td>
                            <td>Fecha Expedición</td>
                            <td>Emisor</td>
                            <td>Valor Total</td>
                            <td>Acción</td>
                        </tr>
                    </thead>
                    <tbody id="pedimentos-loader">
                        @isset($coves)
                   			@foreach ($coves as $row)
                                @php
                                    $collection = collect(json_decode($row['json_cove'],true));
                                    $collection = new App\Collector\Collector($collection);
                                    $mercancias = $collection['comprobantes']['mercancias'];
                                    $total= $mercancias['valorTotal'];
                                   // dd($mercancias);
                                   // $total= $collection['comprobantes'];
                                    //dd($total);
                                    //->sum('valorTotal');
                                    $coves_json=json_decode($row['json_cove'],true);
                                      
                                @endphp
                                <tr>
        							<td>{{$row['usr_num_cove']}}</td>
        							<td>{{$coves_json['comprobantes']["tipoOperacion"]}}</td>
        							<td>{{$coves_json['comprobantes']["fechaExpedicion"]}}</td>
        							<td>{{$coves_json['comprobantes']["emisor"]["nombre"]}}</td>
        							<td>${{number_format(isset($total) ? $total : '0' ,2)}}</td>
        							<td>
                                        <a class="btn btn-default btn-xs pull-left" href="{{url('cove_factura',['id_cove'=>$row['id'],'id_expediente' => $id_expediente])}}" role="button" aria-label="Left Align" title="Ver Factura COVE" ><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>

                                        <a class="btn btn-default btn-xs pull-left" href="{{url('/asigna_cove',['id_cove'=>$row['id'],'id_expediente'=>$id_expediente])}}" role="button" aria-label="Left Align" title="Asignar Cove" ><span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span></a>
        							</td>
        						</tr>							
                   			@endforeach
                        @endisset
                    </tbody>
                </table>
                
            </div>
        </div>
	  </div>
	</div>	
</div>
@endsection

@push('scripts')

@endpush