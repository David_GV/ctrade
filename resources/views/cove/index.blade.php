@extends('layout.master')

@section('content')
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">Lista general de COVES
            <div class="panel-body">
          
                <div class="row">
                          
                            <div class="col-md-8"></div>
                            <div class="col-md-4">
                              
                              <a role="button" class="btn btn-primary pull-left"  href="{{url('cargar_cove')}}" ><span class="glyphicon glyphicon-plus"></span> Carga Automática</a>
                              <a role="button" class="btn btn-primary pull-right"  href="{{url('cargar_cove')}}" ><span class="glyphicon glyphicon-plus"></span> Cargar COVE</a>

                            </div>
                        <div class="col-md-12">
                        <div class="table-responsive"> 
                        <table id="myTable" class="table table-striped table-bordered table-hover dataTable no-footer" cellspacing="0">
                            <thead>
                                <tr>
                                    <td>N° Cove</td>
                                    <td>Fecha</td>
                                    <td>Valor Total en dolares</td>
                                    <td width="25">Expediente</td>
                                    <td>Proveedor</td>
                                </tr>
                            </thead>
                            <tbody>
                                @isset($coves)
                                    @foreach ($coves as $row)
                                        @php
                                          $collection = collect(json_decode($row['json_cove'],true));

                                          $collection = new App\Collector\Collector($collection);
                                          $mercancias = $collection->comprobantes['mercancias'];
                                          $total= $mercancias['valorTotal'];
                                          $cove_json=json_decode($row['json_cove'],true);
                                          /*foreach ($mercancias as $mercancia){
                                              dd($mercancia);
                                                if (is_array($mercancia)){
                                                    $total += $mercancia['valorTotal'];
                                             }
                                          }*/

                                        @endphp
                                      <tr>
                                        <td>{{$row['usr_num_cove']}}</td>
                                        <td>{{$cove_json['comprobantes']['fechaExpedicion']}}</td>
                                        <td>${{ number_format($total,2) }}</td>
                                        <td>{{ isset($row['id_expediente']) ? $row['id_expediente'] : 'N/A' }}</td>
                                        <td>{{$cove_json['comprobantes']['emisor']['nombre']}}</td>
                                      </tr>
                                    @endforeach
                                @endisset

                            </tbody>
                        </table>
                    </div>
                    </div>
                    </div>
            
        
                  
                </div>
            </div>
        </div>
    </div>
@push('scripts')

	$(document).ready(function(){
  
          $('#myTable').DataTable( {

            "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
            },
              lengthMenu: [
              [10, 25, 50, -1 ],
              [ '10 ', '25 ', '50 ', 'Ver Todo' ]
              ]
              ,buttons: ['pageLength']
          } );



});

 @endpush

   

@endsection