@extends('layout.master')
@section('content')
    <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-12">
        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>RFC</th>
                </tr>
                </thead>
                <tbody>
                    @isset($agencias)
                        @foreach($agencias as $agencia)
                            <tr>
                                <td>{{ $agencia['id'] }}</td>
                                <td>{{ $agencia['nombre'] }}</td>
                                <td>{{ $agencia['rfc'] }}</td>
                            </tr>
                        @endforeach
                    @endisset
                </tbody>
            </table>
        </div>
    </div>
    @if(method_exists($agencias,'render'))
        {!! $agencias->render() !!}
    @endif
@endsection

