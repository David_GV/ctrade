@extends('layout.master')
@section('content')
        <div class="col-md-10 col-md-offset-1 col-sm-8 col-sm-offset-2 col-xs-12">
            <a href="{{url("usuarios/create")}}" class="btn btn-sm btn-success btn-addon"><i class="glyphicon glyphicon-plus"></i> Crear usuario</a>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Nombre usuario</th>
                            <th>Email</th>
                            <th>Tipo usuario</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($user as $u)
                        <tr>
                            <td>{{$u['username']}}</td>
                            <td>{{$u['email']}}</td>

                            @foreach ($type as $t)
                               @if ($u['usertype_id'] == $t['id'])
                                <td>{{$t['usertype']}}</td>
                               @endif
                            @endforeach
                            
                            <td>
                                <div style="display: flex">
                                    <a href="" class="btn btn-primary"><span class="glyphicon glyphicon-pencil"></span></a>
                                    &nbsp;&nbsp;
                                 
                                    <a href="" class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-trash"></span></a>
                                </div>
                            </td>
                        </tr> 
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
@endsection